package com.tanhoda.farmer.adapter.landItem

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.R
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.model.landAndEditLand.DataItem
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.utils.Utils.Companion.REGISTERED_LAND_COUNT
import com.tanhoda.farmer.utils.Utils.Companion.htmlCodeConvert
import com.tanhoda.farmer.views.landDetails.EditLandDetails
import java.lang.Exception


class ItemLandDetails(val activity: AppCompatActivity, private val landData: List<DataItem?>?) :
    RecyclerView.Adapter<ItemLandDetails.HolderApplication>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderApplication {
        return HolderApplication(
            LayoutInflater.from(activity).inflate(
                R.layout.item_land, parent,
                false
            )
        )

    }

    override fun getItemCount(): Int {
        return landData!!.size
    }

    @TargetApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: HolderApplication, position: Int) {

        holder.txtlandVillage.text =
            htmlCodeConvert(
                activity!!.getString(R.string.village),
                landData!![position]!!.villagename!!.villageName.toString()
            )
        holder.txtlandChittaNo.text =
            htmlCodeConvert(
                activity!!.getString(R.string.chitta_no),
                landData!![position]!!.chittaNo!!.toString()
            )
        holder.txtlandArea.text =
            htmlCodeConvert(
                activity!!.getString(R.string.total_area),
                landData!![position]!!.totalArea + " ha"
            )

        holder.itemLand.setOnClickListener {
            try {
                REGISTERED_LAND_COUNT -= landData!![position]!!.totalArea.toString().toDouble()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            activity.startActivity(
                Intent(
                    activity,
                    EditLandDetails::class.java
                ).putExtra(ParamKey.LAND_INFO_TO_EDIT, landData!![position]!!)
            )
        }
        
    }


    class HolderApplication(item: View) : RecyclerView.ViewHolder(item) {
        val txtlandVillage = item.findViewById<TextView>(R.id.txtlandVillage)
        val txtlandChittaNo = item.findViewById<TextView>(R.id.txtlandChittaNo)
        val txtlandArea = item.findViewById<TextView>(R.id.txtlandArea)
        val itemLand = item.findViewById<CardView>(R.id.itemLand)
    }
}