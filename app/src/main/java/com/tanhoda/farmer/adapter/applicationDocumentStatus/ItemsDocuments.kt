package com.tanhoda.farmer.adapter.applicationDocumentStatus


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.R
import com.tanhoda.farmer.model.applicationViewDetails.DocumentsItem
import com.tanhoda.farmer.utils.ImageUtils
import com.tanhoda.farmer.utils.Utils


class ItemsDocuments(
    val activity: AppCompatActivity,
    val documentsStatus: List<DocumentsItem?>?
) :
    RecyclerView.Adapter<ItemsDocuments.HolderApplication>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderApplication {
        return HolderApplication(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_document, parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return documentsStatus!!.size
    }

    override fun onBindViewHolder(holder: HolderApplication, position: Int) {

        ImageUtils.setImageLive(
            holder.itemDocumentImg,
            Utils.IMAGE_URL_PATH + documentsStatus!![position]!!.imageName,
            activity
        )
        holder.itemDocumentName.text = documentsStatus!![position]!!.documentName!!

        holder.itemDocumentImg.setOnClickListener {

            Utils.showImage(
                activity,
                Utils.IMAGE_URL_PATH + documentsStatus!![position]!!.imageName,
                holder.itemDocumentImg,
                "profile_image",
                "live",
                documentsStatus!![position]!!.documentName!!
            )

        }
    }

    class HolderApplication(item: View) : RecyclerView.ViewHolder(item) {
        val itemDocumentImg = item.findViewById<ImageView>(R.id.itemDocumentImg)
        val itemDocumentName = item.findViewById<TextView>(R.id.itemDocumentName)
    }

}