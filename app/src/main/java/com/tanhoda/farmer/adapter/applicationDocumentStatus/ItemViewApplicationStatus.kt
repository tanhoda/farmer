package com.tanhoda.farmer.adapter.applicationDocumentStatus

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.R
import com.tanhoda.farmer.model.applicationViewDetails.ViewStatusUpdatesItem
import java.lang.Exception

class ItemViewApplicationStatus(
    val activity: AppCompatActivity,
    val viewStatus: List<ViewStatusUpdatesItem?>?

) : RecyclerView.Adapter<ItemViewApplicationStatus.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.item_status, null

            )
        )

    }

    override fun getItemCount(): Int {
        return viewStatus!!.size
    }

    /**
     * Once check Status Icon Changes
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.itemViewStatusStage.text = viewStatus!![position]!!.stages
            holder.itemViewStatusDate.text =
                if (viewStatus[position]!!.date!!.toString().isNotEmpty()) viewStatus[position]!!.date!!.toString() else "-"
            holder.viewItemApproved.text = viewStatus[position]!!.status

            /*holder.itemViewStatusStage.text = viewStatus!![position]!!.stages
                holder.itemViewStatusDate.text = viewStatus!![position]!!.date!!.toString()
                holder.viewItemApproved.text = viewStatus!![position]!!.status*/


            if (viewStatus[position]!!.status.toString().toLowerCase() == "Pending".toLowerCase()) {
                holder.itemImgViewStatus.setImageResource(R.drawable.vector_drawable_pending)
            } else {
                holder.itemImgViewStatus.setImageResource(R.drawable.ic_approve)
                //holder.itemImgViewStatus.setImageResource(R.drawable.vector_drawable_reject_sign)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    open class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemViewStatusStage = itemView.findViewById<TextView>(R.id.itemViewStatusStage)
        val itemViewStatusDate = itemView.findViewById<TextView>(R.id.itemViewStatusDate)
        val viewItemApproved = itemView.findViewById<TextView>(R.id.viewItemApproved)
        val itemImgViewStatus = itemView.findViewById<ImageView>(R.id.itemImgViewStatus)
    }
}