package com.tanhoda.farmer.adapter.schemeDetails

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.model.schemeDetails.SchemeComponentItem
import com.tanhoda.farmer.model.schemeDetails.SchemecategoryItem
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.views.schemeDetails.ViewSubComponent
import java.lang.Exception

class ItemSchemeComponent(
    val activity: BaseActivity,
    val schemecomponent: List<SchemeComponentItem?>?,
    val schemecategory: ArrayList<SchemecategoryItem?>?,
    val schemeName: String
) : RecyclerView.Adapter<ItemSchemeComponent.ItemViewHodler>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHodler {

        return ItemViewHodler(
            LayoutInflater.from(activity).inflate(
                R.layout.item_scheme_sub_componet,
                null
            )
        )
    }

    override fun getItemCount(): Int {

        return schemecomponent!!.size
    }

    override fun onBindViewHolder(holder: ItemViewHodler, position: Int) {

        if (position == 0) {
            holder.layOfAction.visibility = View.GONE
        } else {

            holder.layOfAction.visibility = View.GONE
            holder.itemSubComponentView.setBackgroundColor(
                ContextCompat.getColor(
                    activity,
                    R.color.colorWhite
                )
            )

            holder.item_si_no.setTextColor(ContextCompat.getColor(activity, R.color.colorBlack))
            holder.item_cub_component.setTextColor(
                ContextCompat.getColor(
                    activity,
                    R.color.colorBlack
                )
            )
            holder.item_unit.setTextColor(ContextCompat.getColor(activity, R.color.colorBlack))
            holder.item_min_limit.setTextColor(
                ContextCompat.getColor(
                    activity,
                    R.color.colorBlack
                )
            )
            holder.item_max_limit.setTextColor(
                ContextCompat.getColor(
                    activity,
                    R.color.colorBlack
                )
            )
            Log.d("subComponetList", "" + schemecomponent!![position]!!.componentName!!.toString())

            holder.item_min_limit.text = schemecomponent!![position]!!.minLimit!!.toString()
            holder.item_max_limit.text = schemecomponent[position]!!.maxLimit!!.toString()
            holder.item_si_no.text = "" + (position)
            Utils.marquee(holder.item_cub_component)
            holder.item_cub_component.text =
                schemecomponent[position]!!.componentName!!.toString()
            holder.item_unit.text = schemecomponent[position]!!.unit!!.toString()

        }

        holder.txtItemKnowMore.setOnClickListener {
            MessageUtils.showToastMessage(activity, "Know More Waiting", false)
        }



        holder.txtItemApply.setOnClickListener {
            MessageUtils.showToastMessage(activity, "Apply Waiting", false)
        }


        holder.itemSubComponentView.setOnClickListener {
            var category = ""
            try {
                schemecategory!!.forEachIndexed { index, schemecategoryItem ->
                    if (schemecategoryItem!!.id == schemecomponent!![position]!!.categoryId) {
                        category = schemecategoryItem.category!!.toString()
                    }
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            activity.startActivity(
                Intent(
                    activity,
                    ViewSubComponent::class.java
                ).putExtra(
                    ParamKey.SUB_COMPONENT,
                    schemecomponent?.get(position)!!
                ).putExtra(
                    ParamKey.SUB_COMPONENT_NAME,
                    schemecomponent?.get(position)!!.componentName
                ).putExtra(
                    ParamKey.SCHEME_CATEGORY,
                    category
                ).putExtra(
                    ParamKey.SCHEME_NAME,
                    schemeName
                )
            )
        }
    }

    class ItemViewHodler(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemSubComponentView = itemView.findViewById<LinearLayout>(R.id.itemSubComponentView)
        val item_si_no = itemView.findViewById<TextView>(R.id.item_si_no)
        val item_cub_component = itemView.findViewById<TextView>(R.id.item_cub_component)
        val item_unit = itemView.findViewById<TextView>(R.id.item_unit)
        val item_min_limit = itemView.findViewById<TextView>(R.id.item_min_limit)
        val item_max_limit = itemView.findViewById<TextView>(R.id.item_max_limit)
        val layOfAction = itemView.findViewById<LinearLayout>(R.id.layOfAction)

        val txtItemApply = itemView.findViewById<TextView>(R.id.txtItemApply)
        val txtItemKnowMore = itemView.findViewById<TextView>(R.id.txtItemKnowMore)
    }
}