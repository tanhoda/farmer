package com.tanhoda.farmer.adapter.schemeDetails

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.model.schemeDetails.SchemesItem
import com.tanhoda.farmer.utils.ImageUtils
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.views.schemeDetails.DetailedSchemes

class ItemSchemeInfo(val activity: BaseActivity, val listOfSchemeInfo: List<SchemesItem?>?) :
    RecyclerView.Adapter<ItemSchemeInfo.ItemSchemeHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemSchemeHolder {
        return ItemSchemeHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.item_scheme_info,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return listOfSchemeInfo!!.size
    }

    override fun onBindViewHolder(holder: ItemSchemeHolder, position: Int) {
        holder.scheme_name.text = listOfSchemeInfo!![position]!!.schemeName
        ImageUtils.setImageLive(
            holder.imgScheme,
            Utils.IMAGE_URL_PATH_SCHEME + listOfSchemeInfo!![position]!!.schemeImage,
            activity
        )
        holder.itemLayOfScheme.setOnClickListener {
            activity.startActivity(
                Intent(
                    activity,
                    DetailedSchemes::class.java
                ).putExtra(ParamKey.SCHEME_NAME, listOfSchemeInfo!![position])
            )
        }
    }

    class ItemSchemeHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val scheme_name = itemView.findViewById<TextView>(R.id.txtSchemeName)
        val imgScheme = itemView.findViewById<ImageView>(R.id.imgScheme)
        val itemLayOfScheme = itemView.findViewById<CardView>(R.id.itemLayOfScheme)
    }
}