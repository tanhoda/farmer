package com.tanhoda.farmer.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.R
import com.tanhoda.farmer.utils.ImageUtils
import java.io.File

class ItemMultiImages(
    val activity: FragmentActivity,
    val fileArrayList: ArrayList<File>
) : RecyclerView.Adapter<ItemMultiImages.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_multi_images,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return fileArrayList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.filename?.text = fileArrayList[position].name
            ImageUtils.setImage(holder.image, fileArrayList[position].absolutePath, activity)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        val filename = itemView?.findViewById<TextView>(R.id.image_name_textview)
        val image = itemView?.findViewById<ImageView>(R.id.image_view)
    }
}