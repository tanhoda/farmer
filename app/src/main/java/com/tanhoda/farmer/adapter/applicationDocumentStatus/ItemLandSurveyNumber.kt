package com.tanhoda.farmer.adapter.applicationDocumentStatus

import android.app.Dialog
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.R
import com.tanhoda.farmer.model.applicationViewDetails.ViewApplicationLandDetails
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.Utils
import kotlinx.android.synthetic.main.dialog_land_info.*
import kotlinx.android.synthetic.main.dialog_title.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*


class ItemLandSurveyNumber(
    val activity: AppCompatActivity,
    val landsInfo: ArrayList<ViewApplicationLandDetails?>?
) : RecyclerView.Adapter<ItemLandSurveyNumber.ViewHolderForLand>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderForLand {
        return ViewHolderForLand(
            LayoutInflater.from(activity).inflate(
                R.layout.item_land_survey_number,
                null
            )
        )
    }

    override fun getItemCount(): Int {
        return landsInfo!!.size
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: ViewHolderForLand, position: Int) {
        holder.itemLandSurveySiNo.text = "" + (position + 1)
        holder.itemLandSurveyNumber.text = landsInfo!![position]!!.serveyno
        holder.itemLandSurveyNumberAddress.text =
            landsInfo[position]!!.village + " - " + landsInfo!![position]!!.block

        holder.linear_layout_land_list_details.setOnClickListener {
            showLandInfoDialog(landsInfo[position])
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun showLandInfoDialog(viewApplicationLandDetails: ViewApplicationLandDetails?) {
        val dialog = Dialog(activity)

        dialog.setContentView(R.layout.dialog_land_info)
        dialog.window.setBackgroundDrawableResource(R.color.colorDialogTrans)
        dialog.btnSuccess.visibility = View.GONE
        val backBtn = dialog.findViewById<Button>(R.id.btnCancel)
        backBtn.text = activity.getString(R.string.dismiss)
        dialog.message_title_permission.text = activity.getString(R.string.land_details)


        backBtn.setOnClickListener {
            MessageUtils.dismissDialog(dialog)
        }
        dialog.edtSurveyNumInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.survey_no) + " : ",
            viewApplicationLandDetails!!.serveyno.toString()
        )
        dialog.edtChittaNumberInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.chitta_no) + " : ",
            viewApplicationLandDetails.chittano.toString()
        )
        dialog.edtStateInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.state) + " : ",
            viewApplicationLandDetails.state.toString()
        )
        dialog.edtDistrictInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.district) + " : ",
            viewApplicationLandDetails.district.toString()
        )
        dialog.edtBlockInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.block) + " : ",
            viewApplicationLandDetails.block.toString()
        )
        dialog.edtVillageInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.village) + " : ",
            viewApplicationLandDetails.village.toString()
        )
        dialog.edtSubDivisionInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.sub_division) + " : ",
            viewApplicationLandDetails.subdivision.toString()
        )

        var units = ""
        if (viewApplicationLandDetails.unit.toString().isNotEmpty()) {
            units = if (viewApplicationLandDetails.unit.toString() == "Square Meter") {
                "Sq m"
            } else {
                viewApplicationLandDetails.unit.toString()
            }
        }

        dialog.edtAreaProposedInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.area_proposed) + " : ",
            viewApplicationLandDetails.areaproposed.toString() + " " + units
        )
        dialog.edtSourceOfIrrigationInLandInfo.text =
            Utils.htmlNextLine(
                activity.getString(R.string.source_of_irrigation) + " : ",
                viewApplicationLandDetails.sourceOfIrregation.toString()
            )

        dialog.edtTotalAreaInLandInfo.text = Utils.htmlNextLine(
            activity.getString(R.string.total_area) + " : ",
            viewApplicationLandDetails.farmerland.toString() + " Ha"
        )

        dialog.show()
    }

    class ViewHolderForLand(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemLandSurveySiNo = itemView.findViewById<TextView>(R.id.itemLandSurveySiNo)
        val itemLandSurveyNumber = itemView.findViewById<TextView>(R.id.itemLandSurveyNumber)
        val linear_layout_land_list_details =
            itemView.findViewById<LinearLayout>(R.id.linear_layout_land_list_details)
        val itemLandSurveyNumberAddress =
            itemView.findViewById<TextView>(R.id.itemLandSurveyNumberAddress)
    }
}