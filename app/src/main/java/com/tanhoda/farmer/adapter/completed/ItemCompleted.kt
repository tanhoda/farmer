package com.tanhoda.farmer.adapter.completed

import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.R
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.model.applicationStatus.DataDTO
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.views.applicationProgressReport.applicationDetailedView.ApplicationDetailedView


class ItemCompleted(val activity: AppCompatActivity, private val completedList: List<DataDTO?>?) :
    RecyclerView.Adapter<ItemCompleted.HolderApplication>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderApplication {
        return HolderApplication(
            LayoutInflater.from(activity).inflate(
                R.layout.item_application_status, parent,
                false
            )
        )

    }

    override fun getItemCount(): Int {
        return completedList!!.size
    }

    override fun onBindViewHolder(holder: HolderApplication, position: Int) {
        holder.txtApplicationId.text =
            Utils.htmlCodeConvert(
                activity.resources.getString(R.string.application_id),
                completedList!![position]!!.application_id.toString()
            )
        holder.txtApplicationStatus.text =
            Utils.htmlCodeConvert(
                activity.resources.getString(R.string.application_status),
                completedList!![position]!!.application_status.toString()
            )
        holder.txtAppName.text =
            Utils.htmlCodeConvert(
                activity.resources.getString(R.string.scheme_name),
                completedList!![position]!!.schemename!!.scheme_name.toString()
            )

        holder.itemApplicationDetails.setOnClickListener {
            Log.d("sdslkds",""+completedList!![position]!!.id)
            activity.startActivity(
                Intent(activity, ApplicationDetailedView::class.java).putExtra(
                    ParamKey.APP_ID,
                    completedList!![position]!!.id.toString()
                )
            )
        }
    }


    class HolderApplication(item: View) : RecyclerView.ViewHolder(item) {
        val txtApplicationId = item.findViewById<TextView>(R.id.txtApplicationId)
        val txtApplicationStatus = item.findViewById<TextView>(R.id.txtApplicationStatus)
        val txtAppName = item.findViewById<TextView>(R.id.txtAppName)
        val itemApplicationDetails = item.findViewById<CardView>(R.id.itemApplicationDetails)

    }
}