package com.tanhoda.farmer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.utils.*
import com.tanhoda.farmer.views.addNewApplication.AddNewApplication
import com.tanhoda.farmer.views.registerNewFarmer.RegisterActivity
import com.tanhoda.farmer.views.schemeDetails.SchemeDetails
import com.tanhoda.farmer.views.settings.SettingsActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.nav_header_main.*

open class MainActivity : BaseActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        var drawerLayout: DrawerLayout? = null
        var navView: NavigationView? = null
    }

    lateinit var toggle: ActionBarDrawerToggle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.container_body)
        drawerLayout?.addDrawerListener(toggle)
        toggle.syncState()

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_about
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView?.setupWithNavController(navController)

        /* navView?.setNavigationItemSelectedListener {
             when (it.itemId) {
             }
             true
         }*/


        rcyNav.layoutManager = LinearLayoutManager(this)
        rcyNav.adapter = ItemNavigation(this, getNavigationItems())
        itemNavLay.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java).putExtra("from", "profile"))

        }

    }

    override fun onResume() {
        super.onResume()
        try {
            val farmerDetails = SessionManager.getObject(this)
            if (farmerDetails != null) {
                val profile = Utils.IMAGE_URL_PATH_PROFILE + farmerDetails.profile_pic
                try {
                    ImageUtils.setImageLive(profileImage, profile, this)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

                textViewFarmerNameHeader.text = farmerDetails.name
                textViewEmailHeader.text = farmerDetails.email

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        BaseActivity("MainActivity")
    }

    override fun onPause() {
        super.onPause()
        BaseActivity("")
    }

    private fun getNavigationItems(): ArrayList<ModelNavigation> {
        val navItems = ArrayList<ModelNavigation>()
        var modelNavigation =
            ModelNavigation(getString(R.string.menu_home), R.drawable.ic_menu_camera)
        navItems.add(modelNavigation)
        modelNavigation =
            ModelNavigation(getString(R.string.profile_info), R.drawable.ic_menu_camera)
        navItems.add(modelNavigation)

        modelNavigation =
            ModelNavigation(getString(R.string.scheme), R.drawable.ic_menu_camera)
        navItems.add(modelNavigation)

        modelNavigation = ModelNavigation(
            getString(R.string.add_new_application),
            R.drawable.ic_menu_camera
        )
        navItems.add(modelNavigation)
        /*modelNavigation =
            ModelNavigation(getString(R.string.menu_about), R.drawable.ic_menu_camera)
        navItems.add(modelNavigation)
        modelNavigation =
            ModelNavigation(getString(R.string.menu_privacy_policy), R.drawable.ic_menu_camera)
        navItems.add(modelNavigation)*/
        modelNavigation =
            ModelNavigation(getString(R.string.app_settings), R.drawable.ic_menu_camera)
        navItems.add(modelNavigation)
        modelNavigation = ModelNavigation(getString(R.string.logout), R.drawable.ic_menu_camera)
        navItems.add(modelNavigation)


        return navItems
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.container_body)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun disableNavigation() {
        supportActionBar!!.show()

        toggle.isDrawerIndicatorEnabled = false
        drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(false)
        toggle.setToolbarNavigationClickListener {
            onBackPressed()
        }

    }


    fun enableNavigation(titleName: String, position: Int) {
        supportActionBar!!.show()
        drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        supportActionBar!!.setHomeButtonEnabled(true)
        toggle.isDrawerIndicatorEnabled = true
        supportActionBar!!.title = titleName
        navView?.menu?.getItem(position)?.isChecked = true

    }


    fun navigationHide() {
        supportActionBar!!.hide()
        drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }


    class ItemNavigation(
        val activity: AppCompatActivity,
        val itemSelectedListener: ArrayList<ModelNavigation>
    ) : RecyclerView.Adapter<ItemNavigation.HolderNavigation>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderNavigation {
            return HolderNavigation(
                LayoutInflater.from(activity).inflate(
                    R.layout.menu_navigation,
                    parent,
                    false
                )
            )
        }

        override fun getItemCount(): Int {
            return itemSelectedListener.size
        }

        override fun onBindViewHolder(holder: HolderNavigation, position: Int) {
            holder.imgMenu.setImageResource(itemSelectedListener[position].navIcon)
            holder.txtMenuTitle.text = itemSelectedListener[position].navTitle
            Utils.marquee(holder.txtMenuTitle)
            holder.itemMenuClick.setOnClickListener {
                try {
                    drawerLayout?.closeDrawer(GravityCompat.START)
                    when (position) {
                        0 -> { //Home
                            //activity.startActivity(Intent(activity, HomeFragment::class.java))
                        }
                        1 -> { //Profile
                            activity.startActivity(
                                Intent(
                                    activity,
                                    RegisterActivity::class.java
                                ).putExtra("from", "profile")
                            )
                        }
                        2 -> {
                            activity.startActivity(
                                Intent(
                                    activity,
                                    SchemeDetails::class.java
                                )
                            )
                        }

                        3 -> { //Add New Application
                            if (checkBankAndLandAdded()) {
                                activity.startActivity(
                                    Intent(
                                        activity,
                                        AddNewApplication::class.java
                                    ).putExtra(ParamKey.IS_THERE, false)
                                )
                            }
                        }
                        4 -> {
                            activity.startActivity(Intent(activity, SettingsActivity::class.java))
                        }
                        5 -> {
                            //activity.startActivity(Intent(activity, SplashFragment::class.java))
                            SessionManager.logoutUser(activity)
                        }

                    }


                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }


        }

        private fun checkBankAndLandAdded(): Boolean {
            /* if (LAND_COUNT_MAX == 0 && BANK_COUNT_MAX == 0) {
                 snackView=  MessageUtils.showSnackBar(
                     activity!!,
                     activity!!.homeSnackView,
                     "Add your Land & Bank Details"
                 )
                 return false
             } else*/ if (Utils.LAND_COUNT_MAX == 0) {
                MessageUtils.showToastMessage(
                    activity,
                    "Add your Land Info and try again", false
                )
                return false
            } else if (Utils.BANK_COUNT_MAX == 0) {
                MessageUtils.showToastMessage(
                    activity, "Add your Bank Info and Try again", false
                )
                return false
            }
            return true
        }


        class HolderNavigation(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val itemMenuClick = itemView.findViewById<LinearLayout>(R.id.itemMenuClick)
            val imgMenu = itemView.findViewById<ImageView>(R.id.imgMenu)
            val txtMenuTitle = itemView.findViewById<TextView>(R.id.txtMenuTitle)
        }
    }

    public class ModelNavigation(val navTitle: String, val navIcon: Int)

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase, "en"))
    }


}
