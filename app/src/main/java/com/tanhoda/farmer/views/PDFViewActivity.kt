package com.tanhoda.farmer.views


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build

import android.os.Bundle
import android.print.PrintAttributes
import android.print.PrintManager
import android.util.Log
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity

import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.utils.ConnectivityReceiverListener
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.utils.Utils.Companion.JITINVOICEPDF
import com.tanhoda.farmer.utils.Utils.Companion.PDF_LOADER
import com.tanhoda.farmer.utils.Utils.Companion.RTGSPDF
import com.template.receiver.ConnectivityReceiver

import kotlinx.android.synthetic.main.activity_pdfview.*


@SuppressLint("SetJavaScriptEnabled", "ByteOrderMark")
class PDFViewActivity : BaseActivity(), ConnectivityReceiverListener {


    lateinit var dialog: Dialog
    var mNetworkReceiver: BroadcastReceiver? = null
    var fileName: String = ""
    var filetype: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {

            setContentView(R.layout.activity_pdfview)
            if (intent.hasExtra("fileName")) {
                fileName = intent.getStringExtra("fileName")
            }

            if (intent.hasExtra("type")) {
                filetype = intent.getStringExtra("type")
            }

            Log.d("PDFActivity", "$fileName filetype $filetype")

            supportActionBar?.title = filetype
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

            webPDFWorkorder.webViewClient = WebViewClient()
            webPDFWorkorder.webChromeClient = WebChromeClient()
            //webPDFWorkorder.loadUrl(WORK_ORDER_PDF+fileName)
            //txtPdfTitle?.text=filetype
            // Get the web view settings instance
            val settings = webPDFWorkorder.settings
            // Enable java script in web view
            settings.javaScriptEnabled = true
            // Enable and setup web view cache
            settings.setAppCacheEnabled(true)
            settings.cacheMode = WebSettings.LOAD_DEFAULT
            settings.setAppCachePath(cacheDir.path)
            // Enable zooming in web view
            settings.setSupportZoom(true)
            settings.builtInZoomControls = true
            settings.displayZoomControls = false
            // Zoom web view text
            settings.textZoom = 40
            // Enable disable images in web view
            settings.blockNetworkImage = false
            // Whether the WebView should load image resources
            settings.loadsImagesAutomatically = true
            // More web view settings
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                settings.safeBrowsingEnabled = true  // api 26
            }
            //settings.pluginState = WebSettings.PluginState.ON
            settings.useWideViewPort = true
            settings.loadWithOverviewMode = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            settings.mediaPlaybackRequiresUserGesture = false
            // More optional settings, you can enable it by yourself
            settings.domStorageEnabled = true
            settings.setSupportMultipleWindows(true)
            settings.allowContentAccess = true
            settings.setGeolocationEnabled(true)
            settings.allowUniversalAccessFromFileURLs = true
            settings.allowFileAccess = true

            val newUA =
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Safari/602.1.50"
            settings.userAgentString = newUA

            // WebView settings
            webPDFWorkorder.fitsSystemWindows = true

            /* //Custom Tab Builder

                 val builder = CustomTabsIntent.Builder()
                 builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
                 builder.addDefaultShareMenuItem()
                 builder.setExitAnimations(this, R.anim.right_to_left_end, R.anim.left_to_right_end)
                 builder.setStartAnimations(this, R.anim.left_to_right_start, R.anim.right_to_left_start)
                 builder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))

                 val customTabsIntent = builder.build()
                 customTabsIntent.launchUrl(this, Uri.parse(WORK_ORDER_PDF+fileName))*/

            /*  if(fileName.isNotEmpty()){
                      if(Utils.haveNetworkConnection(this)){
                          imgPdfPrinter?.visibility =View.VISIBLE
                          webPDFWorkorder?.visibility =View.VISIBLE
                          txtWorkOrderNoDataFound?.visibility =View.GONE
                          printWebView(fileName)
                      }else{
                          webPDFWorkorder?.visibility =View.GONE
                          txtWorkOrderNoDataFound?.visibility =View.VISIBLE
                          imgPdfPrinter?.visibility =View.GONE
                          txtWorkOrderNoDataFound?.text = getString(R.string.check_internet)
                          MessageUtils.showSnackBar(this,txtWorkOrderNoDataFound,getString(R.string.check_internet))
                      }
                  } else{
                      imgPdfPrinter?.visibility =View.GONE
                      txtWorkOrderNoDataFound?.visibility =View.VISIBLE
                      txtWorkOrderNoDataFound?.text = getString(R.string.no_pdf_found)
                      MessageUtils.showSnackBar(this,txtWorkOrderNoDataFound,getString(R.string.no_pdf_found))
                  }*/

            imgPdfPrinter?.setOnClickListener {
                try {
                    createWebPrintJob(webPDFWorkorder!!)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }


            /**
             * interface network checking
             */

            ConnectivityReceiver.networkInterface(this)

            /**
             * Broadcast Receiver
             */
            mNetworkReceiver = ConnectivityReceiver()

            registerReceiver(
                mNetworkReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )

        } catch (Ex: java.lang.Exception) {
            Ex.printStackTrace()
        }

    }

    private fun createWebPrintJob(webView: WebView) {

        val printManager =
            this.getSystemService(Context.PRINT_SERVICE) as PrintManager
        val printAdapter = webView.createPrintDocumentAdapter("MyDocument")
        val jobName = getString(R.string.app_name) + " Print Test"
        printManager.print(jobName, printAdapter, PrintAttributes.Builder().build())
    }


    private fun printWebView(url: String) {
        dialog = MessageUtils.showDialog(this)
        dialog.setOnKeyListener { _, _, _ ->
            dialog.dismiss()
            finish()
            false
        }

        Log.d("WEBURL_PDF_reader", "filetype $filetype fileName $fileName")
        var urlLink = ""
        //                createWebPrintJob(view)
        when (filetype) {
            getString(R.string.work_order_release) -> urlLink = Utils.WORK_ORDER_PDF + url
            getString(R.string.view_application_pdf) -> urlLink = PDF_LOADER + url
            getString(R.string.view_rtgs_pdf) -> urlLink = RTGSPDF + url

            getString(R.string.view_jit_pdf) -> urlLink = JITINVOICEPDF + url
        }

        Log.d("urlLinksdsd", "" + urlLink)

        webPDFWorkorder.loadUrl(urlLink)


        webPDFWorkorder.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                return false
            }

            override fun onPageFinished(view: WebView, url: String) {
//                createWebPrintJob(view)
                MessageUtils.dismissDialog(dialog)

            }

        }


    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


    override fun onDestroy() {
        super.onDestroy()
        try {
            if (mNetworkReceiver != null) {
                unregisterReceiver(mNetworkReceiver)
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }


    /**
     * interface IsNetWorking Checking
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {

        if (isConnected) {
            imgPdfPrinter?.visibility = View.VISIBLE
            webPDFWorkorder?.visibility = View.VISIBLE
            txtWorkOrderNoDataFound?.visibility = View.GONE
            printWebView(fileName)
        } else {
            webPDFWorkorder?.visibility = View.GONE
            txtWorkOrderNoDataFound?.visibility = View.VISIBLE
            imgPdfPrinter?.visibility = View.GONE
            txtWorkOrderNoDataFound?.text = getString(R.string.no_internet_connection)
            MessageUtils.showSnackBarAction(
                this,
                txtWorkOrderNoDataFound,
                getString(R.string.no_internet_connection)
            )
        }
    }


}
