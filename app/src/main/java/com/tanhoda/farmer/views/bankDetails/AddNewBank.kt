package com.tanhoda.farmer.views.bankDetails


import android.Manifest.permission.*

import android.content.Intent
import android.os.Bundle
import android.os.Environment

import android.provider.MediaStore
import android.util.Log


import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.Gson

import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamAPI.Companion.ADD_NEW_BANK
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.addNewBank.bank.BanknamesDTO
import com.tanhoda.farmer.model.addNewBank.bank.ModelBankNameDTO
import com.tanhoda.farmer.model.addNewBank.branch.BranchesDTO
import com.tanhoda.farmer.model.addNewBank.branch.ModelBranchNamesDTO
import com.tanhoda.farmer.model.addNewBank.successBank.ModelBankAddedSuccessDTO
import com.tanhoda.farmer.model.login.FarmerDetails
import com.tanhoda.farmer.utils.ImageUtils
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.SessionManager
import com.tanhoda.farmer.utils.SessionManager.getFarmerId
import com.tanhoda.farmer.utils.SessionManager.updateLoginSession
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.utils.Utils.Companion.FORMAT_IFSC_CODE

import com.tanhoda.farmer.utils.Utils.Companion.isValidInputFormat
import com.tanhoda.farmer.utils.Utils.Companion.multiPartFromFile
import com.tanhoda.farmer.utils.Utils.Companion.showImage
import com.tanhoda.farmer.utils.permissions.CallbackPermission
import com.tanhoda.farmer.utils.permissions.PermissionDescription
import com.tanhoda.farmer.utils.permissions.RequestPermission
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.BANK_PASS_BOOK
import kotlinx.android.synthetic.main.activity_bank_details_add.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File


class AddNewBank : BaseActivity(), CallbackPermission, View.OnClickListener, ReadWriteAPI {
    var bankPassBook = ""
    var readWriteAPI: ReadWriteAPI? = null

    var bankDetailedList: List<BanknamesDTO>? = null
    var bankList: ArrayList<String>? = null

    var branchDetailedList: List<BranchesDTO>? = null
    var branchList: ArrayList<String>? = null
    var farmerDetails: FarmerDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.add_new_bank)
        setContentView(R.layout.activity_bank_details_add)
        readWriteAPI = this
        initCallbackPermission(this)
        edtPassBook.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
        btnSuccess.setOnClickListener(this)
        imgBankPassBook.setOnClickListener(this)
        imgBankPassBookLive.setOnClickListener(this)

        farmerDetails = SessionManager.getObject(this)
        if (farmerDetails != null) {
            updateUI(farmerDetails!!)
        }

        imgBankPassBook.visibility = View.GONE
        sizeBankPassBook.visibility = View.GONE

        val map = HashMap<String, String>()
        Connector.callBasic(this, map, this, ParamAPI.BANK_NAMES)

        edtBankName.setOnClickListener { spBankName.performClick() }
        edtBranchName.setOnClickListener { spBranchName.performClick() }

        spBankName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val bankName = spBankName.selectedItem.toString()
                    if (bankName != getString(R.string.select_your_bank)) {
                        val map = HashMap<String, String>()
                        map[ParamKey.BANK_NAME] = bankName.trim()

                        Connector.callBasic(
                            this@AddNewBank,
                            map,
                            readWriteAPI!!,
                            ParamAPI.BANK_BRANCH_NAMES
                        )
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.edtPassBook -> {
                try {
                    RequestPermission.checkPermissions(
                        BANK_PASS_BOOK,
                        this,
                        this,
                        PermissionDescription.permissionForCameraAndStorage(this),
                        arrayOf(
                            WRITE_EXTERNAL_STORAGE,
                            READ_EXTERNAL_STORAGE,
                            CAMERA
                        )
                    )
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
            R.id.btnSuccess -> {
                try {
                    val map = HashMap<String, Any>()

                    val bankName = spBankName.selectedItem.toString()
                    val branchName = spBranchName.selectedItem.toString()
                    val ifsc = edtIFSC.text.toString()
                    val accountNumber = edtBankAccountNo.text.toString()
                    map[ParamKey.FARMER_ID] = getFarmerId(this)
                    map[ParamKey.BANK_NAME] = bankName
                    map[ParamKey.BRANCH_NAME] = branchName
                    map[ParamKey.IFSC_CODE] = ifsc
                    map[ParamKey.BANK_ACCOUNT_NUMBER] = accountNumber
                    //map[ParamKey.BANK_PASSBOOK_COPY] = bankPassBook

                    if (addBankValidation(map)) {
                        if (bankPassBook.isEmpty()) {
                            showSnackView(
                                getString(R.string.select_bank_pass_book),
                                addBankSnackView
                            )
                            return
                        }

                        val listOfPart = ArrayList<MultipartBody.Part>()

                        try {
                            if (Utils.checkFile(bankPassBook)) {
                                listOfPart.add(
                                    multiPartFromFile(
                                        bankPassBook,
                                        ParamKey.BANK_PASSBOOK_COPY
                                    )
                                )
                            }
                        } catch (ex: java.lang.Exception) {
                            ex.printStackTrace()
                        }

                        Connector.callBasicWithPart(
                            this,
                            map,
                            listOfPart,
                            readWriteAPI!!,
                            ADD_NEW_BANK
                        )

                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
            R.id.btnCancel -> {
                onBackPressed()
            }
            R.id.imgBankPassBook -> {
                try {
                    showImage(
                        this,
                        bankPassBook,
                        imgBankPassBook,
                        "profile_image",
                        "local",
                        getString(R.string.bank_passbook)
                    )
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
            R.id.imgBankPassBookLive -> {
                try {

                    if (farmerDetails != null) {
                        if (farmerDetails!!.bank_passbook_copy != null) {
                            Log.d(
                                "valusehsdj",
                                "" + Utils.IMAGE_URL_PATH + farmerDetails!!.bank_passbook_copy.toString()
                            )
                            showImage(
                                this,
                                Utils.IMAGE_URL_PATH + farmerDetails!!.bank_passbook_copy!!,
                                imgBankPassBookLive,
                                "profile_image",
                                "live",
                                getString(R.string.bank_passbook)
                            )
                        } else {
                            MessageUtils.showToastMessage(
                                this,
                                getString(R.string.image_file_not_found),
                                false
                            )
                        }
                    } else {
                        MessageUtils.showToastMessage(
                            this,
                            getString(R.string.file_not_upload),
                            false
                        )
                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun addBankValidation(map: java.util.HashMap<String, Any>): Boolean {
        if (map[ParamKey.BANK_NAME].toString().isEmpty() || map[ParamKey.BANK_NAME].toString() == getString(
                R.string.select_your_bank
            )
        ) {
            showSnackView(getString(R.string.select_your_bank), addBankSnackView)
            return false
        } else if (map[ParamKey.BRANCH_NAME].toString().isEmpty() || map[ParamKey.BRANCH_NAME].toString() == getString(
                R.string.select_your_branch
            )
        ) {
            showSnackView(getString(R.string.select_your_branch), addBankSnackView)
            return false
        } else if (map[ParamKey.IFSC_CODE].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_ifsc_code), addBankSnackView)
            return false
        } else if (!isValidInputFormat(map[ParamKey.IFSC_CODE].toString(), FORMAT_IFSC_CODE)) {
            showSnackView(getString(R.string.enter_valid_ifsc_code), addBankSnackView)
            return false
        } else if (map[ParamKey.BANK_ACCOUNT_NUMBER].toString().isEmpty()) {
            showSnackView(getString(R.string.ewnter_bank_account_number), addBankSnackView)
            return false
        } else if (map[ParamKey.BANK_ACCOUNT_NUMBER].toString().length < 9) {
            showSnackView(getString(R.string.enter_valid_account_number), addBankSnackView)
            return false
        } else if (map[ParamKey.BANK_ACCOUNT_NUMBER].toString().length > 18) {
            showSnackView(getString(R.string.enter_valid_account_number), addBankSnackView)
            return false
        } /*else if (map[ParamKey.BANK_PASSBOOK_COPY].toString().isEmpty()) {
            showSnackView(getString(R.string.select_bank_pass_book), addBankSnackView)
            return false
        }*/
        return true

    }


    override fun activityResultCallback(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            val contentURI = data!!.data
            val bitmap = MediaStore.Images.Media.getBitmap(
                contentResolver,
                contentURI
            )
            if (contentURI != null) {
                imgBankPassBook.visibility = View.VISIBLE
                imgBankPassBookLive.visibility = View.GONE
                bankPassBook = ImageUtils.saveImage(this, bitmap)
                if (Utils.getFileSize(bankPassBook) <= Utils.IMAGE_SIZE) {
                    imgBankPassBook.setImageBitmap(bitmap)
                    sizeBankPassBook.visibility = View.GONE
                } else {
                    bankPassBook = ""
                    imgBankPassBook.setImageResource(R.drawable.ic_menu_camera)
                    sizeBankPassBook.visibility = View.VISIBLE
                }

            } else {
                imgBankPassBookLive.visibility = View.VISIBLE
                imgBankPassBook.visibility = View.GONE
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onError(message: String, api: String) {
        showSnackView(message, addBankSnackView)
    }

    override fun onActionAfterPermissionGrand(requestCode: Int) {
        RequestPermission.choosePhotoFromGallery(this, requestCode)
    }


    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        when (api) {
            ParamAPI.BANK_NAMES -> {
                setBankName(responseBody)
            }
            ParamAPI.BANK_BRANCH_NAMES -> {
                setBranchName(responseBody)
            }
            ADD_NEW_BANK -> {
                try {
                    val modelBankAddedSuccess =
                        Gson().fromJson(
                            responseBody.body()?.string(),
                            ModelBankAddedSuccessDTO::class.java
                        )

                    if (modelBankAddedSuccess != null) {
                        if (modelBankAddedSuccess.status!!) {


                            if (modelBankAddedSuccess.data != null) {
                                MessageUtils.showToastMessage(
                                    this,
                                    modelBankAddedSuccess.message!!,
                                    false
                                )
                                updateLoginSession(
                                    this,
                                    modelBankAddedSuccess.data.name,
                                    modelBankAddedSuccess.data.email,
                                    modelBankAddedSuccess.data.mobile_number,
                                    modelBankAddedSuccess.data.id.toString(),
                                    "",
                                    modelBankAddedSuccess.data.profile_pic,
                                    modelBankAddedSuccess.data
                                )
                                updateUI(modelBankAddedSuccess.data)
                            } else {
                                showSnackView(
                                    getString(R.string.something_went_wrong),
                                    addBankSnackView
                                )
                            }
                            val wallpaperDirectory =
                                File(Environment.getExternalStorageDirectory().toString() + ImageUtils.IMAGE_DIRECTORY)
                            wallpaperDirectory.delete()

                        } else {
                            showSnackView(
                                getString(R.string.something_went_wrong),
                                addBankSnackView
                            )
                        }
                    } else {
                        showSnackView(
                            getString(R.string.something_went_wrong),
                            addBankSnackView
                        )
                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }


    private fun updateUI(data: FarmerDetails) {

        try {
            if (data != null) {

                layOfAddBankCancel.visibility = View.VISIBLE
                layOfBankAdd.visibility = View.VISIBLE
                addBankNoData.visibility = View.GONE

                edtIFSC.setText(data.ifsc_code)
                edtBankAccountNo.setText(data.bank_account_no)
                imgBankPassBook.visibility = View.GONE
                imgBankPassBookLive.visibility = View.VISIBLE

                btnSuccess.text = getString(R.string.update)
                txtAddNewBankTitle.text = getString(R.string.update_bank_info)
                title = getString(R.string.update_bank_info)
                if (data.bank_passbook_copy != null) {
                    imgBankPassBookLive.visibility = View.VISIBLE
                    ImageUtils.setImageLive(
                        imgBankPassBookLive,
                        Utils.IMAGE_URL_PATH + data.bank_passbook_copy,
                        this
                    )
                } else {
                    imgBankPassBookLive.visibility = View.GONE
                }
                bankPassBook = "" + data.bank_passbook_copy
                Utils.editTextFocusable(edtIFSC)
                Utils.editTextFocusable(edtBankAccountNo)
            } else {
                layOfAddBankCancel.visibility = View.GONE
                layOfBankAdd.visibility = View.GONE
                addBankNoData.visibility = View.VISIBLE
                addBankNoData.text = getString(R.string.farmer_details_not_found)
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    /**
     * Get and Set Drop down for Branch Name form Selected bank Name
     */

    private fun setBranchName(responseBody: Response<ResponseBody>) {
        try {
            val modelBranchName =
                Gson().fromJson(responseBody.body()?.string(), ModelBranchNamesDTO::class.java)
            if (modelBranchName != null) {
                if (modelBranchName.success!!) {
                    branchDetailedList = ArrayList()
                    branchList = ArrayList()
                    branchDetailedList = modelBranchName.data!!.branches
                    val count = if (branchDetailedList != null) branchDetailedList!!.size else 0
                    if (count != 0) {
                        branchDetailedList!!.forEachIndexed { _, branchesDTO ->
                            val branch = branchesDTO.bank_branch_name.toString()
                            branchList!!.add((branch))
                        }
                    }
                    branchList = Utils.findAndRemoveDuplicate(branchList!!)
                    branchList!!.add(0, getString(R.string.select_your_branch))
                    spBranchName.adapter =
                        ArrayAdapter(this, R.layout.single_view, R.id.singe_item, branchList)
                    if (farmerDetails!!.branch_name != null) {
                        spBranchName.setSelection(branchList!!.indexOf(farmerDetails!!.branch_name.toString()))
                    }

                } else {
                    showSnackView(modelBranchName.message!!, addBankSnackView)
                }
            } else {
                showSnackView(getString(R.string.something_went_wrong), addBankSnackView)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    /**
     * Get and Set Drop Down Spinner for Bank name
     */
    private fun setBankName(responseBody: Response<ResponseBody>) {
        try {
            val modelBankName =
                Gson().fromJson(responseBody.body()?.string(), ModelBankNameDTO::class.java)
            if (modelBankName != null) {
                if (modelBankName.success!!) {
                    bankDetailedList = ArrayList()
                    bankList = ArrayList()
                    bankDetailedList = modelBankName.data!!.banknames
                    val count =
                        if (bankDetailedList != null) bankDetailedList!!.size else 0
                    if (count != 0) {
                        for ((index, item) in bankDetailedList!!.withIndex()) {
                            val name = item.bank_name.toString()
                            bankList!!.add((name))
                        }
                    }
                    bankList = Utils.findAndRemoveDuplicate(bankList!!)
                    bankList!!.add(0, getString(R.string.select_your_bank))

                    spBankName.adapter =
                        ArrayAdapter(this, R.layout.single_view, R.id.singe_item, bankList)
                    Log.d("sdlksld", "" + farmerDetails!!.bank_name)
                    if (farmerDetails!!.bank_name != null) {
                        spBankName.setSelection(bankList!!.indexOf(farmerDetails!!.bank_name!!.toString()))
                    }
                } else {
                    showSnackView(modelBankName.message!!, addBankSnackView)
                }
            } else {
                showSnackView(getString(R.string.something_went_wrong), addBankSnackView)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, addBankSnackView)
    }
}
