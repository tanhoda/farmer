package com.tanhoda.farmer.views.schemeDetails

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.adapter.schemeDetails.ItemSchemeInfo
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.schemeDetails.ModelSchemeDetails
import com.tanhoda.farmer.utils.SessionManager
import kotlinx.android.synthetic.main.activity_bank_details.*
import okhttp3.ResponseBody
import retrofit2.Response

class SchemeDetails : BaseActivity(), ReadWriteAPI {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Scheme"
        setContentView(R.layout.activity_bank_details)
        fabAdd.hide()
        val map = HashMap<String, String>()
        map[ParamKey.FARMER_ID] = SessionManager.getObject(this).id!!.toString()
        map[ParamKey.DISTRICT] = SessionManager.getObject(this).district!!.toString()
        Connector.callBasic(this, map, this, ParamAPI.SCHEME_DETAILS)
    }


    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        try {
            val modelScheme =
                Gson().fromJson(responseBody.body()!!.string(), ModelSchemeDetails::class.java)
            if (modelScheme != null) {
                if (modelScheme.success!!) {
                    Log.d("schemeInfo", "" + modelScheme.data!!.schemes!!.size)
                    if (modelScheme.data!!.schemes!!.isNotEmpty()) {
                        rcyBankInfo.visibility = View.VISIBLE
                        noLandDetails.visibility = View.GONE
                        rcyBankInfo.layoutManager = LinearLayoutManager(this)
                        rcyBankInfo.adapter = ItemSchemeInfo(this, modelScheme.data.schemes!!)

                    } else {
                        noLandDetails.visibility = View.VISIBLE
                        rcyBankInfo.visibility = View.GONE
                        noLandDetails.text = getString(R.string.scheme_details_not_found)
                        showSnackView(modelScheme.message!!, layOfBankList)
                    }
                } else {
                    noLandDetails.visibility = View.VISIBLE
                    rcyBankInfo.visibility = View.GONE
                    noLandDetails.text = modelScheme.message!!
                    showSnackView(modelScheme.message!!, layOfBankList)
                }
            } else {
                rcyBankInfo.visibility = View.GONE
                noLandDetails.visibility = View.VISIBLE
                noLandDetails.text = getString(R.string.something_went_wrong)
                showSnackView(getString(R.string.something_went_wrong), layOfBankList)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        //showSnackView(message, layOfBankList)
        noLandDetails.visibility = View.VISIBLE
        rcyBankInfo.visibility = View.GONE
        noLandDetails.text = getString(R.string.something_went_wrong)
    }


}