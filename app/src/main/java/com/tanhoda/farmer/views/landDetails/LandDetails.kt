package com.tanhoda.farmer.views.landDetails

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.adapter.landItem.ItemLandDetails
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.landAndEditLand.ModelLandEdit
import com.tanhoda.farmer.model.login.FarmerDetails
import com.tanhoda.farmer.utils.SessionManager
import com.tanhoda.farmer.utils.Utils.Companion.REGISTERED_LAND_COUNT


import kotlinx.android.synthetic.main.activity_bank_details.*
import okhttp3.ResponseBody
import retrofit2.Response

class LandDetails : BaseActivity(), ReadWriteAPI {
    var farmerDetails: FarmerDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = getString(R.string.land_details)

        setContentView(R.layout.activity_bank_details)
        noLandDetails.text = getString(R.string.no_land_details)
        farmerDetails = SessionManager.getObject(this@LandDetails)

        fabAdd.setOnClickListener {
            val farmerType = SessionManager.getObject(this).farmer_type.toString()
            /*if (validateFarmerTypeLandAllow(
                    REGISTERED_LAND_COUNT,
                    farmerType,
                    layOfBankList,
                    this
                )
            ) {*/
            startActivity(Intent(this, AddNewLandDetails::class.java))
            // }
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            REGISTERED_LAND_COUNT = 0.0
            val map = HashMap<String, String>()
            map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(this)
            map[ParamKey.VILLAGE] = farmerDetails!!.village!!

            Connector.callBasic(this, map, this, ParamAPI.READ_LAND_DETAILS)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        try {
            if (responseBody.body() != null) {
                val modelLandDetails =
                    Gson().fromJson(responseBody.body()?.string(), ModelLandEdit::class.java)
                if (modelLandDetails != null) {
                    if (modelLandDetails.status!!) {
                        var totalArea = 0.0
                        if (modelLandDetails.data!!.isNotEmpty()) {
                            //Log.d("sldkskd", "0 " + modelLandDetails.data!!.size)
                            for ((index, items) in modelLandDetails.data.withIndex()) {
                                totalArea += items!!.totalArea!!.toDouble()
                            }
                            REGISTERED_LAND_COUNT = totalArea
                            noLandDetails.visibility = View.GONE
                            rcyBankInfo.visibility = View.VISIBLE
                            rcyBankInfo.layoutManager =
                                LinearLayoutManager(this@LandDetails)
                            rcyBankInfo.adapter =
                                ItemLandDetails(this, modelLandDetails.data)
                        } else {
                            rcyBankInfo.visibility = View.GONE
                            noLandDetails.visibility = View.VISIBLE
                            noLandDetails.text = "Land Details Not Found"
                        }
                    } else {
                        rcyBankInfo.visibility = View.GONE
                        noLandDetails.visibility = View.VISIBLE
                        noLandDetails.text = modelLandDetails.message!!
                    }
                } else {
                    rcyBankInfo.visibility = View.GONE
                    noLandDetails.visibility = View.VISIBLE
                    noLandDetails.text = getString(R.string.something_went_wrong)
                    showSnackView(getString(R.string.something_went_wrong), layOfBankList)
                }
            } else {
                rcyBankInfo.visibility = View.GONE
                noLandDetails.visibility = View.VISIBLE
                noLandDetails.text = getString(R.string.something_went_wrong)
                showSnackView(getString(R.string.something_went_wrong), layOfBankList)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            rcyBankInfo.visibility = View.GONE
            noLandDetails.visibility = View.VISIBLE
            noLandDetails.text = ex.message!!
        }

    }

    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, layOfBankList)
        noLandDetails.text = message
        noLandDetails.visibility = View.VISIBLE
        rcyBankInfo.visibility = View.GONE
    }
}
