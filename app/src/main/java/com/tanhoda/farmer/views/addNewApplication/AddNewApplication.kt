package com.tanhoda.farmer.views.addNewApplication


import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI.Companion.BLOCK_LIST_PRO
import com.tanhoda.farmer.api.ParamAPI.Companion.COMPONENT_LIST
import com.tanhoda.farmer.api.ParamAPI.Companion.CREATE_NEW_APPLICATION
import com.tanhoda.farmer.api.ParamAPI.Companion.INIT_VALUES_FOR_CREATE_APPLICATION
import com.tanhoda.farmer.api.ParamAPI.Companion.SUB_COMPONENT_AREA
import com.tanhoda.farmer.api.ParamAPI.Companion.SUB_COMPONENT_LIST
import com.tanhoda.farmer.api.ParamAPI.Companion.SUB_DIVISION_LIST_PRO
import com.tanhoda.farmer.api.ParamAPI.Companion.SURVEY_LIST_PRO
import com.tanhoda.farmer.api.ParamAPI.Companion.VILLAGE_LIST_PRO
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ParamKey.Companion.ADANGAL_COPY_IMG
import com.tanhoda.farmer.api.ParamKey.Companion.BLOCK_ID
import com.tanhoda.farmer.api.ParamKey.Companion.COMPONENT
import com.tanhoda.farmer.api.ParamKey.Companion.COMPONENT_ID
import com.tanhoda.farmer.api.ParamKey.Companion.FARMER_ID
import com.tanhoda.farmer.api.ParamKey.Companion.HOTNET_ID
import com.tanhoda.farmer.api.ParamKey.Companion.PRE_SUBSIDY_AMOUNT
import com.tanhoda.farmer.api.ParamKey.Companion.SCHEME_COUNT
import com.tanhoda.farmer.api.ParamKey.Companion.SCHEME_ID
import com.tanhoda.farmer.api.ParamKey.Companion.SOIL_CERTIFICATE_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.SUBSIDY_AMOUNT
import com.tanhoda.farmer.api.ParamKey.Companion.SUB_COMPONENT_ID
import com.tanhoda.farmer.api.ParamKey.Companion.SURVEY_NO
import com.tanhoda.farmer.api.ParamKey.Companion.VAO_CERTIFICATE_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.VILLAGE_ID
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.addNewApplication.categoryComponent.DataComponentDTO
import com.tanhoda.farmer.model.addNewApplication.categoryComponent.ModelComponentCategoryDTO
import com.tanhoda.farmer.model.addNewApplication.schemCountOrAreaApplied.SchemeAreaCountDTO
import com.tanhoda.farmer.model.addNewApplication.schemeList.DistrictsDTO
import com.tanhoda.farmer.model.addNewApplication.schemeList.ModelSchemeListDTO
import com.tanhoda.farmer.model.addNewApplication.schemeList.SchemeDTO
import com.tanhoda.farmer.model.addNewApplication.subComponent.DataSubComponentDTO
import com.tanhoda.farmer.model.addNewApplication.subComponent.ModelSubComponentDTO
import com.tanhoda.farmer.model.addNewApplication.subDivision.DataSubDivisionDTO
import com.tanhoda.farmer.model.addNewApplication.subDivision.ModelSubDivisionDTO
import com.tanhoda.farmer.model.addNewApplication.subDivision.newSubDivision.DataItem
import com.tanhoda.farmer.model.addNewApplication.subDivision.newSubDivision.PojoSubDivision
import com.tanhoda.farmer.model.addNewApplication.surveyNumbers.ModelSurveyNumbersDTO
import com.tanhoda.farmer.model.login.FarmerDetails
import com.tanhoda.farmer.model.register.blockDetails.BlockDTO
import com.tanhoda.farmer.model.register.blockDetails.ModelBlockDTO
import com.tanhoda.farmer.model.register.registerSuccess.ModelRegisterSuccess
import com.tanhoda.farmer.model.register.village.ModelVillageDTO
import com.tanhoda.farmer.model.register.village.VillageDTO
import com.tanhoda.farmer.utils.*
import com.tanhoda.farmer.utils.ImageUtils.IMAGE_DIRECTORY
import com.tanhoda.farmer.utils.permissions.CallbackPermission
import com.tanhoda.farmer.utils.permissions.PermissionDescription
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.ADANGAL_COPY
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.BANK_LOAN
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.SOIL_TEST
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.VAO_CERTIFICATE
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.checkPermissions
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.choosePhotoFromGallery
import kotlinx.android.synthetic.main.activity_add_new_application.*
import kotlinx.android.synthetic.main.add_more.*
import kotlinx.android.synthetic.main.add_more.view.*
import kotlinx.android.synthetic.main.add_more_any_govt.*
import kotlinx.android.synthetic.main.add_more_any_govt.view.*
import kotlinx.android.synthetic.main.add_more_layout_area_proposed.*
import kotlinx.android.synthetic.main.add_more_layout_area_proposed.view.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File


open class AddNewApplication : BaseActivity(), View.OnClickListener, CallbackPermission,
    ReadWriteAPI {

    var bankLoan = ""
    var adangalCopy = ""
    var vaoCertificate = ""
    var soilTest = ""
    var previoslySubsidy = ""
    var bankLoanOpen = ""
    var soilwater_test = ""

    var readWriteAPI: ReadWriteAPI? = null
    var appDetailsToAddMore = ArrayList<HashMap<String, Any>>()
    var addMorePreviouslySubsidy = ArrayList<HashMap<String, Any>>()

    var schemeDetails: List<SchemeDTO?>? = null
    var schemeInfo: ArrayList<String>? = null

    var componentDetails: List<DataComponentDTO?>? = null
    var componentInfo: ArrayList<String>? = null

    var subComponentDetails: List<DataSubComponentDTO?>? = null
    var subComponentInfo: ArrayList<String>? = null

    var districtDetails: List<DistrictsDTO?>? = null
    var districtInfo: ArrayList<String>? = null
    var isChek: Boolean = false

    var blockDetails = ArrayList<BlockDTO?>()
    var blockInfo = ArrayList<String>()

    var villageDetails = ArrayList<VillageDTO?>()
    var villageInfo = ArrayList<String>()

    var proposedCropInfo: ArrayList<String>? = null
    var listOfSurveyInfo: ArrayList<String>? = null
    var blockId = -1
    var districtId = -1
    var villageId = -1
    var surveyId = -1
    var schemeId = -1
    var categoryORSubComponentID = -1
    var componentId = -1
    var subDivisionID = -1
    var farmerDetails: FarmerDetails? = null
    var subDivisionDetails: ArrayList<DataItem?>? = null
    var subDivisionInfo: ArrayList<String>? = null
    var land_area = 0.0
    var minLimit = 0.0
    var maxLimit = 0.0


    var subsidyUpto500 = 0.0
    var subsidy500to1008 = 0.0
    var subsidy1008to2080 = 0.0
    var subsidy2080to4000 = 0.0

    var unit = ""
    var subsidyAmount = 0.0
    var subsidyPercentage = 0.0
    var schemecount = 0.0

    var isThere = false
    var schemeName = ""
    var subComponent = ""
    var category = ""
    var areaapplied = 0.0
    var areaa = 0.0


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.add_new_application)

        setContentView(R.layout.activity_add_new_application)

        initCallbackPermission(this)
        readWriteAPI = this

        farmerDetails = SessionManager.getObject(this@AddNewApplication)

        try {
            if (intent.hasExtra(ParamKey.IS_THERE)) {
                isThere = intent.getBooleanExtra(ParamKey.IS_THERE, false)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        try {
            if (isThere) {
                if (intent.hasExtra(ParamKey.SCHEME_NAME)) {
                    schemeName = intent.getStringExtra(ParamKey.SCHEME_NAME)
                }
                if (intent.hasExtra(ParamKey.SCHEME_CATEGORY)) {
                    category = intent.getStringExtra(ParamKey.SCHEME_CATEGORY)

                }
                if (intent.hasExtra(ParamKey.SUB_COMPONENT_NAME)) {
                    subComponent = intent.getStringExtra(ParamKey.SUB_COMPONENT_NAME)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        edtAreProposed.isFocusedByDefault = false

        /*Log.d(
            "subComponentsds",
            " isThere: $isThere  subComponent :  $subComponent  category : $category schemeName:$schemeName "
        )*/

        btnSuccess.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
        imgBankLoan.setOnClickListener(this)
        imgVAOCertificate.setOnClickListener(this)
        imgAdangalCopySlection.setOnClickListener(this)
        imgSoil_WaterTest.setOnClickListener(this)
        layOfAddMore.setOnClickListener(this)

        //layOfBankLoanIfYes.animate().alpha(0.0f)
        layOfBankLoanIfYes.visibility = View.GONE
        // iplAnyOtherLoan.animate().alpha(0.0f)
        layOfAnyGovt.visibility = View.GONE

        edtNameOfTheScheme.setOnClickListener { spNameOfScheme.performClick() }
        edtNameOfTheComponent.setOnClickListener { spNameOfTheComponent.performClick() }
        edtNameOfTheSubcomponent.setOnClickListener { spNameOfTheSubcomponent.performClick() }
        edtDistrict.setOnClickListener { spDistrict.performClick() }
        edtBlockAddMore.setOnClickListener { spBlockAddMore.performClick() }
        edtVillageAddMore.setOnClickListener { spVillageAddMore.performClick() }
        edtSubDivisionAddMore.setOnClickListener { spSubDivisionAddMore.performClick() }
        edtSurveyNoAddMore.setOnClickListener { spSurveyNoAddMore.performClick() }
        edtProposedCrop.setOnClickListener { spProposedCrop.performClick() }

        val map = HashMap<String, String>()
        map[FARMER_ID] = SessionManager.getFarmerId(this)
        Connector.callBasic(this, map, readWriteAPI!!, INIT_VALUES_FOR_CREATE_APPLICATION)


        spDistrict.setSelection(spDistrict.selectedItemPosition, false)
        spBlockAddMore.setSelection(spBlockAddMore.selectedItemPosition, false)
        spVillageAddMore.setSelection(spVillageAddMore.selectedItemPosition, false)
        spSurveyNoAddMore.setSelection(spSurveyNoAddMore.selectedItemPosition, false)
        spSubDivisionAddMore.setSelection(spSubDivisionAddMore.selectedItemPosition, false)
        txtAddMoreAnyGovt.setOnClickListener(this)

        //Utils.nonEditable(edtTotalAreaProposedIn)

        gSubSidyYes.setOnCheckedChangeListener { _, isChecked ->
            isChek = isChecked
            if (isChecked) {
                addMorePreviouslySubsidy.clear()
            }
        }

        edtCostProposed.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {

                    try {
                        val costProposed = s.toString()
                        val areaProposed = edtTotalAreaProposedIn.text.toString()

                        if (costProposed.isNotEmpty()) {
                            calculateSubsidyAmount(costProposed, areaProposed)
                        } else {
                            edtSubsidyAmount.setText("")
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        hideKeyboard()
                        showSnackView(
                            MessageUtils.getFailureMessage(ex.message!!),
                            lafOfAddNewSnackView
                        )
                    }
                }

            })

        /* edtAreProposed.setOnFocusChangeListener { v, hasFocus ->
             if (!hasFocus) {
                 try {
                     var input = edtAreProposed.text.toString()
                     if (input.isNotEmpty()) {
                         if (input == ".") {
                             input = "0$input"
                         }
                         Log.d("inputsdsd", "" + input)
                         var areaProsed = input.toDouble()//Utils.convertDouble(input)//
                         Log.d(
                             "areaProsedsdsd",
                             "start $areaProsed land_area $land_area schemecount:$schemecount minLimit: $minLimit maxLimit: $maxLimit"
                         )

                         if (areaProsed != 0.0) {
                             var areaCount = 0.0
                             if (appDetailsToAddMore.size != 0) {
                                 for ((index, item) in appDetailsToAddMore.withIndex()) {
                                     if (item[ParamKey.AREA_PROPOSED].toString().isNotEmpty()) {
                                         areaCount += item[ParamKey.AREA_PROPOSED].toString()
                                             .toDouble()
                                     }
                                 }
                             }
                             Log.d("areaCountsd", "" + areaCount)

                             areaProsed += schemecount + areaapplied + areaCount

                             Log.d("areaProsedsdsd", "sa $areaProsed areaapplied $land_area")
                             if (areaProsed <= land_area) {
                                 //areaProsed = countLandArea(areaProsed - schemecount - areaCount)
                                 areaProsed = areaProsed - areaapplied-areaCount
                                 Log.d(
                                     "itemvalueds",
                                     "areaProsed:  $areaProsed minLimit: $minLimit maxLimit: $maxLimit"
                                 )

                                 if (areaProsed in minLimit..maxLimit) {
                                     MessageUtils.dismissSnackBar(snackView)
                                     edtTotalAreaProposedIn.setText(
                                         "" + Utils.convertDouble(
                                             "" + ( areaProsed+areaCount)
                                         )
                                     )
                                     try {
                                         val costProposed = edtCostProposed.text.toString()
                                         val areaProposed =
                                             edtTotalAreaProposedIn.text.toString()

                                         calculateSubsidyAmount(costProposed, areaProposed)
                                         //edtSubsidyAmount.setText("")
                                     } catch (ex: Exception) {
                                         ex.printStackTrace()
                                     }
                                 } else {
                                     //hideKeyboard()
                                     edtAreProposed.setText("")
                                     edtTotalAreaProposedIn.setText("")
                                     showSnackView(
                                         "Limit Upto $minLimit to $maxLimit",
                                         lafOfAddNewSnackView
                                     )
                                 }

                             } else {
                                 //hideKeyboard()
                                 edtAreProposed.setText("")
                                 edtTotalAreaProposedIn.setText("")
                                 showSnackView(
                                     "Reached Maximum Land Area $land_area",
                                     lafOfAddNewSnackView
                                 )
                             }
                         }
                     } else {
                         var areaCount = 0.0
                         if (appDetailsToAddMore.size != 0) {
                             for ((index, item) in appDetailsToAddMore.withIndex()) {
                                 if (item[ParamKey.AREA_PROPOSED].toString().isNotEmpty()) {
                                     areaCount += item[ParamKey.AREA_PROPOSED].toString().toDouble()
                                 }
                             }
                         }
                         edtTotalAreaProposedIn.setText("" + Utils.convertDouble("" + areaCount))
                     }
                 } catch (ex: java.lang.Exception) {
                     ex.printStackTrace()
                 }
             }
         }*/

        edtAreProposed.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                @SuppressLint("SetTextI18n")
                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    try {

                        Log.d(
                            "itemvalueds",
                            "Min: $minLimit Max: $maxLimit schemecount: $schemecount LandAre :$land_area"
                        )
                        val subDivision = spSubDivisionAddMore.selectedItem.toString()

                        var input = s.toString()
                        if (input.isNotEmpty()) {
                            if (subDivision != getString(R.string.select_sub_division)) {
                                if (input == ".") {
                                    input = "0$input"
                                }
                                var areaProsed = input.toDouble()//Utils.convertDouble(input)//
                                Log.d(
                                    "areaProsedsdsd",
                                    "start $areaProsed land_area $land_area"
                                )
                                if (areaProsed != 0.0) {
                                    if (areaProsed in minLimit..maxLimit) {
                                        var areaCount = 0.0
                                        if (appDetailsToAddMore.size != 0) {
                                            for ((index, item) in appDetailsToAddMore.withIndex()) {
                                                if (item[ParamKey.AREA_PROPOSED].toString().isNotEmpty()) {
                                                    areaCount += item[ParamKey.AREA_PROPOSED].toString()
                                                        .toDouble()
                                                }
                                            }
                                        }
                                        areaProsed += areaapplied + areaCount
                                        Log.d(
                                            "areaProsedsdsd",
                                            " 2o2o schemecount $schemecount areaCount$areaCount areaProsed $areaProsed land_area$land_area areaapplied$areaapplied minLimit $minLimit maxLimit$maxLimit areaapplied$areaapplied"
                                        )
                                        if (areaProsed <= land_area) {
                                            if (areaProsed in minLimit..maxLimit) {
                                                Log.d("areaProsedsdsd", "true")
                                                MessageUtils.dismissSnackBar(snackView)
                                                edtTotalAreaProposedIn.setText(
                                                    "" + Utils.convertDouble(
                                                        "" + (areaProsed - areaapplied)
                                                    )
                                                )
                                                try {
                                                    val costProposed =
                                                        edtCostProposed.text.toString()
                                                    val areaProposed =
                                                        edtTotalAreaProposedIn.text.toString()
                                                    calculateSubsidyAmount(
                                                        costProposed,
                                                        areaProposed
                                                    )
                                                } catch (ex: Exception) {
                                                    ex.printStackTrace()
                                                }
                                            } else {
                                                Log.d("areaProsedsdsd", "false")
                                                checkMinMax()
                                            }
                                        } else {
                                            hideKeyboard()
                                            edtAreProposed.setText("")
                                            edtTotalAreaProposedIn.setText("")
                                            showSnackView(
                                                "Reached Maximum Land Area $land_area",
                                                lafOfAddNewSnackView
                                            )
                                        }

                                    } else {
                                        checkMinMax()
                                    }
                                }
                            } else {
                                edtAreProposed.setText("")
                                showSnackView(
                                    getString(R.string.select_sub_division),
                                    lafOfAddNewSnackView
                                )
                            }
                        } else {
                            var areaCount = 0.0
                            if (appDetailsToAddMore.size != 0) {
                                for ((index, item) in appDetailsToAddMore.withIndex()) {
                                    areaCount += item[ParamKey.AREA_PROPOSED].toString()
                                        .toDouble()
                                }
                            }
                            edtTotalAreaProposedIn.setText("" + Utils.convertDouble("" + areaCount))
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(
                            MessageUtils.getFailureMessage(ex.message!!),
                            lafOfAddNewSnackView
                        )
                    }
                }
            })

        spSubDivisionAddMore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val input = spSubDivisionAddMore.selectedItem.toString()
                    val survey = spSurveyNoAddMore.selectedItem.toString()
                    Log.d("surveysdsd", "survey $survey input $input ")
                    if (input != getString(R.string.select_sub_division)) {
                        MessageUtils.dismissSnackBar(snackView)
                        appDetailsToAddMore.forEachIndexed { index, hashMap ->
                            Log.d(
                                "surveysdsd",
                                "" + survey + " input $input loopSurvey ${hashMap[SURVEY_NO]} loopsub_division ${hashMap["division"]}"
                            )
                            if (hashMap[ParamKey.SURVEY_NO].toString() == survey) {
                                if (hashMap["division"].toString() == input) {
                                    spSubDivisionAddMore.setSelection(0)
                                    showSnackView(
                                        getString(R.string.sub_division_number_exist),
                                        lafOfAddNewSnackView
                                    )
                                    edtAreProposed.setText("")
                                    return@forEachIndexed
                                }
                            }
                        }

                        for ((index, item) in subDivisionDetails!!.withIndex()) {
                            val items = item!!.subdivision!!
                            if (items.subdivision.toString() == input) {
                                subDivisionID = items.id!!
                                if (items.land_area.toString().isNotEmpty()) {
                                    land_area = items.land_area!!.toDouble()
                                    areaa = item!!.area!!
                                    areaapplied = item.areaapplied!!
                                }
                            }
                        }
                    }

                    Log.d(
                        "land_areasds",
                        "init $land_area $unit schemecount: $schemecount  areaapplied $areaapplied  areaa $areaa"
                    )

                    if (unit.toLowerCase() == "Square Meter".toLowerCase()) {
                        land_area *= 10000
                        areaapplied *= 10000
                        edtAreProposed.setText("")
                        iplTotalAreaProposedIn.hint = getString(R.string.total_area_proposed_in_sqr)
                        iplAreaProposed.hint = getString(R.string.area_proposed_sqr)
                    } else {
                        iplTotalAreaProposedIn.hint = getString(R.string.total_area_proposed_in_ha)
                        iplAreaProposed.hint = getString(R.string.area_proposed)
                    }
                    Log.d("land_areasds===>", "" + land_area)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }

        spDistrict.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val district = spDistrict.selectedItem.toString()
                    if (district != getString(R.string.select_district)) {
                        //Call API For Block Details
                        val map = HashMap<String, String>()
                        for ((index, item) in districtDetails!!.withIndex()) {
                            if (district == item!!.district_name) {
                                districtId = item.id!!
                            }
                        }
                        if (districtId != -1) {
                            map[FARMER_ID] = SessionManager.getFarmerId(this@AddNewApplication)
                            map[ParamKey.DISTRICT_ID] = districtId.toString()
                            Connector.callBasic(
                                this@AddNewApplication,
                                map,
                                readWriteAPI!!,
                                BLOCK_LIST_PRO
                            )
                        }
                    } else {
                        //setDistrictDefault()
                        setBlockDefault()
                        //setVillageDefault()

                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }

        spBlockAddMore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val blockName = spBlockAddMore.selectedItem.toString()
                    Log.d("blockNamesd", "" + blockName)
                    if (blockName != getString(
                            R.string.select_block
                        )
                    ) {
                        val map = HashMap<String, String>()
                        var injex = 0

                        for ((index, item) in blockDetails.withIndex()) {
                            if (item!!.block_name == blockName) {
                                Log.d("blockNamesd", "" + blockName + "  " + item.block_name)
                                blockId = item.id!!
                                break
                            }
                        }
                        if (blockId != -1) {
                            map[FARMER_ID] = SessionManager.getFarmerId(this@AddNewApplication)
                            map[BLOCK_ID] = blockId.toString()
                            Connector.callBasic(
                                this@AddNewApplication,
                                map,
                                readWriteAPI!!,
                                VILLAGE_LIST_PRO
                            )
                        } else {
                            showSnackView(
                                getString(R.string.please_select_block),
                                lafOfAddNewSnackView
                            )
                        }
                    } else {
                        setVillageDefault()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        }

        spVillageAddMore.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val village = spVillageAddMore.selectedItem.toString()
                        Log.d("blockNamesd", "village: " + village)
                        if (village != getString(R.string.select_village)) {
                            for ((index, items) in villageDetails.withIndex()) {
                                if (village == items?.village_name) {
                                    villageId = items!!.id!!
                                }
                            }

                            if (villageId != -1) {
                                map[FARMER_ID] = SessionManager.getFarmerId(this@AddNewApplication)
                                map[VILLAGE_ID] = villageId.toString()
                                Connector.callBasic(
                                    this@AddNewApplication,
                                    map,
                                    readWriteAPI!!,
                                    SURVEY_LIST_PRO
                                )
                            } else {
                                showSnackView(
                                    getString(R.string.select_your_village),
                                    lafOfAddNewSnackView
                                )
                            }
                        } else {
                            setSurveyNumber()
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(ex.message!!, lafOfAddNewSnackView)
                    }

                }
            }


        spSurveyNoAddMore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val survey = spSurveyNoAddMore.selectedItem.toString()

                    Log.d("blockNamesd", "survey: " + survey)
                    if (survey != getString(R.string.select_survey_no)) {
                        map[FARMER_ID] = SessionManager.getFarmerId(this@AddNewApplication)
                        map[VILLAGE_ID] = villageId.toString()
                        map[SURVEY_NO] = survey
                        Connector.callBasic(
                            this@AddNewApplication,
                            map,
                            readWriteAPI!!,
                            SUB_DIVISION_LIST_PRO
                        )

                    } else {
                        setSubDivision()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    showSnackView(ex.message!!, lafOfAddNewSnackView)
                }
            }
        }

        spNameOfScheme.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val schemeName = spNameOfScheme.selectedItem.toString()
                    Log.d("blockNamesd", "schemeName: $schemeName")
                    schemeId = -1
                    if (schemeName != getString(R.string.select_scheme)) {
                        var hotnet_id = ""
                        for ((index, item) in schemeDetails!!.withIndex()) {
                            if (schemeName == item!!.scheme_name) {
                                schemeId = item.id!!
                                hotnet_id = item.hortnet_id!!
                            }
                        }
                        Log.d("hotnet_idsd", "" + hotnet_id)
                        if (hotnet_id.isNotEmpty() && hotnet_id.toLowerCase() == "yes".toLowerCase()) {
                            edtHotNetId.visibility = View.VISIBLE
                        } else {
                            edtHotNetId.visibility = View.GONE
                        }

                        val map = HashMap<String, String>()
                        map[SCHEME_ID] = schemeId.toString()

                        Connector.callBasic(
                            this@AddNewApplication, map, readWriteAPI!!,
                            COMPONENT_LIST
                        )
                    } else {
                        edtHotNetId.visibility = View.GONE
                        //setDistrictDefault()
                        setTheComponent()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }


        spNameOfTheComponent.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    categoryORSubComponentID = -1
                    val componentName = spNameOfTheComponent.selectedItem.toString()
                    if (componentName != getString(R.string.select_category)) {
                        for ((index, item) in componentDetails!!.withIndex()) {
                            if (componentName == item!!.category) {
                                categoryORSubComponentID = item.id!!
                            }
                        }

                        val map = HashMap<String, String>()
                        map[COMPONENT_ID] = categoryORSubComponentID.toString()
                        Connector.callBasic(
                            this@AddNewApplication, map, readWriteAPI!!,
                            SUB_COMPONENT_LIST
                        )
                    } else {
                        setTheSubComponent()
                        appDetailsToAddMore.clear()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }

        spNameOfTheSubcomponent.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val subComponent = spNameOfTheSubcomponent.selectedItem.toString()
                        componentId = -1
                        if (subComponent != getString(R.string.select_sub_componet)) {
                            for ((index, item) in subComponentDetails!!.withIndex()) {
                                if (item!!.component_name == subComponent) {
                                    try {
                                        componentId = item!!.id!!
                                        if (item.min_limit!!.isNotEmpty()) {
                                            minLimit = item.min_limit!!.toDouble()
                                        }
                                        if (item.max_limit!!.isNotEmpty()) {
                                            maxLimit = item.max_limit!!.toDouble()
                                        }

                                        soilwater_test = item.soilwater_test.toString()
                                        if (soilwater_test.toString().toLowerCase() == "Yes".toLowerCase()) {
                                            layOfSoil_WaterTest.visibility = View.VISIBLE
                                        } else {
                                            layOfSoil_WaterTest.visibility = View.GONE
                                        }

                                        //Utils.enableEditable(edtTotalAreaProposedIn)
                                        unit = item.unit!!
                                        subsidyAmount = item.subsidy_amount!!
                                        subsidyPercentage = item.subsidy_percentage!!
                                        try {

                                            if (item.subsidyupto500!!.isNotEmpty() && item.subsidyupto500 != null) {
                                                subsidyUpto500 = item.subsidyupto500.toDouble()


                                            }
                                            if (item.subsidy500to1008!!.isNotEmpty() && item.subsidy500to1008 != null) {
                                                subsidy500to1008 =
                                                    item.subsidy500to1008.toDouble()
                                            }
                                            if (item.subsidy1008to2080!!.isNotEmpty() && item.subsidy1008to2080 != null) {
                                                subsidy1008to2080 =
                                                    item.subsidy1008to2080.toDouble()
                                            }
                                            if (item.subsidy2080to4000!!.isNotEmpty() && item.subsidy2080to4000 != null) {
                                                subsidy2080to4000 =
                                                    item.subsidy2080to4000.toDouble()
                                            }
                                        } catch (ex: java.lang.Exception) {
                                            ex.printStackTrace()
                                        }

                                        Log.d("subsidyAmountsdsd", "" + subsidyAmount)
                                        val map = HashMap<String, String>()
                                        map[FARMER_ID] =
                                            SessionManager.getFarmerId(this@AddNewApplication)
                                        map[SUB_COMPONENT_ID] = componentId.toString()

                                        Connector.callBasic(
                                            this@AddNewApplication,
                                            map,
                                            readWriteAPI!!,
                                            SUB_COMPONENT_AREA
                                        )

                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }
                                }

                            }
                        } else {
                            addMorePreviouslySubsidy.clear()
                            appDetailsToAddMore.clear()
                            anyGovtSubsidy.removeAllViews()
                            layOfDynamicAreaProposed.removeAllViews()
                            edtTotalAreaProposedIn.setText("")
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }


            }


        rg_bank_loan.setOnCheckedChangeListener { group, checkedId ->
            val rb = group.findViewById<View>(checkedId) as RadioButton
            if (null != rb && checkedId > -1) {
                bankLoanOpen = rb.text.toString()
                if (getString(R.string.yes) == bankLoanOpen) {
                    layOfBankLoanIfYes.visibility = View.VISIBLE
                } else {
                    layOfBankLoanIfYes.visibility = View.GONE
                }
            } else {
                layOfBankLoanIfYes.visibility = View.GONE
            }
        }

        rg.setOnCheckedChangeListener { group, checkedId ->
            val rb = group.findViewById<View>(checkedId) as RadioButton
            if (null != rb && checkedId > -1) {
                previoslySubsidy = rb.text.toString()
                if (getString(R.string.yes) == previoslySubsidy) {
                    previoslySubsidy = "Yes"
                    layOfAnyGovt.visibility = View.VISIBLE
                } else {
                    previoslySubsidy = "No"
                    layOfAnyGovt.visibility = View.GONE
                }
            } else {
                layOfAnyGovt.visibility = View.GONE
            }
        }


    }

    private fun checkMinMax() {

        if (minLimit == 0.0 && maxLimit == 0.0) {
            edtAreProposed.setText("")
            edtTotalAreaProposedIn.setText("")
            showSnackView(
                getString(R.string.some_fileds_missing),
                lafOfAddNewSnackView
            )
        } else {
            //hideKeyboard()
            //edtAreProposed.setText("")
            edtTotalAreaProposedIn.setText("")
            showSnackView(
                "Limit Upto $minLimit to $maxLimit",
                lafOfAddNewSnackView
            )
        }
    }


    private fun addMoreForAnyGovtSubsidyDynamic(map: HashMap<String, Any>) {

        try {
            addMorePreviouslySubsidy.add(map)

            edtSchemeName.setText("")
            edtComponent.setText("")
            edtTotalAmount.setText("")

            val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.add_more_any_govt, null)
            val layOfAddOrRemove = view.findViewById<TextView>(R.id.txtAddMoreAnyGovt)
            val lp = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            lp.setMargins(24, 12, 24, 12)
            view.layoutParams = lp
            layOfAddOrRemove.text = getString(R.string.remove_item)
            layOfAddOrRemove.setTextColor(resources.getColor(R.color.colorRed))
            layOfAddOrRemove.setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_remove_circle_red,
                0
            )
            Log.d("asklaks", "" + map)

            view.edtSchemeName.setText(map[ParamKey.SCHEME_NAME]!!.toString())
            view.edtComponent.setText(map[ParamKey.COMPONENT]!!.toString())
            view.edtTotalAmount.setText(map[ParamKey.TOTAL_AMOUNT]!!.toString())

            layOfAddOrRemove.setOnClickListener {
                anyGovtSubsidy.removeView(view)
                addMorePreviouslySubsidy.remove(map)
                MessageUtils.showSnackBarRemove(
                    this@AddNewApplication,
                    lafOfAddNewSnackView,
                    getString(R.string.item_remove)
                )
            }

            anyGovtSubsidy.addView(view)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun calculateSubsidyAmount(costProposed: String, areaProposed: String) {
        try {
            if (costProposed.isNotEmpty()) {
                val inputConvert = costProposed.toDouble()
                var costCalc1 = 0.0
                var costCalc2 = 0.0
                val totalAreaProposed: Double

                if (areaProposed.isNotEmpty()) {
                    totalAreaProposed = areaProposed.toDouble()
                } else {
                    hideKeyboard()
                    edtCostProposed.setText("")
                    showSnackView(
                        getString(R.string.total_area_cant_be_empty),
                        lafOfAddNewSnackView
                    )
                    return
                }

                Log.d(
                    "totalAreaProposed",
                    "totalAreaProposed:  $totalAreaProposed subsidyAmount: $subsidyAmount  inputConvert ; $inputConvert  subsidyPercentage: $subsidyPercentage"
                )

                try {
                    if (unit.toLowerCase() == "Square Meter".toLowerCase()) {
                        Log.d("subsidyupto50045787", "" + subsidyUpto500)
                        if (subsidyUpto500 != 0.0 && totalAreaProposed < 500) {
                            costCalc1 = totalAreaProposed * subsidyUpto500
                        } else if (subsidy500to1008 != 0.0 && totalAreaProposed < 1008) {
                            costCalc1 = totalAreaProposed * subsidy500to1008
                        } else if (subsidy1008to2080 != 0.0 && totalAreaProposed < 2080) {
                            costCalc1 = totalAreaProposed * subsidy1008to2080
                        } else if (subsidy2080to4000 != 0.0 && totalAreaProposed < 4000) {
                            costCalc1 = totalAreaProposed * subsidy2080to4000
                        } else {
                            costCalc1 = totalAreaProposed / 1000 * subsidyAmount
                        }
                    } else {
                        Log.d("subsidyupto50045787", "ha")
                        costCalc1 = totalAreaProposed * subsidyAmount
                    }
                    costCalc2 = inputConvert * subsidyPercentage / 100
                    Log.d("costCalc2sd", "$costCalc2 costCalc2: $costCalc2")
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
                Log.d(
                    "costProposedsdsd",
                    "costProposed : $costProposed areaProposed : $areaProposed"
                )

                if (costProposed.isNotEmpty() && areaProposed.isNotEmpty()) {
                    if (costCalc1 <= costCalc2) {
                        edtSubsidyAmount.setText("" + Utils.convertDouble("" + costCalc1))
                    } else {
                        edtSubsidyAmount.setText("" + Utils.convertDouble("" + costCalc2))
                    }
                } else {
                    edtSubsidyAmount.setText("")
                }

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun countLandArea(areaProsed: Double): Double {
        var areaProsed1 = 0.0
        try {
            var areaCount = 0.0
            if (appDetailsToAddMore.size != 0) {
                for ((index, item) in appDetailsToAddMore.withIndex()) {
                    if (item[ParamKey.AREA_PROPOSED].toString().isNotEmpty()) {
                        areaCount += item[ParamKey.AREA_PROPOSED].toString()
                            .toDouble()
                    }
                }
            }
            areaProsed1 = areaProsed + areaCount
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return areaProsed1
    }

    private fun hideKeyboard() {
        Utils.hideSoftKeyboard(
            this@AddNewApplication.currentFocus,
            this@AddNewApplication
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSuccess -> {
                addNewApplication()
            }
            R.id.btnCancel -> {
                onBackPressed()
            }
            R.id.imgBankLoan -> {
                checkPermissions(
                    BANK_LOAN, this, this as CallbackPermission,
                    PermissionDescription.permissionForCameraAndStorage(this),
                    arrayOf(
                        WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA
                    )
                )
            }

            R.id.imgAdangalCopySlection -> {
                checkPermissions(
                    ADANGAL_COPY, this, this as CallbackPermission,
                    PermissionDescription.permissionForCameraAndStorage(this),
                    arrayOf(
                        WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA
                    )
                )
            }

            R.id.imgVAOCertificate -> {
                checkPermissions(
                    VAO_CERTIFICATE,
                    this,
                    this as CallbackPermission,
                    PermissionDescription.permissionForCameraAndStorage(this),
                    arrayOf(
                        WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA
                    )
                )
            }

            R.id.imgSoil_WaterTest -> {
                checkPermissions(
                    SOIL_TEST, this, this as CallbackPermission,
                    PermissionDescription.permissionForCameraAndStorage(this),
                    arrayOf(
                        WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA
                    )
                )
            }

            R.id.layOfAddMore -> {
                val map = HashMap<String, Any>()

                map[ParamKey.DISTRICT] = spDistrict.selectedItem.toString()
                map[ParamKey.BLOCK] = spBlockAddMore.selectedItem.toString()
                map[ParamKey.VILLAGE] = spVillageAddMore.selectedItem.toString()
                map[ParamKey.SURVEY_NO] = spSurveyNoAddMore.selectedItem.toString()
                map[ParamKey.SUB_DIVISION] = subDivisionID
                map["division"] = spSubDivisionAddMore.selectedItem.toString()
                map[ParamKey.AREA_PROPOSED] = edtAreProposed.text.toString()

                if (addMoreValidation(map)) {
                    /* var areaCount = 0.0
                     if (appDetailsToAddMore.size != 0) {
                         for ((index, item) in appDetailsToAddMore.withIndex()) {
                             if (item[ParamKey.AREA_PROPOSED].toString().isNotEmpty()) {
                                 areaCount += item[ParamKey.AREA_PROPOSED].toString().toDouble()
                             }
                         }
                     }
                     var inputCount =
                         if (edtAreProposed.text.toString().isNotEmpty()) edtAreProposed.text.toString().toDouble() else 0.0
                     Log.d("areaCountsdsd", "" + areaCount)
                     inputCount += schemecount + areaCount + areaapplied
                     Log.d(
                         "areaCountsdsd",
                         "totalValidation $areaCount schemecount $schemecount inputCount $inputCount"
                     )
                     if (inputCount < land_area) {*/
                    addMoreInDynamic(map)
                    /*} else {
                        showSnackView("Your reached total land area", lafOfAddNewSnackView)
                    }*/
                    //if (map[ParamKey.AREA_PROPOSED].toString().isNotEmpty()) {
                }
            }
            R.id.txtAddMoreAnyGovt -> {

                val map = HashMap<String, Any>()
                map[ParamKey.SCHEME_NAME] = edtSchemeName.text.toString().trim()
                map[ParamKey.COMPONENT] = edtComponent.text.toString().trim()
                map[ParamKey.TOTAL_AMOUNT] = edtTotalAmount.text.toString().trim()
                if (addMoreSubsidy(map)) {

                    addMoreForAnyGovtSubsidyDynamic(map)
                }
            }
        }
    }

    private fun addMoreSubsidy(map: java.util.HashMap<String, Any>): Boolean {
        when {
            map[ParamKey.SCHEME_NAME].toString().isEmpty() -> {
                showSnackView("Enter Previously Scheme Name", lafOfAddNewSnackView)
                return false
            }
            map[ParamKey.COMPONENT].toString().isEmpty() -> {
                showSnackView("Enter Previously Component Name", lafOfAddNewSnackView)
                return false
            }
            map[ParamKey.TOTAL_AMOUNT].toString().isEmpty() -> {
                showSnackView("Enter Previously Total Amount", lafOfAddNewSnackView)
                return false
            }
        }
        return true
    }

    private fun addMoreInDynamic(map: java.util.HashMap<String, Any>) {
        try {
            Log.d("mapsds", "" + map)
            appDetailsToAddMore.add(map)
            val initCount = appDetailsToAddMore.size
            appDetailsToAddMore = Utils.findDuplicates(appDetailsToAddMore)
            val currentCount = appDetailsToAddMore.size
            if (currentCount != initCount) {
                showSnackView(
                    getString(R.string.please_select_diff_values),
                    lafOfAddNewSnackView
                )
            } else {

                spDistrict.setSelection(0)
                edtAreProposed.setText("")

                val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val view = inflater.inflate(R.layout.add_more_layout_area_proposed, null)
                val layOfAddOrRemove = view.findViewById<LinearLayout>(R.id.layOfAddMore)

                val lp = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                lp.setMargins(24, 12, 24, 12)
                view.layoutParams = lp

                view.spDistrict.visibility = View.GONE
                view.spBlockAddMore.visibility = View.GONE
                view.spVillageAddMore.visibility = View.GONE
                view.spSurveyNoAddMore.visibility = View.GONE
                view.spSubDivisionAddMore.visibility = View.GONE

                view.txtAddMore.text = getString(R.string.remove_item)

                view.txtAddMore.setTextColor(resources.getColor(R.color.colorRed))
                view.txtAddMore.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_remove_circle_red,
                    0
                )

                view.edtDistrict.setText(map[ParamKey.DISTRICT].toString())
                view.edtBlockAddMore.setText(map[ParamKey.BLOCK].toString())
                view.edtVillageAddMore.setText(map[ParamKey.VILLAGE].toString())
                view.edtSurveyNoAddMore.setText(map[ParamKey.SURVEY_NO].toString())
                view.edtSubDivisionAddMore.setText(map["division"].toString())
                view.edtAreProposed.setText(map[ParamKey.AREA_PROPOSED].toString())

                view.edtAreProposed.isFocusable = false
                view.edtAreProposed.isFocusableInTouchMode = false
                view.edtAreProposed.isCursorVisible = false
                layOfAddOrRemove.setOnClickListener {
                    layOfDynamicAreaProposed.removeView(view)
                    MessageUtils.showSnackBarRemove(
                        this@AddNewApplication,
                        lafOfAddNewSnackView,
                        "One Item Removed Successfully"
                    )
                    appDetailsToAddMore.remove(map)

                    try {
                        var areaLan = 0.0
                        if (appDetailsToAddMore.size != 0) {
                            for ((index, item) in appDetailsToAddMore.withIndex()) {
                                if (item[ParamKey.AREA_PROPOSED].toString().isNotEmpty()) {
                                    areaLan += item[ParamKey.AREA_PROPOSED].toString().toDouble()
                                }
                            }
                        }
                        Log.d("areaLan11", "areaLan : $areaLan")
                        //areaLan += schemecount
                        Log.d("areaLan11", "areaLan after : $areaLan")
                        val aProposed = edtAreProposed.text.toString()
                        if (aProposed.isNotEmpty()) {
                            val areaProposedConvert = aProposed.toDouble()
                            areaLan += areaProposedConvert
                        }
                        Log.d("areaLan11", "areaLan final : $areaLan")
                        edtTotalAreaProposedIn.setText("" + Utils.convertDouble("" + areaLan))
                        val costProposed = edtCostProposed.text.toString()
                        val areaProposed = edtTotalAreaProposedIn.text.toString()
                        calculateSubsidyAmount(costProposed, areaProposed)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

                layOfDynamicAreaProposed.addView(view)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            showSnackView(MessageUtils.getFailureMessage(ex.message!!), lafOfAddNewSnackView)
        }

    }

    private fun addMoreValidation(map: java.util.HashMap<String, Any>): Boolean {
        Log.d("map_area_prosed", "" + map)
        if (map[ParamKey.DISTRICT].toString().isEmpty() || map[ParamKey.DISTRICT] == getString(R.string.select_district)) {
            showSnackView("Select Your District", lafOfAddNewSnackView)
            return false
        } else if (map[ParamKey.BLOCK].toString().isEmpty() || map[ParamKey.BLOCK] == getString(
                R.string.select_block
            )
        ) {
            showSnackView("Select Your Block", lafOfAddNewSnackView)
            return false
        } else if (map[ParamKey.VILLAGE].toString().isEmpty() || map[ParamKey.VILLAGE] == getString(
                R.string.select_village
            )
        ) {
            showSnackView("Select Your Village", lafOfAddNewSnackView)
            return false
        } else if (map[ParamKey.SURVEY_NO].toString().isEmpty() || map[ParamKey.SURVEY_NO] == getString(
                R.string.select_survey_no
            )
        ) {
            showSnackView("Select Your Survey Number", lafOfAddNewSnackView)
            return false
        } else if (map[ParamKey.SUB_DIVISION].toString().isEmpty() || map[ParamKey.SUB_DIVISION] == getString(
                R.string.select_sub_division
            ) || map[ParamKey.SUB_DIVISION] == -1
        ) {
            showSnackView("Select Your Sub Division", lafOfAddNewSnackView)
            return false
        } else if (map[ParamKey.AREA_PROPOSED].toString().isEmpty()) {
            showSnackView("Area Proposed can't be empty", lafOfAddNewSnackView)
            return false
        } else if (map[ParamKey.AREA_PROPOSED].toString().toDouble() == 0.0) {
            showSnackView(
                "Total area proposed should be in minimum $minLimit and maximum $maxLimit Ha",
                lafOfAddNewSnackView
            )
            return false
        }
        return true
    }

    private fun addNewApplication() {
        try {

            val schemeName = spNameOfScheme.selectedItem.toString()
            val componentCategory = spNameOfTheComponent.selectedItem.toString()
            val subComponent = spNameOfTheSubcomponent.selectedItem.toString()

            val totalAreaProposed = edtTotalAreaProposedIn.text.toString()
            val proposedCrop = spProposedCrop.selectedItem.toString()
            val costProposed = edtCostProposed.text.toString()
            val subsidyAmount = edtSubsidyAmount.text.toString()
            val hotNetId = edtHotNetId.text.toString()
            val appId = edtUzhavanAppPriorityNumber.text.toString()
            val relevant = AnyOtherRelevantInformation.text.toString()

            val map = HashMap<String, Any>()
            map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(this).toInt()
            map[ParamKey.GENDER] = farmerDetails!!.gender!!.toString()
            map[ParamKey.FARMER_TYPE] = farmerDetails!!.farmer_type!!.toString()
            map[ParamKey.SCHEME_NAME] = schemeId
            map[ParamKey.COMPONENT] = componentId
            map[ParamKey.SUB_COMPONENT] = categoryORSubComponentID

            if (totalAreaProposed.isNotEmpty()) {
                map[ParamKey.AREA_PROPOSED] = totalAreaProposed.toDouble()
            } else {
                map[ParamKey.AREA_PROPOSED] = 0.0
            }
            map[ParamKey.PROPOSED_CROP] = proposedCrop
            if (costProposed.isNotEmpty()) {
                map[ParamKey.COST_PEROSED] = costProposed.toDouble()
            } else {
                map[ParamKey.COST_PEROSED] = ""
            }

            Log.d(
                "isCheksds",
                " $previoslySubsidy : " + isChek + " sizeOfPrevious " + addMorePreviouslySubsidy.size + " Previous " + gSubSidyYes.isChecked + "  no " + gSubSidyNo.isChecked + " bankLoa ${loanYes.isChecked} No  ${loanNo.isChecked}"
            )

            try {
                if (gSubSidyYes.isChecked) {
                    map[ParamKey.GOVT_SUBSIDY] = "Yes"
                    previoslySubsidy = "Yes"
                } else if (gSubSidyNo.isChecked) {
                    previoslySubsidy = "No"
                    map[ParamKey.GOVT_SUBSIDY] = "No"
                    addMorePreviouslySubsidy.clear()
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            if (gSubSidyYes.isChecked) {
                if (addMorePreviouslySubsidy.size != 0) {
                    val mapPreviouse = HashMap<String, Any>()
                    mapPreviouse[ParamKey.SCHEME_NAME] = edtSchemeName.text.toString().trim()
                    mapPreviouse[ParamKey.COMPONENT] = edtComponent.text.toString().trim()
                    mapPreviouse[ParamKey.TOTAL_AMOUNT] = edtTotalAmount.text.toString().trim()
                    if (addMoreSubsidy(mapPreviouse)) {
                        addMorePreviouslySubsidy.add(mapPreviouse)
                    }
                    map[ParamKey.PREVIOUSLY_SUBSIDY] = addMorePreviouslySubsidy
                }
            }

            try {
                if (loanYes.isChecked) {
                    map[ParamKey.GET_BANK_LOAN] = "Yes"
                } else if (loanNo.isChecked) {
                    map[ParamKey.GET_BANK_LOAN] = "No"
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            if (subsidyAmount.isNotEmpty()) {
                map[SUBSIDY_AMOUNT] = subsidyAmount.toDouble()
            } else {
                map[SUBSIDY_AMOUNT] = ""
            }
            map[PRE_SUBSIDY_AMOUNT] = previoslySubsidy
            map[ADANGAL_COPY_IMG] = adangalCopy
            map[HOTNET_ID] = hotNetId
            map[SCHEME_COUNT] = schemecount
            map[ParamKey.MINI_LIMIT] = minLimit
            map[ParamKey.MAX_LIMIT] = maxLimit
            map[ParamKey.ULAVAN_APP_ID] = appId
            map[ParamKey.RELEVANT] = relevant
            map[ParamKey.SOIL_TEST_REPORT] = soilwater_test

            val listOfImages = ArrayList<MultipartBody.Part>()

            if (validationNewApplication(map)) {
                val mapAdd = HashMap<String, Any>()
                mapAdd[ParamKey.DISTRICT] = spDistrict.selectedItem.toString()
                mapAdd[ParamKey.BLOCK] = spBlockAddMore.selectedItem.toString()
                mapAdd[ParamKey.VILLAGE] = spVillageAddMore.selectedItem.toString()
                mapAdd[ParamKey.SURVEY_NO] = spSurveyNoAddMore.selectedItem.toString()
                mapAdd[ParamKey.SUB_DIVISION] =
                    subDivisionID//spSubDivisionAddMore.selectedItem.toString()
                mapAdd[ParamKey.AREA_PROPOSED] = edtAreProposed.text.toString()

                appDetailsToAddMore.add(mapAdd)
                appDetailsToAddMore = Utils.findDuplicates(appDetailsToAddMore)
                map[ParamKey.VILLAGE] = appDetailsToAddMore


                if (Utils.checkFile(adangalCopy)) {
                    listOfImages.add(Utils.multiPartFromFile(adangalCopy, ADANGAL_COPY_IMG))
                } else {
                    showSnackView(getString(R.string.adangal_copy_not_exist), lafOfAddNewSnackView)
                    return
                }

                if (map[ParamKey.GET_BANK_LOAN].toString().toLowerCase() == "Yes".toLowerCase()) {
                    if (Utils.checkFile(bankLoan)) {
                        listOfImages.add(
                            Utils.multiPartFromFile(
                                bankLoan,
                                ParamKey.BANK_LOAN_COPY_IMG
                            )
                        )
                    } /*else {
                        showSnackView(
                            getString(R.string.bank_loan_file_not_exits),
                            lafOfAddNewSnackView
                        )
                        return
                    }*/
                }

                if (soilTest.isNotEmpty()) {
                    if (Utils.checkFile(soilTest)) {
                        listOfImages.add(Utils.multiPartFromFile(soilTest, SOIL_CERTIFICATE_COPY))
                    }
                }

                if (vaoCertificate.isNotEmpty()) {
                    if (Utils.checkFile(vaoCertificate)) {
                        listOfImages.add(
                            Utils.multiPartFromFile(
                                vaoCertificate,
                                VAO_CERTIFICATE_COPY
                            )
                        )
                    }
                }

                Log.d("listOfImagessd", "" + listOfImages.size)

                Connector.callBasicWithPart(
                    this@AddNewApplication,
                    map,
                    listOfImages,
                    readWriteAPI!!,
                    CREATE_NEW_APPLICATION
                )
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun setTheComponent() {
        val district = resources.getStringArray(R.array.category_list)
        spNameOfTheComponent.adapter = ArrayAdapter<String>(
            this,
            R.layout.single_view,
            R.id.singe_item,
            district
        )
    }

    private fun setTheSubComponent() {
        val district = resources.getStringArray(R.array.component_list)
        spNameOfTheSubcomponent.adapter = ArrayAdapter<String>(
            this,
            R.layout.single_view,
            R.id.singe_item,
            district
        )
    }


    private fun setVillageDefault() {
        val village = resources.getStringArray(R.array.village_list)
        spVillageAddMore.adapter = ArrayAdapter<String>(
            this,
            R.layout.single_view,
            R.id.singe_item,
            village
        )
    }

    private fun setBlockDefault() {
        val block = resources.getStringArray(R.array.block_list)
        spBlockAddMore.adapter = ArrayAdapter<String>(
            this,
            R.layout.single_view,
            R.id.singe_item,
            block
        )
    }

    private fun setDistrictDefault() {
        val district = resources.getStringArray(R.array.district_list)
        spDistrict.adapter = ArrayAdapter<String>(
            this,
            R.layout.single_view,
            R.id.singe_item,
            district
        )
    }


    private fun setSurveyNumber() {
        val survey = resources.getStringArray(R.array.survey_list)
        spSurveyNoAddMore.adapter = ArrayAdapter<String>(
            this,
            R.layout.single_view,
            R.id.singe_item,
            survey
        )
    }

    private fun setSubDivision() {
        val subDivision = resources.getStringArray(R.array.sub_division_list)
        spSubDivisionAddMore.adapter = ArrayAdapter<String>(
            this,
            R.layout.single_view,
            R.id.singe_item,
            subDivision
        )
    }

    private fun validationNewApplication(map: java.util.HashMap<String, Any>): Boolean {
        Log.d("mapsdsd", "" + map)
        if (map[ParamKey.SCHEME_NAME] == -1 || map[ParamKey.SCHEME_NAME].toString() == "Select Scheme") {
            showSnackView(getString(R.string.pls_select_scheme_name), lafOfAddNewSnackView)
            return false
        } else if ((map[COMPONENT] == -1 || map[COMPONENT].toString() == "Select Category")) {
            showSnackView(getString(R.string.pls_select_category_name), lafOfAddNewSnackView)
            return false
        } else if ((map[ParamKey.SUB_COMPONENT] == -1 || map[ParamKey.SUB_COMPONENT].toString() == "Select Component")) {
            showSnackView(getString(R.string.pls_select_component_name), lafOfAddNewSnackView)
            return false
        } else if ((map[ParamKey.AREA_PROPOSED].toString().isEmpty())) {
            showSnackView(getString(R.string.enter_total_area_proposed), lafOfAddNewSnackView)
            return false
        } else if ((map[ParamKey.PROPOSED_CROP].toString().isEmpty() || map[ParamKey.PROPOSED_CROP].toString() == "Select Proposed Crop")) {
            showSnackView(getString(R.string.pls_select_proposed_crop), lafOfAddNewSnackView)
            return false
        } else if (map[ParamKey.GOVT_SUBSIDY] == -1) {
            showSnackView(getString(R.string.pls_select_privoise_subsidy), lafOfAddNewSnackView)
            return false
        } else if ((map[ParamKey.COST_PEROSED].toString().isEmpty())) {
            showSnackView(getString(R.string.pls_enter_cost_of_estimate), lafOfAddNewSnackView)
            return false
        } else if ((map[ParamKey.SUBSIDY_AMOUNT].toString().isEmpty())) {
            showSnackView(getString(R.string.subsidy_amount_missing), lafOfAddNewSnackView)
            return false
        } else if ((map[PRE_SUBSIDY_AMOUNT].toString().isEmpty())) {
            showSnackView(getString(R.string.enter_proposed_crop_amount), lafOfAddNewSnackView)
            return false
        } else if (map[ADANGAL_COPY_IMG].toString().isEmpty()) {
            showSnackView(getString(R.string.pls_select_adangal_copy), lafOfAddNewSnackView)
            return false
        }
        return true
    }

    /**
     * Set Respective View after Selected File
     */
    override fun activityResultCallback(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            val contentURI = data!!.data
            val bitmap = MediaStore.Images.Media.getBitmap(
                contentResolver,
                contentURI
            )
            when (requestCode) {
                BANK_LOAN -> {
                    imgBankLoan.setImageBitmap(bitmap)
                    bankLoan = ImageUtils.saveImage(this, bitmap)
                    //txtBankLoan.text = File(bankLoan).name

                    if (Utils.getFileSize(bankLoan) <= Utils.IMAGE_SIZE) {
                        txtSizeBank.visibility = View.GONE
                    } else {
                        bankLoan = ""
                        imgBankLoan.setImageResource(R.drawable.ic_menu_camera)
                        txtSizeBank.visibility = View.VISIBLE
                    }
                }

                ADANGAL_COPY -> {
                    imgAdangalCopySlection.setImageBitmap(bitmap)
                    adangalCopy = ImageUtils.saveImage(this, bitmap)
                    // txtAdangalCopy.text = File(adangalCopy).name
                    if (Utils.getFileSize(adangalCopy) <= Utils.IMAGE_SIZE) {
                        txtSizeAadhar.visibility = View.GONE
                    } else {
                        adangalCopy = ""
                        imgAdangalCopySlection.setImageResource(R.drawable.ic_menu_camera)
                        txtSizeAadhar.visibility = View.VISIBLE
                    }
                }

                VAO_CERTIFICATE -> {
                    imgVAOCertificate.setImageBitmap(bitmap)
                    vaoCertificate = ImageUtils.saveImage(this, bitmap)
                    //txtVAOCertificate.text = File(vaoCertificate).name
                    if (Utils.getFileSize(vaoCertificate) <= Utils.IMAGE_SIZE) {
                        txtSizeVAO.visibility = View.GONE
                    } else {
                        vaoCertificate = ""
                        imgVAOCertificate.setImageResource(R.drawable.ic_menu_camera)
                        txtSizeVAO.visibility = View.VISIBLE
                    }
                }

                SOIL_TEST -> {
                    imgSoil_WaterTest.setImageBitmap(bitmap)
                    soilTest = ImageUtils.saveImage(this, bitmap)
                    //txtSoil_WaterTest.text = File(soilTest).name
                    if (Utils.getFileSize(soilTest) <= Utils.IMAGE_SIZE) {
                        txtSizeSoil.visibility = View.GONE
                    } else {
                        soilTest = ""
                        imgSoil_WaterTest.setImageResource(R.drawable.ic_menu_camera)
                        txtSizeSoil.visibility = View.VISIBLE
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            showSnackView(MessageUtils.getFailureMessage(ex.message!!), lafOfAddNewSnackView)
        }
    }

    override fun onError(message: String, api: String) {
        showSnackView(message, lafOfAddNewSnackView)
    }

    override fun onActionAfterPermissionGrand(requestCode: Int) {
        choosePhotoFromGallery(this, requestCode)
    }

    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        when (api) {
            INIT_VALUES_FOR_CREATE_APPLICATION -> {
                setDropDownForSchemes(responseBody)
            }
            COMPONENT_LIST -> {
                setDropDownForComponentList(responseBody)
            }
            SUB_COMPONENT_LIST -> {
                setDropDownForSubComponentList(responseBody)
            }
            SUB_COMPONENT_AREA -> {
                val modelSchemeCount =
                    Gson().fromJson(
                        responseBody.body()?.string(),
                        SchemeAreaCountDTO::class.java
                    )

                Log.d(
                    "sdlkjfkldsf",
                    "" + modelSchemeCount.data!!.proposedcrop.toString().toLowerCase()
                )
                if (modelSchemeCount.data!!.proposedcrop.toString().toLowerCase() == "Yes".toLowerCase()) {
                    layOfProposedCrop.visibility = View.VISIBLE
                } else {
                    layOfProposedCrop.visibility = View.GONE
                }
                schemecount = if (modelSchemeCount != null) {
                    if (modelSchemeCount.status!!) {
                        modelSchemeCount.data!!.schemecount!!.toDouble()
                    } else {
                        0.0
                    }
                } else {
                    0.0
                }

                Log.d("schemecountds", "" + schemecount)
            }
            BLOCK_LIST_PRO -> {
                setBlockSpinner(responseBody)

            }
            VILLAGE_LIST_PRO -> {
                setVillageSpinner(responseBody)
            }
            SURVEY_LIST_PRO -> {
                try {
                    val modelSurvey = Gson().fromJson(
                        responseBody.body()?.string(),
                        ModelSurveyNumbersDTO::class.java
                    )
                    if (modelSurvey.status!!) {
                        if (modelSurvey!!.data!!.isNotEmpty()) {
                            listOfSurveyInfo = ArrayList()
                            for ((index, item) in modelSurvey!!.data!!.withIndex()) {
                                listOfSurveyInfo!!.add(item!!.servey_no!!)
                            }
                            listOfSurveyInfo!!.add(0, getString(R.string.select_survey_no))
                            spSurveyNoAddMore.adapter = ArrayAdapter(
                                this,
                                R.layout.single_view,
                                R.id.singe_item, listOfSurveyInfo
                            )
                        } else {
                            showSnackView(
                                getString(R.string.survey_no_not_found),
                                lafOfAddNewSnackView
                            )
                        }
                    } else {
                        showSnackView(
                            modelSurvey!!.message!!,
                            lafOfAddNewSnackView
                        )
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()

                    showSnackView(
                        MessageUtils.getFailureMessage(ex.message!!),
                        lafOfAddNewSnackView
                    )
                }
            }
            SUB_DIVISION_LIST_PRO -> {
                setDropDownForSubDivision(responseBody)
            }

            CREATE_NEW_APPLICATION -> {
                val modelCreate =
                    Gson().fromJson(
                        responseBody.body()?.string(),
                        ModelRegisterSuccess::class.java
                    )
                if (modelCreate != null) {
                    if (modelCreate.status!!) {
                        MessageUtils.showToastMessage(
                            this@AddNewApplication,
                            modelCreate.message!!,
                            false
                        )
                        val wallpaperDirectory =
                            File(Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY)
                        wallpaperDirectory.delete()
                        finish()
                    } else {
                        showSnackView(modelCreate.message!!, lafOfAddNewSnackView)
                    }
                } else {
                    showSnackView(
                        getString(R.string.something_went_wrong),
                        lafOfAddNewSnackView
                    )
                }
            }

        }
    }

    private fun setDropDownForSubDivision(responseBody: Response<ResponseBody>) {

        val modelSurveyNumbersDTO =
            Gson().fromJson(responseBody.body()?.string(), PojoSubDivision::class.java)
        if (modelSurveyNumbersDTO != null) {
            if (modelSurveyNumbersDTO.status!!) {
                subDivisionDetails = ArrayList()
                subDivisionInfo = ArrayList()
                subDivisionDetails = modelSurveyNumbersDTO.data!!
                if (subDivisionDetails!!.isNotEmpty()) {
                    for ((index, items) in subDivisionDetails!!.withIndex()) {
                        subDivisionInfo!!.add(items!!.subdivision!!.subdivision.toString())
                    }

                    subDivisionInfo!!.add(0, "Select Sub Division")

                    spSubDivisionAddMore.adapter =
                        ArrayAdapter(
                            this,
                            R.layout.single_view,
                            R.id.singe_item,
                            subDivisionInfo!!
                        )
                } else {
                    showSnackView(
                        getString(R.string.something_went_wrong),
                        lafOfAddNewSnackView
                    )
                }
            } else {
                showSnackView(modelSurveyNumbersDTO.message!!, lafOfAddNewSnackView)

            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), lafOfAddNewSnackView)
        }
    }

    private fun setBlockSpinner(responseBody: Response<ResponseBody>) {
        val modelBlock = Gson().fromJson(
            responseBody.body()?.string(),
            ModelBlockDTO::class.java
        )

        if (modelBlock != null) {
            if (modelBlock.status!!) {
                blockDetails = modelBlock.data as ArrayList<BlockDTO?>
                val count =
                    if (blockDetails != null) blockDetails.size else 0
                if (count != 0) {
                    blockInfo.clear()
                    blockInfo.add(getString(R.string.select_block))
                    var index = -1
                    for ((indext, values) in blockDetails.withIndex()) {
                        blockInfo.add(values?.block_name!!)
                        try {

                            if (values.id!!.toString() == farmerDetails!!.block.toString()) {
                                index = indext
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                    spBlockAddMore.adapter = ArrayAdapter(
                        this,
                        R.layout.single_view,
                        R.id.singe_item, blockInfo
                    )
                    /*if (index != -1) {
                        spBlockAddMore.setSelection(index + 1)
                    }*/
                    //setVillageDefault()
                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        lafOfAddNewSnackView
                    )
                }
            } else {
                showSnackView(modelBlock.message!!, lafOfAddNewSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), lafOfAddNewSnackView)
        }
    }

    private fun setDropDownForSubComponentList(responseBody: Response<ResponseBody>) {
        try {
            val modelSubComponentListDTO =
                Gson().fromJson(
                    responseBody.body()?.string(),
                    ModelSubComponentDTO::class.java
                )
            if (modelSubComponentListDTO != null) {
                if (modelSubComponentListDTO.status!!) {
                    try {
                        val count =
                            if (modelSubComponentListDTO.data != null) modelSubComponentListDTO.data.size else 0
                        if (count != 0) {
                            subComponentInfo = ArrayList()
                            subComponentDetails = modelSubComponentListDTO.data
                            for ((indext, item) in subComponentDetails!!.withIndex()) {
                                subComponentInfo!!.add(item!!.component_name!!)
                            }
                        }
                        subComponentInfo!!.add(0, getString(R.string.select_sub_componet))
                        spNameOfTheSubcomponent.adapter = ArrayAdapter(
                            this,
                            R.layout.single_view,
                            R.id.singe_item,
                            subComponentInfo!!
                        )
                        try {
                            if (isThere) {
                                spNameOfTheSubcomponent.setSelection(
                                    subComponentInfo!!.indexOf(
                                        subComponent
                                    )
                                )
                            }
                        } catch (ex: java.lang.Exception) {
                            ex.printStackTrace()
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(
                            MessageUtils.getFailureMessage(ex.message!!),
                            lafOfAddNewSnackView
                        )
                    }
                } else {
                    showSnackView(modelSubComponentListDTO.message!!, lafOfAddNewSnackView)
                }
            } else {
                showSnackView(getString(R.string.something_went_wrong), lafOfAddNewSnackView)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            showSnackView(MessageUtils.getFailureMessage(ex.message!!), lafOfAddNewSnackView)
        }
    }

    private fun setDropDownForComponentList(responseBody: Response<ResponseBody>) {
        try {
            val modelComponentListDTO =
                Gson().fromJson(
                    responseBody.body()?.string(),
                    ModelComponentCategoryDTO::class.java
                )
            if (modelComponentListDTO != null) {
                if (modelComponentListDTO.status!!) {
                    try {
                        val count =
                            if (modelComponentListDTO.data != null) modelComponentListDTO.data.size else 0
                        if (count != 0) {
                            componentInfo = ArrayList()
                            componentDetails = modelComponentListDTO.data
                            for ((indext, item) in componentDetails!!.withIndex()) {
                                componentInfo!!.add(item!!.category!!)
                            }
                        }
                        componentInfo!!.add(0, getString(R.string.select_category))
                        spNameOfTheComponent.adapter = ArrayAdapter(
                            this,
                            R.layout.single_view,
                            R.id.singe_item,
                            componentInfo!!
                        )
                        try {
                            if (isThere) {
                                spNameOfTheComponent.setSelection(
                                    componentInfo!!.indexOf(category)
                                )
                            }
                        } catch (ex: java.lang.Exception) {
                            ex.printStackTrace()
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(
                            MessageUtils.getFailureMessage(ex.message!!),
                            lafOfAddNewSnackView
                        )
                    }
                } else {
                    showSnackView(modelComponentListDTO.message!!, lafOfAddNewSnackView)
                }
            } else {
                showSnackView(getString(R.string.something_went_wrong), lafOfAddNewSnackView)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            showSnackView(MessageUtils.getFailureMessage(ex.message!!), lafOfAddNewSnackView)
        }

    }

    /**
     *  get and Show inDrop Down for Scheme List
     */
    private fun setDropDownForSchemes(responseBody: Response<ResponseBody>) {
        try {
            val modelSchemeListDTO =
                Gson().fromJson(responseBody.body()?.string(), ModelSchemeListDTO::class.java)
            if (modelSchemeListDTO != null) {
                if (modelSchemeListDTO.success!!) {
                    try {
                        val count =
                            if (modelSchemeListDTO.data != null) modelSchemeListDTO.data.schemes!!.size else 0
                        if (count != 0) {
                            schemeInfo = ArrayList()
                            schemeDetails = modelSchemeListDTO.data!!.schemes
                            for ((indext, item) in schemeDetails!!.withIndex()) {
                                schemeInfo!!.add(item!!.scheme_name!!)
                            }
                        }
                        schemeInfo!!.add(0, getString(R.string.select_scheme))
                        spNameOfScheme.adapter = ArrayAdapter(
                            this,
                            R.layout.single_view,
                            R.id.singe_item,
                            schemeInfo!!
                        )
                        try {
                            if (isThere) {
                                spNameOfScheme.setSelection(schemeInfo!!.indexOf(schemeName))
                            }
                        } catch (ex: java.lang.Exception) {
                            ex.printStackTrace()
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(
                            MessageUtils.getFailureMessage(ex.message!!),
                            lafOfAddNewSnackView
                        )
                    }
                    try {
                        districtInfo = ArrayList()
                        districtDetails = ArrayList()
                        districtDetails = modelSchemeListDTO.data!!.districts
                        Log.d("districtDetailssd", "" + districtDetails!!.size)
                        val disCount =
                            if (districtDetails != null) districtDetails!!.size else 0
                        if (disCount != 0) {
                            for ((index, item) in districtDetails!!.withIndex()) {
                                districtInfo!!.add(item!!.district_name!!)
                                Log.d("asaslkl", "test " + index)
                            }
                        }
                        Log.d("districtInfosd", "" + districtInfo!!.size)
                        districtInfo!!.add(0, getString(R.string.select_district))
                        spDistrict.adapter = ArrayAdapter(
                            this,
                            R.layout.single_view,
                            R.id.singe_item,
                            districtInfo!!
                        )
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(
                            MessageUtils.getFailureMessage(ex.message!!),
                            lafOfAddNewSnackView
                        )
                    }
                    try {
                        proposedCropInfo = ArrayList()
                        val proCount =
                            if (modelSchemeListDTO.data!!.proposed_crop != null) modelSchemeListDTO.data!!.proposed_crop!!.size else 0

                        if (proCount != 0) {
                            for ((index, item) in modelSchemeListDTO.data!!.proposed_crop!!.withIndex()) {
                                proposedCropInfo!!.add(item!!.crop_name!!)
                            }
                        }

                        /*
                        * Chek Proposed Crop is Enable or Disable From Admin
                        * */

                        proposedCropInfo!!.add(0, getString(R.string.select_proposed_crop))
                        spProposedCrop.adapter = ArrayAdapter(
                            this,
                            R.layout.single_view,
                            R.id.singe_item,
                            proposedCropInfo!!
                        )

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(
                            MessageUtils.getFailureMessage(ex.message!!),
                            lafOfAddNewSnackView
                        )
                    }
                } else {
                    showSnackView(modelSchemeListDTO.message!!, lafOfAddNewSnackView)
                }
            } else {
                showSnackView(getString(R.string.something_went_wrong), lafOfAddNewSnackView)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            showSnackView(MessageUtils.getFailureMessage(ex.message!!), lafOfAddNewSnackView)
        }
    }

    private fun setVillageSpinner(responseBody: Response<ResponseBody>) {
        val modelVillage = Gson().fromJson(
            responseBody.body()?.string(),
            ModelVillageDTO::class.java
        )

        if (modelVillage != null) {
            if (modelVillage.status!!) {
                villageDetails = modelVillage.data as ArrayList<VillageDTO?>
                val count =
                    if (villageDetails != null) villageDetails.size else 0
                if (count != 0) {
                    villageInfo.clear()
                    villageInfo.add(getString(R.string.select_village))
                    for ((indext, values) in villageDetails.withIndex()) {
                        villageInfo.add(values?.village_name!!)
                    }
                    spVillageAddMore.adapter = ArrayAdapter(
                        this,
                        R.layout.single_view,
                        R.id.singe_item, villageInfo
                    )
                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        lafOfAddNewSnackView
                    )
                }
            } else {
                showSnackView(modelVillage.message!!, lafOfAddNewSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), lafOfAddNewSnackView)
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, lafOfAddNewSnackView)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase, "en"))
    }

}
