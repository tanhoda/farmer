package com.tanhoda.farmer.views.schemeDetails

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.model.applicationViewDetails.NewApplication
import com.tanhoda.farmer.model.schemeDetails.SchemeComponentItem
import com.tanhoda.farmer.utils.ImageUtils
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.views.addNewApplication.AddNewApplication
import kotlinx.android.synthetic.main.activity_sub_component.*
import kotlinx.android.synthetic.main.fragment_home.*

class ViewSubComponent : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_component)
        val schemeInfo = intent.getSerializableExtra(ParamKey.SUB_COMPONENT) as SchemeComponentItem
        val schemecategory =
            intent.getSerializableExtra(ParamKey.SUB_COMPONENT) as SchemeComponentItem
        val componentName = intent.getStringExtra(ParamKey.SUB_COMPONENT_NAME)
        val category = intent.getStringExtra(ParamKey.SCHEME_CATEGORY)
        val schemeName = intent.getStringExtra(ParamKey.SCHEME_NAME)
        /*Log.d(
            "componentNamesd",
            "componentName : $componentName  category: $category schemeName : $schemeName"
        )
*/
        if (schemeInfo != null) {
            schemeDetailsScrl.visibility = View.VISIBLE
            txtNoDataFoundInSchemeDetails.visibility = View.GONE

            ImageUtils.setImageLive(
                imgSchemeInfo,
                Utils.IMAGE_URL_PATH_COMPONETS + schemeInfo.image,
                this
            )

            txtSchemeNameInfo.text = schemeInfo.componentName
            txtShortDescriptionInfo.text = schemeInfo.shortDescription
            txtDescriptionInfo.text = schemeInfo.description
        } else {
            txtNoDataFoundInSchemeDetails.visibility = View.VISIBLE
            schemeDetailsScrl.visibility = View.GONE
            txtNoDataFoundInSchemeDetails.text = getString(R.string.no_data_found)
        }

        btnApply.setOnClickListener {
            if (checkBankAndLandAdded()) {
                startActivity(
                    Intent(
                        this,
                        AddNewApplication::class.java
                    ).putExtra(ParamKey.IS_THERE, true)
                        .putExtra(
                            ParamKey.SUB_COMPONENT_NAME,
                            componentName
                        ).putExtra(
                            ParamKey.SCHEME_CATEGORY,
                            category
                        ).putExtra(
                            ParamKey.SCHEME_NAME,
                            schemeName
                        )
                )
            }
            btnCancel.setOnClickListener {
                onBackPressed()
            }
        }
        btnCancel.setOnClickListener {
            onBackPressed()
        }
    }

    private fun checkBankAndLandAdded(): Boolean {

        Log.d(
            "LAND_COUNT_MAXsds",
            "" + Utils.LAND_COUNT_MAX + " BANK_COUNT_MAX: ${Utils.BANK_COUNT_MAX}"
        )
        /* if (LAND_COUNT_MAX == 0 && BANK_COUNT_MAX == 0) {
             snackView=  MessageUtils.showSnackBar(
                 activity!!,
                 activity!!.homeSnackView,
                 "Add your Land & Bank Details"
             )
             return false
         } else*/ if (Utils.LAND_COUNT_MAX == 0) {
            snackView = MessageUtils.showSnackBar(
                this,
                applySnackView,
                "Add your Land Info and try again"
            )
            return false
        } else if (Utils.BANK_COUNT_MAX == 0) {
            snackView = MessageUtils.showSnackBar(
                this,
                applySnackView,
                "Add your Bank Info and Try again"
            )
            return false
        }
        return true
    }

}