package com.tanhoda.farmer.views.profileInfo

import android.os.Bundle
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R

class ProfileInfo : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.profile_info)
        setContentView(R.layout.activity_profile_info)

    }

}
