package com.tanhoda.farmer.views.addNewApplication

import android.content.Intent
import android.os.Bundle
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R

import kotlinx.android.synthetic.main.activity_bank_details.*

class ApplicationDetails : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.applications)
        setContentView(R.layout.activity_bank_details)


        fabAdd.setOnClickListener {
            startActivity(Intent(this, AddNewApplication::class.java))

        }
    }

}
