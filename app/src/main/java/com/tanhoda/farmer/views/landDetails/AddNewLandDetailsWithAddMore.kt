package com.tanhoda.farmer.views.landDetails

import android.Manifest
import android.Manifest.permission.CAMERA
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI.Companion.ADD_LAND
import com.tanhoda.farmer.api.ParamAPI.Companion.BLOCK_LIST
import com.tanhoda.farmer.api.ParamAPI.Companion.DISTRICT_LIST
import com.tanhoda.farmer.api.ParamAPI.Companion.VILLAGE_LIST
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ParamKey.Companion.AREA_PROPOSED
import com.tanhoda.farmer.api.ParamKey.Companion.LAND_AREA
import com.tanhoda.farmer.api.ParamKey.Companion.LAND_FMB_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.LAND_OWNER
import com.tanhoda.farmer.api.ParamKey.Companion.LEASE_AGREEMENT_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.PATTA_CHITTA_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.SOURCE_OF_IRRIGATION
import com.tanhoda.farmer.api.ParamKey.Companion.SUB_DIVISION
import com.tanhoda.farmer.api.ParamKey.Companion.SURVEY_NO
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.register.blockDetails.BlockDTO
import com.tanhoda.farmer.model.register.blockDetails.ModelBlockDTO
import com.tanhoda.farmer.model.register.district.DistrictDTO
import com.tanhoda.farmer.model.register.district.ModelDistrictDTO
import com.tanhoda.farmer.model.register.registerSuccess.ModelRegisterSuccess
import com.tanhoda.farmer.model.register.village.ModelVillageDTO
import com.tanhoda.farmer.model.register.village.VillageDTO
import com.tanhoda.farmer.utils.ImageUtils
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.SessionManager
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.utils.permissions.CallbackPermission
import com.tanhoda.farmer.utils.permissions.PermissionDescription
import com.tanhoda.farmer.utils.permissions.RequestPermission
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.FMB_COPY
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.LEASE_AGREMENT
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.PATTA_CHITTA
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.checkPermissions
import kotlinx.android.synthetic.main.activity_land_details.*
import kotlinx.android.synthetic.main.add_more.*
import kotlinx.android.synthetic.main.add_more.view.*
import kotlinx.android.synthetic.main.add_more_lay_land_details.*
import kotlinx.android.synthetic.main.add_more_lay_land_details.view.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File

class AddNewLandDetailsWithAddMore : BaseActivity(), CallbackPermission, ReadWriteAPI,
    View.OnClickListener {

    var districtDetails = ArrayList<DistrictDTO?>()
    var districtInfo = ArrayList<String>()

    var blockDetails = ArrayList<BlockDTO?>()
    var blockInfo = ArrayList<String>()

    var villageDetails = ArrayList<VillageDTO?>()
    var villageInfo = ArrayList<String>()

    var pattaChitta = ""
    var FMBCopy = ""
    var leaseAgreement = ""
    var appDetailsToAddMore = ArrayList<HashMap<String, Any>>()

    var readWriteAPI: ReadWriteAPI? = null
    var districtId = -1
    var blockId = -1
    var villageId = -1
    var listOfPart = ArrayList<MultipartBody.Part>()
    var FMBCopyArray = ArrayList<MultipartBody.Part>()
    var LeaseCopyArray = ArrayList<MultipartBody.Part>()
    var map: HashMap<String, Any>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.new_land_details)
        setContentView(R.layout.activity_land_details)

        readWriteAPI = this
        initCallbackPermission(this)

        imgChittaInLand.setOnClickListener(this)
        imgFMBCopyInLand.setOnClickListener(this)
        imgLeaseAgreementInLand.setOnClickListener(this)
        layOfAddMore.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
        btnSuccess.setOnClickListener(this)

        layOfFMBCopy.setOnClickListener(this)
        layOfLease.setOnClickListener(this)


        edtStateInLand.setOnClickListener { spStateInLand.performClick() }
        edtDistrictInLand.setOnClickListener { spDistrictInLand.performClick() }
        edtBlockInLand.setOnClickListener { spBlockInLand.performClick() }
        edtVillageInLand.setOnClickListener { spVillageInLand.performClick() }
        edtLandOwnershipInLand.setOnClickListener { spLandOwnership.performClick() }
        edtSourceOfIrrigationInLand.setOnClickListener { spSourceOfIrrigationInLand.performClick() }

        layOfLandBaseImages.visibility = View.GONE

        edtLandAreaInLand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {


            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    var totalAreaForText = 0.0
                    val input = s.toString()
                    if (input.isNotEmpty()) {
                        val inputArea = input.toDouble()
                        val farmerDetails =
                            SessionManager.getObject(this@AddNewLandDetailsWithAddMore)
                        if (appDetailsToAddMore.size != 0) {
                            for (i in 0 until appDetailsToAddMore.size) {
                                val items = appDetailsToAddMore[i]
                                Log.d("sdsd5487", "" + items[LAND_AREA] + "  " + i)
                                if (items[LAND_AREA].toString().isNotEmpty()) {
                                    totalAreaForText += items[LAND_AREA].toString().toDouble()
                                }
                            }
                        }
                        totalAreaForText += inputArea


                        if (farmerDetails != null) {
                            val totalLand = totalAreaForText + Utils.REGISTERED_LAND_COUNT
                            Log.d(
                                "totalLandsds",
                                "" + totalLand + " " + farmerDetails.farmer_type.toString()
                            )
                            if (!Utils.validateFarmerTypeLandAllow(
                                    totalLand,
                                    farmerDetails.farmer_type.toString(),
                                    landSnackView, this@AddNewLandDetailsWithAddMore

                                )
                            ) {
                                edtLandAreaInLand.setText("")
                                Utils.hideSoftKeyboard(
                                    this@AddNewLandDetailsWithAddMore.currentFocus,
                                    this@AddNewLandDetailsWithAddMore
                                )
                                totalAreaForText = 0.0
                                for (i in 0 until appDetailsToAddMore.size) {
                                    val items = appDetailsToAddMore[i]
                                    if (items[LAND_AREA].toString().isNotEmpty()) {
                                        totalAreaForText += items[LAND_AREA].toString().toDouble()
                                    }
                                }
                            }
                        }
                    } else {
                        if (appDetailsToAddMore.size != 0) {
                            for (i in 0 until appDetailsToAddMore.size) {
                                val items = appDetailsToAddMore!![i]
                                if (items[LAND_AREA].toString().isNotEmpty()) {
                                    totalAreaForText += items[LAND_AREA].toString().toDouble()
                                }
                            }
                        }
                    }
                    logMessage("totalAreaForText", "" + totalAreaForText)
                    txtTotalLandAreaInLand.setText("" + totalAreaForText)

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
        })


        spLandOwnership.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val landOwnerType = spLandOwnership.selectedItem.toString()
                        if (landOwnerType != getString(R.string.select_land)) {
                            layOfLandBaseImages.visibility = View.VISIBLE
                            if (landOwnerType == "Own") {
                                layOfOwn.visibility = View.VISIBLE
                                layOfLease.visibility = View.GONE
                            } else {
                                layOfLease.visibility = View.VISIBLE
                                layOfOwn.visibility = View.VISIBLE
                            }
                        } else {
                            layOfLandBaseImages.visibility = View.GONE
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }
            }


        spStateInLand.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val state = spStateInLand.selectedItem.toString()
                    if (state != null && state.isNotEmpty() && state != getString(R.string.select_state)) {
                        val map = HashMap<String, String>()
                        map[ParamKey.STATE] = state
                        Connector.callBasic(
                            this@AddNewLandDetailsWithAddMore,
                            map,
                            readWriteAPI!!,
                            DISTRICT_LIST
                        )
                    } else {
                        setDistrictDefault()

                        setBlockDefault()

                        setVillageDefault()
                    }
                }
            }

        spDistrictInLand.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val districtName = spDistrictInLand.selectedItem.toString()
                        if (districtName != null && districtName.isNotEmpty() && districtName != getString(
                                R.string.select_district
                            )
                        ) {
                            val map = HashMap<String, String>()
                            for ((index, item) in districtDetails.withIndex()) {
                                if (item!!.district_name == districtName) {
                                    districtId = item.id!!
                                    break
                                }
                            }
                            if (districtId != -1) {
                                map[ParamKey.DISTRICT_ID] = districtId.toString()
                                Connector.callBasic(
                                    this@AddNewLandDetailsWithAddMore,
                                    map,
                                    readWriteAPI!!,
                                    BLOCK_LIST
                                )
                            } else {
                                showSnackView(
                                    getString(R.string.please_select_district),
                                    landSnackView
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }
        spBlockInLand.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val blockName = spBlockInLand.selectedItem.toString()
                        if (blockName != null && blockName.isNotEmpty() && blockName != getString(
                                R.string.select_block
                            )
                        ) {
                            val map = HashMap<String, String>()
                            for ((index, item) in blockDetails.withIndex()) {
                                if (item!!.block_name == blockName) {
                                    blockId = item.id!!
                                    break
                                }
                            }
                            if (blockId != -1) {
                                map[ParamKey.BLOCK_ID] = blockId.toString()
                                Connector.callBasic(
                                    this@AddNewLandDetailsWithAddMore,
                                    map,
                                    readWriteAPI!!,
                                    VILLAGE_LIST
                                )
                            } else {
                                showSnackView(
                                    getString(R.string.please_select_block),
                                    landSnackView
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }

        spVillageInLand.onItemSelectedListener =

            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val village = spVillageInLand.selectedItem.toString()
                        for ((index, items) in villageDetails.withIndex()) {
                            if (village == items?.village_name) {
                                villageId = items.id!!
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(ex.message!!, landSnackView)
                    }

                }
            }
    }

    override fun onClick(v: View?) {
        try {
            when (v!!.id) {

                R.id.btnCancel -> {
                    onBackPressed()
                }

                R.id.btnSuccess -> {
                    try {
                        map = HashMap<String, Any>()
                        map!![ParamKey.FARMER_ID] = SessionManager.getFarmerId(this).toInt()
                        if (spStateInLand.selectedItem.toString() != getString(R.string.select_state)) {
                            map!![ParamKey.STATE] = 1
                        } else {
                            map!![ParamKey.STATE] = -1
                        }
                        map!![ParamKey.DISTRICT] =
                            districtId//spDistrictInLand.selectedItem.toString()
                        map!![ParamKey.BLOCK] = blockId//spBlockInLand.selectedItem.toString()
                        map!![ParamKey.VILLAGE] = villageId//spVillageInLand.selectedItem.toString()
                        map!![ParamKey.CHITTA_NUMBER] = edtCittaNoInLand.text.toString()
                        map!![PATTA_CHITTA_COPY] = pattaChitta
                        if (txtTotalLandAreaInLand.text.toString().isNotEmpty()) {
                            map!![AREA_PROPOSED] = txtTotalLandAreaInLand.text.toString().toDouble()
                            map!![ParamKey.TOTAL_AREA] =
                                txtTotalLandAreaInLand.text.toString().toDouble()
                        } else {
                            map!![AREA_PROPOSED] = txtTotalLandAreaInLand.text.toString()
                            map!![ParamKey.TOTAL_AREA] = txtTotalLandAreaInLand.text.toString()
                        }



                        if (landAddValidation(map!!)) {

                            //validation()

                            /**
                             * Add More Parent can't be empty
                             */

                            if (appDetailsToAddMore.size != 0 || txtTotalLandAreaInLand.text.toString().isNotEmpty()) {
                                val mapAddMore = HashMap<String, Any>()
                                mapAddMore[LAND_OWNER] =
                                    spLandOwnership.selectedItem.toString()
                                mapAddMore[SURVEY_NO] = edtSurveyNoInLand.text.toString()
                                mapAddMore[SUB_DIVISION] =
                                    edtSubDivisionInLand.text.toString()
                                mapAddMore[LAND_AREA] = edtLandAreaInLand.text.toString()
                                mapAddMore[LAND_FMB_COPY] = File(FMBCopy).name
                                mapAddMore[LEASE_AGREEMENT_COPY] = File(leaseAgreement).name
                                mapAddMore[SOURCE_OF_IRRIGATION] =
                                    spSourceOfIrrigationInLand.selectedItem.toString()
                                appDetailsToAddMore.add(mapAddMore)
                                val current = appDetailsToAddMore.size
                                appDetailsToAddMore = Utils.findDuplicates(appDetailsToAddMore)
                                val afterFilter = appDetailsToAddMore.size
                                Log.d("sdlslkd", "" + current + "  " + afterFilter)
                                if (afterFilter != current) {
                                    appDetailsToAddMore.remove(mapAddMore)
                                }


                                map!![ParamKey.LAND_DETAILS] = appDetailsToAddMore

                                if (validationForAddMoreLand(mapAddMore)) {

                                    if (mapAddMore[LAND_OWNER].toString() == "Own") {
                                        if (mapAddMore[LAND_FMB_COPY].toString() == "") {
                                            showSnackView(
                                                getString(R.string.select_your_FMB_copy),
                                                landSnackView
                                            )
                                            return
                                        }
                                    } else if (mapAddMore[LAND_OWNER].toString() == "Lease") {
                                        if (mapAddMore[LAND_FMB_COPY].toString() == "") {
                                            showSnackView(
                                                getString(R.string.select_your_FMB_copy),
                                                landSnackView
                                            )
                                            return
                                        } else if (mapAddMore[LEASE_AGREEMENT_COPY].toString().isEmpty()) {
                                            showSnackView(
                                                getString(R.string.select_leaseagreement),
                                                landSnackView
                                            )
                                            return
                                        }
                                    }

                                    if (mapAddMore[SOURCE_OF_IRRIGATION].toString() == "Select Irrigation") {
                                        showSnackView(
                                            getString(R.string.select_your_irrigation),
                                            landSnackView
                                        )
                                        return
                                    }

                                } else {
                                    return
                                }
                            }
                            Log.d("iscalled", "yes")
                            if (listOfPart.size == 0) {
                                if (Utils.checkFile(pattaChitta)) {
                                    listOfPart.add(
                                        Utils.multiPartFromFile(
                                            pattaChitta,
                                            PATTA_CHITTA_COPY
                                        )
                                    )

                                }
                            }
                            try {
                                if (spLandOwnership.selectedItem.toString().toLowerCase() == "Lease".toLowerCase()) {
                                    if (Utils.checkFile(FMBCopy)) {
                                        FMBCopyArray.add(
                                            Utils.multiPartFromFile(
                                                FMBCopy,
                                                LAND_FMB_COPY + "[]"
                                            )
                                        )
                                    }
                                    if (Utils.checkFile(leaseAgreement)) {
                                        LeaseCopyArray.add(
                                            Utils.multiPartFromFile(
                                                leaseAgreement,
                                                LEASE_AGREEMENT_COPY + "[]"
                                            )
                                        )
                                    }
                                } else {
                                    if (Utils.checkFile(FMBCopy)) {
                                        Log.d("iscalled", "ssdsd")
                                        FMBCopyArray.add(
                                            Utils.multiPartFromFile(
                                                FMBCopy,
                                                LAND_FMB_COPY + "[]"
                                            )
                                        )
                                    }
                                }
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }

                            /*FMBCopyArray = Utils.findDuplicatesFromPart(FMBCopyArray)
                            listOfPart = Utils.findDuplicatesFromPart(listOfPart)
                            LeaseCopyArray = Utils.findDuplicatesFromPart(LeaseCopyArray)*/

                            Connector.callBasicWithMultiPart(
                                this@AddNewLandDetailsWithAddMore,
                                map!!,
                                listOfPart,
                                FMBCopyArray,
                                LeaseCopyArray,
                                readWriteAPI!!,
                                ADD_LAND
                            )

                            // }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
                R.id.imgChittaInLand -> {
                    try {
                        checkPermissions(
                            PATTA_CHITTA, this, this as CallbackPermission,
                            PermissionDescription.permissionForCameraAndStorage(this),
                            arrayOf(
                                WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, CAMERA
                            )
                        )
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
                R.id.imgFMBCopyInLand -> {
                    try {
                        checkPermissions(
                            FMB_COPY, this, this as CallbackPermission,
                            PermissionDescription.permissionForCameraAndStorage(this),
                            arrayOf(
                                WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, CAMERA
                            )
                        )
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
                R.id.layOfFMBCopy -> {
                    try {
                        checkPermissions(
                            FMB_COPY, this, this as CallbackPermission,
                            PermissionDescription.permissionForCameraAndStorage(this),
                            arrayOf(
                                WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, CAMERA
                            )
                        )
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

                R.id.imgLeaseAgreementInLand -> {
                    try {
                        checkPermissions(
                            LEASE_AGREMENT, this, this as CallbackPermission,
                            PermissionDescription.permissionForCameraAndStorage(this),
                            arrayOf(
                                WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, CAMERA
                            )
                        )
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
                R.id.layOfLease -> {
                    try {
                        checkPermissions(
                            LEASE_AGREMENT, this, this as CallbackPermission,
                            PermissionDescription.permissionForCameraAndStorage(this),
                            arrayOf(
                                WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, CAMERA
                            )
                        )
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
                R.id.layOfAddMore -> {
                    try {

                        val map = HashMap<String, Any>()
                        map[LAND_OWNER] = spLandOwnership.selectedItem.toString()
                        map[SURVEY_NO] = edtSurveyNoInLand.text.toString()
                        map[SUB_DIVISION] = edtSubDivisionInLand.text.toString()
                        map[LAND_AREA] = edtLandAreaInLand.text.toString()
                        map[LAND_FMB_COPY] = File(FMBCopy).name
                        map[LEASE_AGREEMENT_COPY] = File(leaseAgreement).name
                        map[SOURCE_OF_IRRIGATION] =
                            spSourceOfIrrigationInLand.selectedItem.toString()
                        if (validationForAddMoreLand(map)) {

                            var totalAreaForText = 0.0
                            if (appDetailsToAddMore.size != 0) {
                                for (i in 0 until appDetailsToAddMore.size) {
                                    val items = appDetailsToAddMore[i]
                                    Log.d("sdsd5487", "" + items[LAND_AREA] + "  " + i)
                                    if (items[LAND_AREA].toString().isNotEmpty()) {
                                        totalAreaForText += items[LAND_AREA].toString().toDouble()
                                    }
                                }
                            }
                            totalAreaForText += map[LAND_AREA].toString().toDouble()
                            Log.d("totalAreaForTextsd", "" + totalAreaForText)

                            if (Utils.validateFarmerTypeLandAllow(
                                    totalAreaForText,
                                    SessionManager.getObject(this).farmer_type.toString(),
                                    landSnackView,
                                    this@AddNewLandDetailsWithAddMore
                                )
                            ) {

                                if (map[LAND_OWNER].toString() == "Own") {
                                    if (map[LAND_FMB_COPY].toString() == "") {
                                        showSnackView(
                                            getString(R.string.select_your_FMB_copy),
                                            landSnackView
                                        )
                                        return
                                    }
                                } else if (map[LAND_OWNER].toString() == "Lease") {
                                    if (map[LAND_FMB_COPY].toString() == "") {
                                        showSnackView(
                                            getString(R.string.select_your_FMB_copy),
                                            landSnackView
                                        )
                                        return
                                    } else if (map[LEASE_AGREEMENT_COPY].toString().isEmpty()) {
                                        showSnackView(
                                            getString(R.string.select_leaseagreement),
                                            landSnackView
                                        )
                                        return
                                    }
                                }

                                if (map[SOURCE_OF_IRRIGATION].toString() == "Select Irrigation") {
                                    showSnackView(
                                        getString(R.string.select_your_irrigation),
                                        landSnackView
                                    )
                                    return
                                }
                                if (Utils.checkFile(pattaChitta)) {

                                    listOfPart.add(
                                        Utils.multiPartFromFile(
                                            pattaChitta,
                                            PATTA_CHITTA_COPY
                                        )
                                    )

                                }
                                Log.d(
                                    "iscalled",
                                    "yessss " + listOfPart.size + "  " + spLandOwnership.selectedItem.toString().toLowerCase()
                                )
                                try {
                                    if (spLandOwnership.selectedItem.toString().toLowerCase() == "Lease".toLowerCase()) {
                                        if (Utils.checkFile(FMBCopy)) {
                                            FMBCopyArray.add(
                                                Utils.multiPartFromFile(
                                                    FMBCopy,
                                                    LAND_FMB_COPY + "[]"
                                                )
                                            )
                                        }
                                        if (Utils.checkFile(leaseAgreement)) {
                                            LeaseCopyArray.add(
                                                Utils.multiPartFromFile(
                                                    leaseAgreement,
                                                    LEASE_AGREEMENT_COPY + "[]"
                                                )
                                            )
                                        }
                                    } else {
                                        if (Utils.checkFile(FMBCopy)) {
                                            Log.d("iscalled", "ssdsd")
                                            FMBCopyArray.add(
                                                Utils.multiPartFromFile(
                                                    FMBCopy,
                                                    LAND_FMB_COPY + "[]"
                                                )
                                            )

                                        }

                                    }
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                                Log.d("iscalled", "yes " + listOfPart.size)
                                /* FMBCopyArray = Utils.findDuplicatesFromPart(FMBCopyArray)
                                 listOfPart = Utils.findDuplicatesFromPart(listOfPart)
                                 LeaseCopyArray = Utils.findDuplicatesFromPart(LeaseCopyArray)*/

                                //addDynamic(map)


                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun setVillageDefault() {
        val village = resources.getStringArray(R.array.village_list)
        spVillageInLand.adapter = ArrayAdapter<String>(
            this@AddNewLandDetailsWithAddMore,
            R.layout.single_view,
            R.id.singe_item,
            village
        )
    }

    private fun setBlockDefault() {
        val block = resources.getStringArray(R.array.block_list)
        spBlockInLand.adapter = ArrayAdapter<String>(
            this@AddNewLandDetailsWithAddMore,
            R.layout.single_view,
            R.id.singe_item,
            block
        )
    }

    private fun setDistrictDefault() {
        val district = resources.getStringArray(R.array.district_list)
        spDistrictInLand.adapter = ArrayAdapter<String>(
            this@AddNewLandDetailsWithAddMore,
            R.layout.single_view,
            R.id.singe_item,
            district
        )
    }

    private fun addDynamic(map: java.util.HashMap<String, Any>) {

        appDetailsToAddMore.add(map)
        val initCount = appDetailsToAddMore.size

        appDetailsToAddMore = Utils.findDuplicates(appDetailsToAddMore)
        val currentCount = appDetailsToAddMore.size
        if (currentCount != initCount) {
            showSnackView(getString(R.string.please_select_diff_values), landSnackView)
        } else {
            /* val fmbCopy = Utils.multiPartFromFile(FMBCopy, LAND_FMB_COPY + "[]")
             val leaseCopy =
                 Utils.multiPartFromFile(leaseAgreement, LEASE_AGREEMENT_COPY + "[]")*/


            /*FMBCopyArray.add(fmbCopy)
            LeaseCopyArray.add(leaseCopy)*/

            Log.d("checkTotalValues", "adddyanmic " + listOfPart)
            spLandOwnership.setSelection(0)
            edtSurveyNoInLand.setText("")
            edtSubDivisionInLand.setText("")
            edtLandAreaInLand.setText("")
            txtFMBCopyInLand.setText("")
            txtLeaseAgreementInLand.setText("")
            FMBCopy = ""
            leaseAgreement = ""
            imgFMBCopyInLand.setImageResource(R.drawable.ic_menu_camera)
            imgLeaseAgreementInLand.setImageResource(R.drawable.ic_menu_camera)


            spSourceOfIrrigationInLand.setSelection(0)
            if (validationForAddMoreLand(map)) {
                if (map[LAND_OWNER].toString() == "Own") {
                    if (map[LAND_FMB_COPY].toString() == "") {
                        showSnackView(getString(R.string.select_your_FMB_copy), landSnackView)
                        return
                    }
                } else if (map[LAND_OWNER].toString() == "Lease") {
                    if (map[LAND_FMB_COPY].toString() == "") {
                        showSnackView(getString(R.string.select_your_FMB_copy), landSnackView)
                        return
                    } else if (map[LEASE_AGREEMENT_COPY].toString().isEmpty()) {
                        showSnackView(getString(R.string.select_leaseagreement), landSnackView)
                        return
                    }
                }

                if (map[SOURCE_OF_IRRIGATION].toString() == "Select Irrigation") {
                    showSnackView(getString(R.string.select_your_irrigation), landSnackView)
                    return
                }

                val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val view = inflater.inflate(R.layout.add_more_lay_land_details, null)
                val layOfAddOrRemove = view.findViewById<LinearLayout>(R.id.layOfAddMore)

                val lp = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                lp.setMargins(24, 12, 24, 12)
                view.layoutParams = lp
                view.txtAddMore.text = getString(R.string.remove_item)

                view.txtAddMore.setTextColor(resources.getColor(R.color.colorRed))
                view.txtAddMore.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_remove_circle_red,
                    0
                )
                val landOwner = map[LAND_OWNER] as String

                if (landOwner == "Own") {
                    view.layOfOwn.visibility = View.VISIBLE
                    view.layOfLease.visibility = View.GONE
                } else {
                    view.layOfOwn.visibility = View.VISIBLE
                    view.layOfLease.visibility = View.VISIBLE
                }

                view.spLandOwnership.visibility = View.GONE
                view.edtLandOwnershipInLand.setText("" + landOwner)
                view.edtSurveyNoInLand.setText("" + map[SURVEY_NO])
                view.edtSubDivisionInLand.setText("" + map[SUB_DIVISION])
                view.edtLandAreaInLand.setText("" + map[LAND_AREA])
                view.spSourceOfIrrigationInLand.visibility = View.GONE
                view.edtSourceOfIrrigationInLand.setText("" + map[SOURCE_OF_IRRIGATION])

                Utils.nonEditable(view.edtSurveyNoInLand)
                Utils.nonEditable(view.edtSubDivisionInLand)
                Utils.nonEditable(view.edtLandAreaInLand)

                try {
                    if (map.containsKey(LAND_FMB_COPY)) {
                        if (map[LAND_FMB_COPY].toString().isNotEmpty()) {
                            view.imgFMBCopyInLand.setImageURI(Uri.fromFile((File(map[LAND_FMB_COPY] as String))))
                            view.txtFMBCopyInLand.setText(File(map[LAND_FMB_COPY] as String).name)
                        }
                    }
                    if (map.containsKey(LEASE_AGREEMENT_COPY)) {
                        if (map[LEASE_AGREEMENT_COPY].toString().isNotEmpty()) {
                            view.imgLeaseAgreementInLand.setImageURI(Uri.fromFile((File(map[LEASE_AGREEMENT_COPY] as String))))
                            view.txtLeaseAgreementInLand.setText(File(map[LEASE_AGREEMENT_COPY] as String).name)


                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

                layOfLandDetailsInLand.addView(view)

                layOfAddOrRemove.setOnClickListener {

                    layOfLandDetailsInLand.removeView(view)
                    MessageUtils.showSnackBarRemove(
                        this@AddNewLandDetailsWithAddMore,
                        landSnackView,
                        "One Item Removed Successfully"
                    )
                    appDetailsToAddMore.remove(map)

                    /*  listOfPart.remove(fmbCopy)
                      listOfPart.remove(leaseCopy)*/
                    FMBCopyArray.remove(
                        Utils.multiPartFromFile(
                            FMBCopy,
                            LAND_FMB_COPY + "[]"
                        )
                    )

                    LeaseCopyArray.remove(
                        Utils.multiPartFromFile(
                            leaseAgreement,
                            LEASE_AGREEMENT_COPY + "[]"
                        )
                    )

                    try {
                        var totalAreaForText = 0.0
                        val input = edtLandAreaInLand.text.toString()
                        if (input.isNotEmpty()) {
                            val inputArea = input.toDouble()
                            if (appDetailsToAddMore.size != 0) {
                                for ((index, items) in appDetailsToAddMore.withIndex()) {
                                    val landAreaInLand =
                                        items[LAND_AREA].toString().toDouble()
                                    totalAreaForText = landAreaInLand + inputArea
                                }
                            } else {
                                totalAreaForText = inputArea
                            }
                        } else {
                            if (appDetailsToAddMore.size != 0) {
                                for ((index, items) in appDetailsToAddMore.withIndex()) {
                                    totalAreaForText += items[LAND_AREA].toString()
                                        .toDouble()
                                }
                            }
                        }
                        txtTotalLandAreaInLand.setText("" + totalAreaForText)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(getString(R.string.something_went_wrong), landSnackView)
                    }
                }
            }
        }
    }

    private fun validationForAddMoreLand(map: java.util.HashMap<String, Any>): Boolean {
        when {
            map[LAND_OWNER].toString() == getString(R.string.select_land) -> {
                showSnackView(getString(R.string.select_your_land_ownership), landSnackView)
                return false
            }
            map[SURVEY_NO].toString().isEmpty() -> {
                showSnackView(getString(R.string.please_enter_your_surveyNumber), landSnackView)
                return false
            }
            map[SUB_DIVISION].toString().isEmpty() -> {
                showSnackView(getString(R.string.enter_your_sub_division), landSnackView)
                return false
            }
            map[LAND_AREA].toString().isEmpty() -> {
                showSnackView(getString(R.string.enter_your_land_area), landSnackView)
                return false
            }

            else -> return true
        }

    }

    private fun landAddValidation(map: java.util.HashMap<String, Any>): Boolean {

        if (map[ParamKey.STATE].toString() == "-1" || map[ParamKey.STATE] == getString(R.string.select_state)) {
            showSnackView(getString(R.string.select_your_state), landSnackView)
            return false
        } else if (map[ParamKey.DISTRICT].toString() == "-1" || map[ParamKey.DISTRICT] == getString(
                R.string.select_district
            )
        ) {
            showSnackView(getString(R.string.select_your_district), landSnackView)
            return false
        } else if (map[ParamKey.BLOCK].toString() == "-1" || map[ParamKey.BLOCK] == getString(R.string.select_block)) {
            showSnackView(getString(R.string.select_your_block), landSnackView)
            return false
        } else if (map[ParamKey.VILLAGE].toString() == "-1" || map[ParamKey.VILLAGE] == getString(
                R.string.select_village
            )
        ) {
            showSnackView(getString(R.string.select_your_village), landSnackView)
            return false
        } else if (map[ParamKey.CHITTA_NUMBER].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_chitta_number), landSnackView)
            return false
        } else if (map[ParamKey.PATTA_CHITTA_COPY].toString().isEmpty()) {
            showSnackView(getString(R.string.select_your_chitta_file), landSnackView)
            return false
        } else if (map[AREA_PROPOSED].toString().isEmpty()) {
            showSnackView(getString(R.string.total_area_cant_be_empty), landSnackView)
            return false
        }
        return true
    }

    override fun activityResultCallback(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (data != null) {
            val contentURI = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(
                contentResolver,
                contentURI
            )

            when (requestCode) {
                PATTA_CHITTA -> {

                    pattaChitta = ImageUtils.saveImage(this, bitmap)
                    if (Utils.getFileSize(pattaChitta) <= Utils.IMAGE_SIZE) {
                        imgChittaInLand.setImageBitmap(bitmap)
                        txtChittaName.setText(File(pattaChitta).name)
                    } else {
                        pattaChitta = ""
                        imgChittaInLand.setImageResource(R.drawable.ic_menu_camera)
                        txtChittaName.setText("")
                    }
                }
                FMB_COPY -> {

                    FMBCopy = ImageUtils.saveImage(this, bitmap)
                    if (Utils.getFileSize(FMBCopy) <= Utils.IMAGE_SIZE) {
                        imgFMBCopyInLand.setImageBitmap(bitmap)
                        txtFMBCopyInLand.setText(File(FMBCopy).name)
                    } else {
                        FMBCopy = ""
                        imgFMBCopyInLand.setImageResource(R.drawable.ic_menu_camera)
                        txtFMBCopyInLand.setText("")
                    }
                }
                LEASE_AGREMENT -> {
                    leaseAgreement = ImageUtils.saveImage(this, bitmap)
                    if (Utils.getFileSize(leaseAgreement) <= Utils.IMAGE_SIZE) {
                        imgLeaseAgreementInLand.setImageBitmap(bitmap)
                        txtLeaseAgreementInLand.setText(File(leaseAgreement).name)
                    } else {
                        leaseAgreement = ""
                        imgLeaseAgreementInLand.setImageResource(R.drawable.ic_menu_camera)
                        txtLeaseAgreementInLand.setText("")
                    }
                }
            }
        }
    }

    override fun onError(message: String, api: String) {
        showSnackView(message, landSnackView)
        if (api == ADD_LAND) {
            appDetailsToAddMore.remove(map)
        }
    }

    override fun onActionAfterPermissionGrand(requestCode: Int) {
        RequestPermission.choosePhotoFromGallery(this, requestCode)
    }

    /**
     * All Failure ModelApplicationDetails Comes here to this Page
     */
    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, landSnackView)
    }

    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        when (api) {
            DISTRICT_LIST -> {
                setDistrictSpinner(responseBody)
            }
            BLOCK_LIST -> {
                setBlockSpinner(responseBody)
            }
            VILLAGE_LIST -> {
                setVillageSpinner(responseBody)
            }
            ADD_LAND -> {
                appDetailsToAddMore.remove(map)
                val modelSuccess =
                    Gson().fromJson(
                        responseBody.body()?.string(),
                        ModelRegisterSuccess::class.java
                    )
                if (modelSuccess != null) {
                    if (modelSuccess.status!!) {

                        finish()
                    } else {
                        showSnackView(modelSuccess.message!!, landSnackView)
                    }
                } else {
                    showSnackView(getString(R.string.something_went_wrong), landSnackView)
                }

            }
        }
    }

    /**
     * Get Values From Server and Set in Adapter for Village
     */

    private fun setVillageSpinner(responseBody: Response<ResponseBody>) {
        val modelVillage = Gson().fromJson(
            responseBody.body()?.string(),
            ModelVillageDTO::class.java
        )

        if (modelVillage != null) {
            if (modelVillage.status!!) {
                villageDetails = modelVillage.data as ArrayList<VillageDTO?>
                val count =
                    if (villageDetails != null) villageDetails.size else 0
                if (count != 0) {
                    villageInfo.clear()
                    villageInfo.add(getString(R.string.select_village))
                    for ((indext, values) in villageDetails.withIndex()) {
                        villageInfo.add(values?.village_name!!)
                    }
                    spVillageInLand.adapter = ArrayAdapter(
                        this@AddNewLandDetailsWithAddMore,
                        R.layout.single_view,
                        R.id.singe_item, villageInfo
                    )
                } else {
                    showSnackView(getString(R.string.district_details_not_found), landSnackView)
                }
            } else {
                showSnackView(modelVillage.message!!, landSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), landSnackView)
        }
    }

    /**
     * Get Values From Server and Set in Adapter for Block Spinner
     */
    private fun setBlockSpinner(responseBody: Response<ResponseBody>) {
        val modelBlock = Gson().fromJson(
            responseBody.body()?.string(),
            ModelBlockDTO::class.java
        )

        if (modelBlock != null) {
            if (modelBlock.status!!) {
                blockDetails = modelBlock.data as ArrayList<BlockDTO?>
                val count =
                    if (blockDetails != null) blockDetails.size else 0
                if (count != 0) {
                    blockInfo.clear()
                    blockInfo.add(getString(R.string.select_block))
                    for ((indext, values) in blockDetails.withIndex()) {
                        blockInfo.add(values?.block_name!!)
                    }

                    spBlockInLand.adapter = ArrayAdapter(
                        this@AddNewLandDetailsWithAddMore,
                        R.layout.single_view,
                        R.id.singe_item, blockInfo
                    )
                    setVillageDefault()
                } else {
                    showSnackView(getString(R.string.district_details_not_found), landSnackView)
                }
            } else {
                showSnackView(modelBlock.message!!, landSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), landSnackView)
        }
    }


    /**
     * Get Values From Server and Set in Adapter for District Spinner
     */
    private fun setDistrictSpinner(responseBody: Response<ResponseBody>) {
        val modelDistrict: ModelDistrictDTO = Gson().fromJson(
            responseBody.body()?.string(),
            ModelDistrictDTO::class.java
        )

        if (modelDistrict != null) {

            if (modelDistrict.status!!) {
                districtDetails = modelDistrict.data as ArrayList<DistrictDTO?>
                val count =
                    if (districtDetails != null) districtDetails.size else 0
                if (count != 0) {
                    districtInfo.clear()
                    districtInfo.add(getString(R.string.select_district))
                    for ((indext, values) in districtDetails.withIndex()) {
                        districtInfo.add(values?.district_name!!)
                    }

                    spDistrictInLand.adapter = ArrayAdapter(
                        this@AddNewLandDetailsWithAddMore,
                        R.layout.single_view,
                        R.id.singe_item, districtInfo
                    )

                    setBlockDefault()
                    setVillageDefault()

                } else {
                    showSnackView(getString(R.string.district_details_not_found), landSnackView)
                }
            } else {
                showSnackView(modelDistrict.message!!, landSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), landSnackView)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        MessageUtils.dismissSnackBar(snackView)
    }

}
