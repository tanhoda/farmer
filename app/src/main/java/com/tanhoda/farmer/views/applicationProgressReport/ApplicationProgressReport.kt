package com.tanhoda.farmer.views.applicationProgressReport

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.adapter.applicationStatusItem.ItemApplicationProgress
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.applicationStatus.ModelApplicationDetailsDTO
import com.tanhoda.farmer.model.login.FarmerDetails
import com.tanhoda.farmer.utils.SessionManager
import kotlinx.android.synthetic.main.activity_application_progress_details.*
import okhttp3.ResponseBody
import retrofit2.Response

class ApplicationProgressReport : BaseActivity(), ReadWriteAPI {

    var farmerDetails: FarmerDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.application_progress_details)
        setContentView(R.layout.activity_application_progress_details)
        farmerDetails = SessionManager.getObject(this@ApplicationProgressReport)
        val map = HashMap<String, String>()
        map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(this)
        map[ParamKey.VILLAGE] = farmerDetails!!.village!!
        Connector.callBasic(this, map, this, ParamAPI.READ_APPLICATION_STATUS)


    }

    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        if (responseBody.body() != null) {
            val modelApplicationDetails =
                Gson().fromJson(
                    responseBody.body()?.string(),
                    ModelApplicationDetailsDTO::class.java
                )
            if (modelApplicationDetails != null) {
                if (modelApplicationDetails.status!!) {
                    if (modelApplicationDetails.data!!.isNotEmpty()) {
                        noTxtApplicationProgress.visibility = View.GONE
                        rcyApplicationStatus.visibility = View.VISIBLE
                        rcyApplicationStatus.layoutManager =
                            LinearLayoutManager(this@ApplicationProgressReport)
                        rcyApplicationStatus.adapter =
                            ItemApplicationProgress(this, modelApplicationDetails.data)
                    } else {
                        rcyApplicationStatus.visibility = View.GONE
                        noTxtApplicationProgress.visibility = View.VISIBLE
                        noTxtApplicationProgress.text = "Application Status Not Found"
                    }
                } else {
                    rcyApplicationStatus.visibility = View.GONE
                    noTxtApplicationProgress.visibility = View.VISIBLE
                    noTxtApplicationProgress.text = modelApplicationDetails.message!!
                }
            } else {
                rcyApplicationStatus.visibility = View.GONE
                noTxtApplicationProgress.visibility = View.VISIBLE
                noTxtApplicationProgress.text = getString(R.string.something_went_wrong)
                showSnackView(getString(R.string.something_went_wrong), layOfApplicationStatus)
            }
        } else {
            rcyApplicationStatus.visibility = View.GONE
            noTxtApplicationProgress.visibility = View.VISIBLE
            noTxtApplicationProgress.text = getString(R.string.something_went_wrong)
            showSnackView(getString(R.string.something_went_wrong), layOfApplicationStatus)
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, layOfApplicationStatus)
        noTxtApplicationProgress.text = message
        noTxtApplicationProgress.visibility = View.VISIBLE
        rcyApplicationStatus.visibility = View.GONE
    }


}
