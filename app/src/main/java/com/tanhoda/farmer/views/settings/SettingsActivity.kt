package com.tanhoda.farmer.views.settings

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.MainActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.utils.LocaleHelper
import com.tanhoda.farmer.utils.MessageUtils
import kotlinx.android.synthetic.main.fragment_splash.*
import java.lang.Exception

class SettingsActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_tools)
        title = getString(R.string.app_settings)

        languageToggle.setOnClickListener {
            var language = ""

            try {
                language = LocaleHelper.getLanguage(this@SettingsActivity)
                if (language == "ta") {
                    languageToggle.textOff = getString(R.string.english)
                } else {
                    languageToggle.textOff = getString(R.string.tamil)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }


            if (language == "en") {
                language = LocaleHelper.getLanguage(this@SettingsActivity)
                LocaleHelper.setLocale(this@SettingsActivity, "ta")
                LocaleHelper.persistForLanguage(this@SettingsActivity, true)
                //recreate()
                val i = Intent(this, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP;
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i)
            } else {
                language = LocaleHelper.getLanguage(this@SettingsActivity)
                LocaleHelper.setLocale(this@SettingsActivity, "en")
                LocaleHelper.persistForLanguage(this@SettingsActivity, true)
                //recreate()
                finish()
                val i = Intent(this, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP;
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i)
            }
        }
    }

}