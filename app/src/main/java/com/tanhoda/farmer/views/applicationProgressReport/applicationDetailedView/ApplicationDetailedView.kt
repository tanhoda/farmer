package com.tanhoda.farmer.views.applicationProgressReport.applicationDetailedView

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.adapter.ItemMultiImages
import com.tanhoda.farmer.adapter.applicationDocumentStatus.ItemLandSurveyNumber
import com.tanhoda.farmer.adapter.applicationDocumentStatus.ItemViewApplicationStatus
import com.tanhoda.farmer.adapter.applicationDocumentStatus.ItemsDocuments
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI.Companion.APPLICATION_DETAILS
import com.tanhoda.farmer.api.ParamAPI.Companion.UPLOAD_AFFIDAVIT
import com.tanhoda.farmer.api.ParamAPI.Companion.UPLOAD_ESTIMATION
import com.tanhoda.farmer.api.ParamAPI.Companion.UPLOAD_INVOICE
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ParamKey.Companion.AFFIDAVIT_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.APPROVAL_ID
import com.tanhoda.farmer.api.ParamKey.Companion.BANK_LETTER
import com.tanhoda.farmer.api.ParamKey.Companion.ESTIMATION_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.INVOICE_COPY
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.applicationViewDetails.*
import com.tanhoda.farmer.model.register.registerSuccess.ModelRegisterSuccess
import com.tanhoda.farmer.utils.*
import com.tanhoda.farmer.utils.Utils.Companion.D_FORMAL
import com.tanhoda.farmer.utils.Utils.Companion.D_NEW_APPLICATION_DATE_FORMAT
import com.tanhoda.farmer.utils.Utils.Companion.convertDate
import com.tanhoda.farmer.utils.permissions.CallbackPermission
import com.tanhoda.farmer.utils.permissions.PermissionDescription
import com.tanhoda.farmer.utils.permissions.RequestPermission
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.AFFIDAVIT
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.BANK_LOAN_IN_DIALOG
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.ESTIMATION_UPLOAD
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.INVOICE
import com.tanhoda.farmer.views.PDFViewActivity
import com.tanhoda.farmer.views.documentReUpload.PendingActivity
import com.tanhoda.farmer.views.documentReUpload.RejectedActivity
import kotlinx.android.synthetic.main.activity_view_status.*
import kotlinx.android.synthetic.main.dialgo_upload_image.*
import kotlinx.android.synthetic.main.fragment_land_list.*
import kotlinx.android.synthetic.main.lay_basic_details.*
import kotlinx.android.synthetic.main.lay_of_land_details.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*
import kotlinx.android.synthetic.main.submit_cancel_vertical_view.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File

class ApplicationDetailedView : BaseActivity(), ReadWriteAPI, View.OnClickListener,
    CallbackPermission {

    var affidavit = ""
    var estimationUpload = ""
    var invoice = ""
    var imgUploadImage: ImageView? = null
    var rcyMultiImages: RecyclerView? = null
    var txtImageName: EditText? = null
    var layOfDialog: LinearLayout? = null
    var appId = ""
    var statusId = -1

    var dialogUpload: Dialog? = null

    var documentsStatusForBankLoan = 0
    var bankLoanCopy = ""
    var imgBankUploadImage: ImageView? = null
    var txtBankImageName: EditText? = null
    var iplImageName: TextInputLayout? = null
    var txtAffidavit: TextView? = null
    var txtBankCopy: TextView? = null

    var modelRejectedDocument: ArrayList<ModelRejectedDocument>? = null
    var modelPendingDocument: ArrayList<ModelPendingDocument>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_status)

        try {
            initCallbackPermission(this)
            layOfBasic.visibility = View.VISIBLE
            layOfAffidavit.visibility = View.GONE
            layOfEstimationUpload.visibility = View.GONE
            layOfInvoice.visibility = View.GONE


            layOfAffidavit.setOnClickListener(this)
            layOfEstimationUpload.setOnClickListener(this)
            layOfInvoice.setOnClickListener(this)
            work_order_release.setOnClickListener(this)
            btnCancel.text = getString(R.string.rejected_document)
            btnSuccess.text = getString(R.string.pending_document)
            btnCancel.setOnClickListener(this)
            btnSuccess.setOnClickListener(this)

            appId = intent.getStringExtra(ParamKey.APP_ID)!!


            val map = HashMap<String, String>()
            map[ParamKey.APP_ID] = appId
            map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(this)
            Connector.callBasic(this, map, this, APPLICATION_DETAILS)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.layOfAffidavit -> {
                try {
                    showDialogForUploadImage(AFFIDAVIT)

                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
            R.id.layOfEstimationUpload -> {
                try {
                    showDialogForUploadImage(ESTIMATION_UPLOAD)
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
            R.id.layOfInvoice -> {
                try {
                    showDialogForUploadImage(INVOICE)
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
            R.id.btnCancel -> {
                startActivity(
                    Intent(
                        this,
                        RejectedActivity::class.java
                    ).putExtra(ParamKey.REJECTED_DOCUMENT, modelRejectedDocument)
                )
            }
            R.id.btnSuccess -> {
                startActivity(
                    Intent(
                        this,
                        PendingActivity::class.java
                    ).putExtra(ParamKey.PENDING_DOCUMENT, modelPendingDocument)
                )
            }
            R.id.work_order_release -> {
                startActivity(
                    Intent(this@ApplicationDetailedView, PDFViewActivity::class.java).putExtra(
                        "fileName",
                        appId
                    ).putExtra("type", getString(R.string.work_order_release))
                )
                overridePendingTransition(0, 0)
            }

        }
    }

    /**
     * Show Dialog and Ask Permission and Upload Images
     */
    private fun showDialogForUploadImage(requestCode: Int) {
        try {
            Log.d("requestCodesd", "" + requestCode)
            dialogUpload = Dialog(this@ApplicationDetailedView)
            if (dialogUpload != null) {
                dialogUpload!!.setCancelable(false)
                dialogUpload!!.setContentView(R.layout.dialgo_upload_image)
                dialogUpload!!.window.setBackgroundDrawableResource(R.color.colorDialogTrans)
                dialogUpload!!.window.setGravity(Gravity.BOTTOM)

                rcyMultiImages = dialogUpload!!.findViewById(R.id.rcyMultiImages)
                rcyMultiImages!!.layoutManager = LinearLayoutManager(this)
                imgUploadImage = dialogUpload!!.findViewById(R.id.imgUploadImage)
                txtImageName = dialogUpload!!.findViewById(R.id.txtImageName)
                iplImageName = dialogUpload!!.findViewById(R.id.iplImageName)
                layOfDialog = dialogUpload!!.findViewById(R.id.layOfDialog)
                txtAffidavit = dialogUpload!!.findViewById(R.id.txtAffidavit)
                txtBankCopy = dialogUpload!!.findViewById(R.id.txtBankCopy)

                val layOfBankCopy = dialogUpload!!.findViewById<LinearLayout>(R.id.layOfBankCopy)
                txtBankImageName = dialogUpload!!.findViewById(R.id.txtBankImageName)
                imgBankUploadImage = dialogUpload!!.findViewById(R.id.imgBankUploadImage)
                layOfBankCopy.visibility = View.GONE

                dialogUpload!!.done_permission.text = getString(R.string.upload)

                when (requestCode) {
                    AFFIDAVIT -> {
                        dialogUpload!!.dialog_title_name.text = getString(R.string.upload_affidavit)
                        iplImageName!!.hint = getString(R.string.select_affidavit_copy)
                    }
                    ESTIMATION_UPLOAD -> {
                        dialogUpload!!.dialog_title_name.text =
                            getString(R.string.upload_estiamtion)
                        iplImageName!!.hint = getString(R.string.select_upload_estiamtion_copy)
                    }
                    INVOICE -> {
                        dialogUpload!!.dialog_title_name.text = getString(R.string.upload_invoice)
                        iplImageName!!.setHint(getString(R.string.select_upload_invoice))
                        //documentsstatus2!=0
                        if (documentsStatusForBankLoan != 0) {
                            layOfBankCopy.visibility = View.VISIBLE
                        } else {
                            layOfBankCopy.visibility = View.GONE
                        }
                    }
                }

                imgBankUploadImage!!.setOnClickListener {
                    checkPermission(BANK_LOAN_IN_DIALOG)
                }

                /**
                 * Upload Estimation Image
                 */
                imgUploadImage!!.setOnClickListener {
                    checkPermission(requestCode)
                }

                dialogUpload!!.done_permission.setOnClickListener {

                    val map = HashMap<String, Any>()
                    val imageMapList = ArrayList<MultipartBody.Part>()
                    val remarks = dialogUpload!!.edtUploadRemarks.text.toString().trim()
                    map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(this).toInt()
                    map[ParamKey.APP_ID] = appId.toInt()
                    map[APPROVAL_ID] = statusId.toInt()
                    map[ParamKey.REMARKS] = remarks

                    when (requestCode) {
                        AFFIDAVIT -> {
                            if (affidavit.isNotEmpty()) {
                                if (remarks.isNotEmpty()) {
                                    //map[AFFIDAVIT_COPY] = affidavit
                                    imageMapList.add(
                                        Utils.multiPartFromFile(
                                            affidavit,
                                            AFFIDAVIT_COPY
                                        )
                                    )
                                    Connector.callBasicWithPart(
                                        this,
                                        map,
                                        imageMapList,
                                        this,
                                        UPLOAD_AFFIDAVIT
                                    )
                                } else {
                                    showSnackView(
                                        getString(R.string.remarks_can_be_empty),
                                        layOfDialog!!
                                    )
                                }
                            } else {
                                showSnackView(
                                    getString(R.string.please_select_affidavit),
                                    layOfDialog!!
                                )
                            }
                        }
                        ESTIMATION_UPLOAD -> {
                            if (invoicefileList.isNotEmpty()) {  // Multiple Image Uploads
                                if (remarks.isNotEmpty()) {
                                    // map[ESTIMATION_COPY] = estimationUpload
                                    for (item in invoicefileList) {
                                        Log.d("sldkjskldjf", "" + item.absolutePath)
                                        imageMapList.add(
                                            Utils.multiPartFromFile(
                                                item.absolutePath, "$ESTIMATION_COPY[]"
                                            )
                                        )
                                    }
                                    Log.d("imageMapList", "" + imageMapList.size)
                                    Connector.callBasicWithPart(
                                        this,
                                        map,
                                        imageMapList,
                                        this,
                                        UPLOAD_ESTIMATION
                                    )
                                } else {
                                    showSnackView(
                                        getString(R.string.remarks_can_be_empty),
                                        layOfDialog!!
                                    )
                                }
                            } else if (estimationUpload.isNotEmpty()) { // Single Image Upload
                                if (remarks.isNotEmpty()) {
                                    // map[ESTIMATION_COPY] = estimationUpload
                                    imageMapList.add(
                                        Utils.multiPartFromFile(
                                            estimationUpload, ESTIMATION_COPY
                                        )
                                    )

                                    Connector.callBasicWithPart(
                                        this,
                                        map,
                                        imageMapList,
                                        this,
                                        UPLOAD_ESTIMATION
                                    )
                                } else {
                                    showSnackView(
                                        getString(R.string.remarks_can_be_empty),
                                        layOfDialog!!
                                    )
                                }
                            } else {
                                showSnackView(
                                    getString(R.string.please_select_estimation_copy),
                                    layOfDialog!!
                                )
                            }
                        }
                        INVOICE -> {
                            if (invoice.isNotEmpty()) {
                                if (remarks.isNotEmpty()) {
                                    // map[INVOICE_COPY] = invoice
                                    // map[BANK_LETTER] = bankLoanCopy
                                    if (Utils.checkFile(invoice)) {
                                        imageMapList.add(
                                            Utils.multiPartFromFile(
                                                invoice,
                                                INVOICE_COPY
                                            )
                                        )
                                    }

                                    if (Utils.checkFile(bankLoanCopy)) {
                                        imageMapList.add(
                                            Utils.multiPartFromFile(
                                                bankLoanCopy,
                                                BANK_LETTER
                                            )
                                        )
                                    }


                                    Connector.callBasicWithPart(
                                        this,
                                        map,
                                        imageMapList,
                                        this,
                                        UPLOAD_INVOICE
                                    )
                                } else {
                                    showSnackView(
                                        getString(R.string.remarks_can_be_empty),
                                        layOfDialog!!
                                    )
                                }
                            } else {
                                showSnackView(
                                    getString(R.string.please_select_invoice_copy),
                                    layOfDialog!!
                                )
                            }
                        }
                    }


                }

                dialogUpload!!.cancel_permission.setOnClickListener {
                    MessageUtils.dismissDialog(dialogUpload)
                }
                dialogUpload!!.show()
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }

    }


    fun checkPermission(requestCode: Int) {

        RequestPermission.checkPermissions(
            requestCode, this, this as CallbackPermission,
            PermissionDescription.permissionForCameraAndStorage(this),
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
            )
        )
    }

    var invoicefileList = ArrayList<File>()

    override fun activityResultCallback(requestCode: Int, resultCode: Int, data: Intent?) {
        try {

            if (requestCode == ESTIMATION_UPLOAD && resultCode == Activity.RESULT_OK && data!!.clipData != null) {
                Log.d("invoicefileListsdsd", "$data   clipData   ${data!!.clipData}")
                //if (data!!.clipData != null) {
                for (i: Int in 0 until data!!.clipData.itemCount) {
                    val file = CompressFile.getCompressedImageFile(
                        File(
                            PathUtil.getPath(
                                applicationContext,
                                data.clipData.getItemAt(i).uri
                            )
                        ), applicationContext
                    )
                    if (file != null) {
                        val fileSize = file?.length()!! / 1024
                        if (fileSize <= 300) {
                            invoicefileList?.add(file)
                            Log.d("InVoiceIMAge SIze", "" + fileSize)
                        }
                    }
                }
                Log.d("invoicefileListsd", "" + invoicefileList.size)
                val adapter = ItemMultiImages(this, invoicefileList)
                rcyMultiImages!!.visibility = View.VISIBLE
                rcyMultiImages!!.adapter = adapter

                // }

            } else {
                if (data != null) {

                    val contentURI = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(
                        contentResolver,
                        contentURI
                    )
                    Log.d("selectFromGalleryAfter", "" + requestCode)
                    when (requestCode) {
                        AFFIDAVIT -> {
                            affidavit = ImageUtils.saveImage(this, bitmap)
                            if (Utils.getFileSize(affidavit) <= Utils.IMAGE_SIZE) {
                                imgUploadImage!!.setImageBitmap(bitmap)
                                txtImageName!!.setText(File(affidavit).name)
                                txtAffidavit!!.visibility = View.GONE
                            } else {
                                affidavit = ""
                                imgUploadImage!!.setImageResource(R.drawable.ic_menu_camera)
                                txtImageName!!.setText("")
                                txtAffidavit!!.visibility = View.VISIBLE
                            }
                        }
                        ESTIMATION_UPLOAD -> {

                            Log.d(
                                "selectFromGalleryFromResult",
                                "requestCode " + requestCode + " datas " + data.data
                            )
                            estimationUpload = ImageUtils.saveImage(this, bitmap)
                            if (Utils.getFileSize(estimationUpload) <= Utils.IMAGE_SIZE) {
                                imgUploadImage!!.setImageBitmap(bitmap)
                                txtImageName!!.setText(File(estimationUpload).name)
                                txtAffidavit!!.visibility = View.GONE
                            } else {
                                estimationUpload = ""
                                imgUploadImage!!.setImageResource(R.drawable.ic_menu_camera)
                                txtImageName!!.setText("")
                                txtAffidavit!!.visibility = View.VISIBLE
                            }

                        }
                        INVOICE -> {
                            invoice = ImageUtils.saveImage(this, bitmap)
                            if (Utils.getFileSize(invoice) <= Utils.IMAGE_SIZE) {
                                imgUploadImage!!.setImageBitmap(bitmap)
                                txtImageName!!.setText(File(invoice).name)
                                txtAffidavit!!.visibility = View.GONE
                            } else {
                                invoice = ""
                                imgUploadImage!!.setImageResource(R.drawable.ic_menu_camera)
                                txtImageName!!.setText("")
                                txtAffidavit!!.visibility = View.VISIBLE
                            }
                        }
                        BANK_LOAN_IN_DIALOG -> {
                            bankLoanCopy = ImageUtils.saveImage(this, bitmap)
                            if (Utils.getFileSize(bankLoanCopy) <= Utils.IMAGE_SIZE) {
                                imgBankUploadImage!!.setImageBitmap(bitmap)
                                txtBankImageName!!.setText(File(bankLoanCopy).name)
                                txtBankCopy!!.visibility = View.GONE
                            } else {
                                bankLoanCopy = ""
                                imgBankUploadImage!!.setImageResource(R.drawable.ic_menu_camera)
                                txtBankImageName!!.setText("")
                                txtBankCopy!!.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            showSnackView(ex.message!!, layOfDetailedSnack)
        }
    }

    override fun onError(message: String, api: String) {
        Log.d("sdsd", "onErrorapi $api")
        when (api) {
            UPLOAD_AFFIDAVIT, UPLOAD_ESTIMATION, UPLOAD_INVOICE -> {
                showSnackView(message, layOfDialog!!)
                val wallpaperDirectory =
                    File(Environment.getExternalStorageDirectory().toString() + ImageUtils.IMAGE_DIRECTORY)
                wallpaperDirectory.delete()
            }
            else -> {
                showSnackView(message, layOfDetailedSnack)
            }
        }

    }

    override fun onActionAfterPermissionGrand(requestCode: Int) {
        RequestPermission.choosePhotoFromGallery(this, requestCode)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        try {
            if (responseBody.body() != null) {
                when (api) {
                    APPLICATION_DETAILS -> {
                        getApplicationDetails(responseBody)
                    }
                    UPLOAD_AFFIDAVIT, UPLOAD_ESTIMATION, UPLOAD_INVOICE -> {
                        val success = Gson().fromJson(
                            responseBody.body()?.string(),
                            ModelRegisterSuccess::class.java
                        )
                        if (success != null) {
                            if (success.status!!) {
                                MessageUtils.dismissDialog(dialogUpload!!)
                                MessageUtils.showToastMessage(this, success.message!!, false)
                                when (api) {
                                    UPLOAD_AFFIDAVIT -> {
                                        imgAffidavit.setImageURI(Uri.fromFile(File(affidavit)))
                                    }
                                    UPLOAD_ESTIMATION -> {
                                        if (estimationUpload.isNotEmpty())
                                            imgEstimationUpload.setImageURI(
                                                Uri.fromFile(
                                                    File(
                                                        estimationUpload
                                                    )
                                                )
                                            )
                                    }
                                    UPLOAD_INVOICE -> {
                                        imgInvoice.setImageURI(Uri.fromFile(File(invoice)))
                                    }
                                }

                                val map = HashMap<String, String>()
                                map[ParamKey.APP_ID] = appId
                                map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(this)
                                Connector.callBasic(this, map, this, APPLICATION_DETAILS)

                            } else {
                                showSnackView(success.message!!, layOfDialog!!)
                                when (api) {
                                    UPLOAD_AFFIDAVIT -> {
                                        imgAffidavit.setImageURI(Uri.fromFile(File(affidavit)))
                                    }
                                    UPLOAD_ESTIMATION -> {
                                        imgEstimationUpload.setImageURI(
                                            Uri.fromFile(
                                                File(
                                                    estimationUpload
                                                )
                                            )
                                        )
                                    }
                                    UPLOAD_INVOICE -> {
                                        imgInvoice.setImageURI(Uri.fromFile(File(invoice)))
                                    }
                                }
                            }
                        } else {
                            showSnackView(getString(R.string.something_went_wrong), layOfDialog!!)

                        }
                    }

                }

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun getApplicationDetails(responseBody: Response<ResponseBody>) {
        val modelApplicationDetails = Gson().fromJson(
            responseBody.body()?.string(),
            ModelApplicationDetails::class.java
        )
        if (modelApplicationDetails != null) {
            if (modelApplicationDetails.status!!) {
                if (modelApplicationDetails.data != null) {

                    //show information
                    showInformation(modelApplicationDetails.data)

                    //Status View List
                    val viewStatusUpdates =
                        if (modelApplicationDetails.data.viewstatusupdates != null) modelApplicationDetails.data.viewstatusupdates.size else 0

                    if (viewStatusUpdates != 0) {
                        layOfApplicationStatus.visibility = View.VISIBLE
                        re_ho_app_land_view_status_list.layoutManager =
                            LinearLayoutManager(this)
                        val model = ViewStatusUpdatesItem(
                            "New Application",
                            "Completed",
                            convertDate(
                                modelApplicationDetails.data.newapplication!!.applicationDate!!,
                                D_NEW_APPLICATION_DATE_FORMAT,
                                D_FORMAL
                            )
                        )

                        Log.d(
                            "statusdsd",
                            "" + modelApplicationDetails.data.viewstatusupdates!!.size
                        )
                        modelApplicationDetails.data.viewstatusupdates!!.add(0, model)

                        Log.d(
                            "statusdsd",
                            "after " + modelApplicationDetails.data.viewstatusupdates.size
                        )
                        re_ho_app_land_view_status_list.adapter = ItemViewApplicationStatus(
                            this,
                            modelApplicationDetails.data.viewstatusupdates
                        )

                        try {
                            modelRejectedDocument = modelApplicationDetails.data.rejecteddocuments
                            modelPendingDocument = modelApplicationDetails.data.pendingdocuments

                            val rejectCount = modelRejectedDocument?.size ?: 0
                            val pendingCount = modelPendingDocument?.size ?: 0


                            if (pendingCount != 0 || rejectCount != 0) {
                                layOfHorizontal.visibility = View.VISIBLE
                                if (pendingCount != 0 && rejectCount != 0) {
                                    btnCancel.visibility = View.VISIBLE
                                    btnSuccess.visibility = View.VISIBLE
                                } else if (pendingCount != 0) {
                                    btnCancel.visibility = View.GONE
                                    btnSuccess.visibility = View.VISIBLE
                                } else if (rejectCount != 0) {
                                    btnCancel.visibility = View.VISIBLE
                                    btnSuccess.visibility = View.GONE
                                }

                            } else {
                                layOfHorizontal.visibility = View.GONE
                            }
                        } catch (ex: java.lang.Exception) {
                            ex.printStackTrace()
                        }
                    } else {
                        layOfApplicationStatus.visibility = View.GONE
                    }

                    // Document Status List
                    val documentList: ArrayList<DocumentsItem> =
                        modelApplicationDetails.data.documents!!.clone() as ArrayList<DocumentsItem>

                    val countDocument =
                        documentList.size
                    if (countDocument != 0) {
                        for ((index, items) in modelApplicationDetails.data.documents!!.withIndex()) {
                            try {
                                if (items.imageExtension != null) {
                                    if (items.imageExtension.toString().toLowerCase() == "png" || items.imageExtension.toString().toLowerCase() == "jpg" || items.imageExtension.toString().toLowerCase() == "jpeg") {
                                        Log.d("askalksal", "Notremove " + items.documentName)

                                    } else {
                                        Log.d("askalksal", "remove1  " + items.documentName)
                                        documentList.remove(items)
                                    }
                                } else {
                                    Log.d("askalksal", "remove122  " + items.documentName)
                                    documentList.remove(items)
                                }
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }

                        layOfDocument.visibility = View.VISIBLE
                        re_ho_app_land_document_verify.layoutManager =
                            GridLayoutManager(this, 2)
                        Log.d("statusdsd", "documentList " + documentList.size)
                        re_ho_app_land_document_verify.adapter =
                            ItemsDocuments(this, documentList)
                    } else {
                        layOfDocument.visibility = View.GONE
                    }
                } else {
                    view_status_empty_view.visibility = View.VISIBLE
                    view_status_empty_view.text = getString(R.string.something_went_wrong)
                    scrollableIsView.visibility = View.GONE
                }

            } else {
                view_status_empty_view.visibility = View.VISIBLE
                view_status_empty_view.text = modelApplicationDetails.message!!
                scrollableIsView.visibility = View.GONE
            }
        } else {
            view_status_empty_view.visibility = View.VISIBLE
            view_status_empty_view.text = getString(R.string.something_went_wrong)
            scrollableIsView.visibility = View.GONE
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun showInformation(appDetailsDataModel: AppDetailsDataModel) {

        documentsStatusForBankLoan = appDetailsDataModel.documentsstatus2!!
        if (appDetailsDataModel.newapplication != null) {
            try {
                txtAppId.text = Utils.htmlCodeConvert(
                    getString(R.string.application_id),
                    if (appDetailsDataModel.newapplication != null) appDetailsDataModel.newapplication.applicationId.toString() else "-"
                )
                txtSchemeName.text =
                    Utils.htmlCodeConvert(
                        getString(R.string.scheme_name),
                        if (appDetailsDataModel.newapplication!!.schemename != null) appDetailsDataModel.newapplication.schemename!!.schemeName.toString() else "-"
                    )

                txtComponentName.text =
                    Utils.htmlCodeConvert(
                        getString(R.string.component_name),
                        if (appDetailsDataModel.newapplication.categoryname != null) appDetailsDataModel.newapplication.categoryname.category.toString() else "-"
                    )
                txtSubComponentName.text =
                    Utils.htmlCodeConvert(
                        getString(R.string.sub_component),
                        if (appDetailsDataModel.newapplication.componentname != null) appDetailsDataModel.newapplication.componentname.componentName.toString() else "-"
                    )

                txtAppStatus.text = Utils.htmlCodeConvert(
                    getString(R.string.application_status),
                    if (appDetailsDataModel.newapplication != null) appDetailsDataModel.newapplication.applicationStatus.toString() else "-"
                )
                try {
                    if (appDetailsDataModel.newapplication.waitingFor.toString().isNotEmpty()) {
                        if (appDetailsDataModel.newapplication.waitingFor.toString().toLowerCase() != "Completed".toLowerCase()) {
                            txtAppWaitingFor.visibility = View.VISIBLE
                            txtAppWaitingFor.text = Utils.htmlCodeConvert(
                                getString(R.string.waiting_for),
                                appDetailsDataModel.newapplication.waitingFor.toString()
                            )
                        } else {
                            txtAppWaitingFor.visibility = View.GONE
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                edtPreviouslySubsidy.setText(appDetailsDataModel.newapplication!!.govtSubsidy)
                edtRemarks.setText(appDetailsDataModel.newapplication!!.relevant)
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
        }
        try {
            if (appDetailsDataModel!!.workorder != 0) {
                layOfWorkOrder.visibility = View.VISIBLE
            } else {
                layOfWorkOrder.visibility = View.GONE
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }


        try {
            if (appDetailsDataModel.statusupdate != null) {
                statusId = appDetailsDataModel.statusupdate!!.id!!
                when (appDetailsDataModel.statusupdate.statusId!!) {
                    4 -> { // for Affidavit
                        layOfAffidavit.visibility = View.VISIBLE
                        layOfEstimationUpload.visibility = View.GONE
                        layOfInvoice.visibility = View.GONE
                    }
                    19 -> { //for Estimation Upload
                        layOfAffidavit.visibility = View.GONE
                        layOfEstimationUpload.visibility = View.VISIBLE
                        layOfInvoice.visibility = View.GONE
                    }
                    20 -> { //for Invoice Upload
                        layOfAffidavit.visibility = View.GONE
                        layOfEstimationUpload.visibility = View.GONE
                        layOfInvoice.visibility = View.VISIBLE
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }


        try {
            val countland =
                if (appDetailsDataModel.lands == null) 0
                else {
                    appDetailsDataModel.lands.size
                }
            Log.d("statusdsd", "lanCount " + countland)
            if (countland != 0) {
                layOfLandIncluded.visibility = View.VISIBLE
                recy_land_det_list.layoutManager = LinearLayoutManager(this)
                recy_land_det_list.adapter = ItemLandSurveyNumber(this, appDetailsDataModel.lands)
            } else {
                layOfLandIncluded.visibility = View.GONE
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResponseFailure(message: String, api: String) {

        when (api) {
            APPLICATION_DETAILS -> {
                view_status_empty_view.visibility = View.VISIBLE
                view_status_empty_view.text = getString(R.string.something_went_wrong)
                scrollableIsView.visibility = View.GONE
                showSnackView(message, layOfDetailedSnack)
            }
            UPLOAD_AFFIDAVIT, UPLOAD_ESTIMATION, UPLOAD_INVOICE -> {
                showSnackView(message, layOfDialog!!)
            }
        }
    }


}
