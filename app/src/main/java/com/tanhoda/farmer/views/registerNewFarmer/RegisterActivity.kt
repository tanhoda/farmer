package com.tanhoda.farmer.views.registerNewFarmer


import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI.Companion.AADHAR_CHECK
import com.tanhoda.farmer.api.ParamAPI.Companion.BLOCK_LIST
import com.tanhoda.farmer.api.ParamAPI.Companion.DISTRICT_LIST
import com.tanhoda.farmer.api.ParamAPI.Companion.FARMER_UPDATE
import com.tanhoda.farmer.api.ParamAPI.Companion.REGISTER
import com.tanhoda.farmer.api.ParamAPI.Companion.VILLAGE_LIST
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ParamKey.Companion.AADHAR_COPY
import com.tanhoda.farmer.api.ParamKey.Companion.PROFILE_PIC
import com.tanhoda.farmer.api.ParamKey.Companion.RATION_COPY
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.login.FarmerDetails
import com.tanhoda.farmer.model.profile.updateProfile.ModelProfileUpdate
import com.tanhoda.farmer.model.register.aadharValidation.AadharValidate
import com.tanhoda.farmer.model.register.blockDetails.BlockDTO
import com.tanhoda.farmer.model.register.blockDetails.ModelBlockDTO
import com.tanhoda.farmer.model.register.district.DistrictDTO
import com.tanhoda.farmer.model.register.district.ModelDistrictDTO
import com.tanhoda.farmer.model.register.registerSuccess.ModelRegisterSuccess
import com.tanhoda.farmer.model.register.village.ModelVillageDTO
import com.tanhoda.farmer.model.register.village.VillageDTO
import com.tanhoda.farmer.utils.*
import com.tanhoda.farmer.utils.Utils.Companion.EMAIL_PATTERN
import com.tanhoda.farmer.utils.permissions.CallbackPermission
import com.tanhoda.farmer.utils.permissions.PermissionDescription

import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.AADHAR_CARD
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.PROFILE_IMAGE
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.RATION_CARD
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.checkPermissions
import com.tanhoda.farmer.utils.permissions.RequestPermission.Companion.choosePhotoFromGallery

import kotlinx.android.synthetic.main.layout_register.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File
import java.lang.Exception

import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import com.tanhoda.farmer.utils.Utils.Companion.FORMAT_PAN_CARD
import com.tanhoda.farmer.utils.Utils.Companion.getAgeFromDOB
import com.tanhoda.farmer.utils.Utils.Companion.hideSoftKeyboard
import com.tanhoda.farmer.utils.Utils.Companion.isValidInputFormat
import com.tanhoda.farmer.utils.Utils.Companion.showImage
import com.tanhoda.farmer.utils.Utils.Companion.validCellPhone
import okhttp3.MultipartBody
import java.util.*


class RegisterActivity : BaseActivity(), ReadWriteAPI, CallbackPermission {

    var districtDetails = ArrayList<DistrictDTO?>()
    var districtInfo = ArrayList<String>()

    var blockDetails = ArrayList<BlockDTO?>()
    var blockInfo = ArrayList<String>()

    var villageDetails = ArrayList<VillageDTO?>()
    var villageInfo = ArrayList<String>()

    var profile_pic = ""
    var ration_copy = ""
    var aadhaar_copy = ""
    var readWriteAPI: ReadWriteAPI? = null

    var districtId = -1
    var blockId = -1
    var villageId = -1
    var state_assign = -1
    var sessionManager: SessionManager? = null
    var farmerDetails: FarmerDetails? = null
    var from = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_register)

        sessionManager = SessionManager(this)
        BaseActivity("")
        initCallbackPermission(this)
        readWriteAPI = this
        try {
            val bundle = intent
            from = bundle.getStringExtra("from")
            if (from == "register") {
                title = getString(R.string.register)
                layOfActions.visibility = View.VISIBLE
            } else {
                title = getString(R.string.profile_info)
                layOfActions.visibility = View.VISIBLE
                btnSuccess.text = getString(R.string.update)
                //callProfileDetails()
                farmerDetails = SessionManager.getObject(this)
                updateProfile(farmerDetails!!)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        sizeProfile.visibility = View.GONE
        sizeAadharCard.visibility = View.GONE
        sizeRationCard.visibility = View.GONE

        if (from != "register") {
            Utils.nonEditable(regEdtAadharNumber)
            Utils.nonEditable(regEdtMobileNUmber)
        }

        regEdtPhoto.setOnClickListener {
            checkPermissions(
                PROFILE_IMAGE, this, this as CallbackPermission,
                PermissionDescription.permissionForCameraAndStorage(this),
                arrayOf(WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA)
            )
        }

        regEdtAadhaarCardCopy.setOnClickListener {
            checkPermissions(
                AADHAR_CARD, this, this as CallbackPermission,
                PermissionDescription.permissionForCameraAndStorage(this),
                arrayOf(WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA)
            )
        }

        regEdtRationCardCopy.setOnClickListener {
            checkPermissions(
                RATION_CARD, this, this as CallbackPermission,
                PermissionDescription.permissionForCameraAndStorage(this),
                arrayOf(WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA)
            )
        }


        btnSuccess.setOnClickListener {
            try {
                val aadharId = regEdtAadharNumber.text.toString()
                val farmerType = spFarmerType.selectedItem.toString().toLowerCase().split("(")[0]
                val mobileNumber = regEdtMobileNUmber.text.toString()
                val applicantName = regEdtApplicantName.text.toString()
                val email = regEdtEmail.text.toString() //
                val garden = regEdtGarden.text.toString()
                val gender = spGender.selectedItem.toString().toLowerCase()
                val socialStatus = spSocialStatus.selectedItem.toString()
                val dob = regEdtDateOfBirth.text.toString()
                val age = regEdtAge.text.toString()
                val education = spEducationalQualification.selectedItem.toString()
                val incomePerAnnum = regEdtIncomePerAnnum.text.toString() //
                val panCard = regEdtPANCard.text.toString()//
                val rationCardNumber = regEdtRationCardNumber.text.toString()

                val state = spState.selectedItem.toString()
                val district = spDistrict.selectedItem.toString()
                val block = spBlock.selectedItem.toString()
                val village = spVillage.selectedItem.toString()
                val halmetVillage = regEdtHalmentVillage.text.toString()
                val houstNo = regEdtHouseNo.text.toString()
                val street = regEdtStreet.text.toString()
                val pinCode = regEdtPinCode.text.toString()
                val phoneNumberResidence = regEdtPhoneResidence.text.toString() //
                state_assign = if (state == "Tamil Nadu") {
                    1
                } else {
                    -1
                }

                Log.d("state_assign", "" + state_assign)
                try {
                    for ((index, items) in villageDetails.withIndex()) {
                        if (village == items?.village_name) {
                            villageId = items.id!!
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    showSnackView(ex.message!!, registerSnackView)
                }


                val map = HashMap<String, Any>()
                map[ParamKey.FARMER_TYPE] = farmerType
                map[ParamKey.MOBILE_NUMBER] = mobileNumber
                map[ParamKey.NAME] = applicantName
                map[ParamKey.G_NAME] = garden
                map[ParamKey.GENDER] = gender
                map[ParamKey.EMAIL] = email
                map[ParamKey.IN_CONME] =
                    if (incomePerAnnum.isNotEmpty()) incomePerAnnum.toDouble() else 0
                map[ParamKey.SOCIAL_STATUS] = socialStatus
                map[ParamKey.AGE] = age
                map[ParamKey.QUALIFICATION] = education
                map[ParamKey.STATE] = state_assign
                map[ParamKey.DISTRICT] = districtId
                map[ParamKey.BLOCK] = blockId
                map[ParamKey.VILLAGE] = villageId
                map[ParamKey.HALMET_VILLAGE] = halmetVillage
                map[ParamKey.HOUSE_NUMBER] = houstNo
                map[ParamKey.STREET] = street
                map[ParamKey.PIN_CODE] = if (pinCode.isNotEmpty()) pinCode.toInt() else ""
                map[ParamKey.AADHAR_ID] = aadharId
                map[ParamKey.RATION_CARD] = rationCardNumber
                map[ParamKey.DATE_OF_BIRTH] = dob

                map[ParamKey.PHONE_NUMBER] = phoneNumberResidence
                map[ParamKey.PAN_NUMBER] = panCard


                /* map["a_copy"] = aadhaar_copy
                 map["r_copy"] = ration_copy
                 map["p_copy"] = profile_pic*/

                if (validation(map)) {

                    val mapWithPart = ArrayList<MultipartBody.Part>()
                    if (from == "register") {
                        if (profile_pic.isEmpty()) {
                            showSnackView(
                                getString(R.string.select_profile_photo),
                                registerSnackView
                            )
                            return@setOnClickListener
                        }

                        if (aadhaar_copy.isEmpty()) {
                            showSnackView(
                                getString(R.string.select_your_aadhar_copy),
                                registerSnackView
                            )
                            return@setOnClickListener
                        }
                        if (ration_copy.isEmpty()) {
                            showSnackView(
                                getString(R.string.seklect_your_ration_card_copy),
                                registerSnackView
                            )
                            return@setOnClickListener
                        }

                        if (!isValidInputFormat(email, EMAIL_PATTERN)) {
                            showSnackView(
                                getString(R.string.enter_valid_email),
                                registerSnackView
                            )
                            return@setOnClickListener
                        }
                        if (panCard.isNotEmpty()) {
                            if (!isValidInputFormat(panCard, FORMAT_PAN_CARD)) {
                                showSnackView(
                                    getString(R.string.enter_valid_pan_number),
                                    registerSnackView
                                )
                                return@setOnClickListener
                            }
                        }
                    }
                    // Get MultiPart From File path for Aadhar Copy

                    if (Utils.checkFile(aadhaar_copy)) {
                        mapWithPart.add(
                            Utils.multiPartFromFile(
                                aadhaar_copy, AADHAR_COPY
                            )
                        )
                    }
                    // Get MultiPart From File path for Ration Copy

                    if (Utils.checkFile(ration_copy)) {
                        mapWithPart.add(
                            Utils.multiPartFromFile(ration_copy, RATION_COPY)
                        )
                    }
                    // Get MultiPart From File path for Profile Copy

                    if (Utils.checkFile(profile_pic)) {
                        mapWithPart.add(
                            Utils.multiPartFromFile(profile_pic, PROFILE_PIC)
                        )
                    }

                    if (btnSuccess.text.toString() == getString(R.string.update)) {
                        Connector.callBasicWithPart(this, map, mapWithPart, this, FARMER_UPDATE)
                    } else {
                        Connector.callBasicWithPart(this, map, mapWithPart, this, REGISTER)
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                showSnackView(ex.message!!, registerSnackView)
            }
        }


        btnCancel.setOnClickListener {
            onBackPressed()
        }

        regEdtDateOfBirth.setOnClickListener {
            /*val dFragment = DatePickerFragment()
            // Show the date picker dialog fragment
            dFragment.show(this.supportFragmentManager, "Date Of Birth")*/


            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR) - 18
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

                    val dateOfYourBirth = GregorianCalendar(year, (monthOfYear + 1), dayOfMonth)
                    val today = Calendar.getInstance()
                    var yourAge = today.get(Calendar.YEAR) - dateOfYourBirth.get(Calendar.YEAR)
                    dateOfYourBirth.add(Calendar.YEAR, yourAge)
                    if (today.before(dateOfYourBirth)) {
                        yourAge--
                    }

                    val age = getAgeFromDOB(
                        dayOfMonth,
                        (monthOfYear + 1),
                        year
                    )

                    if (age >= 18) {
                        regEdtDateOfBirth.setText(
                            Utils.convertDateFormat(
                                dayOfMonth,
                                (monthOfYear + 1),
                                year
                            )
                        )
                        regEdtAge.setText(age.toString())
                    } else {
                        regEdtAge.setText("")
                        regEdtDateOfBirth.setText("")
                        showSnackView(getString(R.string.age_incorrect), registerSnackView)
                    }
                }, mYear, mMonth, mDay
            )

            val min = Calendar.getInstance()
            min.set(Calendar.YEAR, mYear)
            datePickerDialog.datePicker.maxDate = min.timeInMillis
            datePickerDialog.show()
        }

        regEdtState.setOnClickListener { spState.performClick() }
        regEdtFarmerType.setOnClickListener { spFarmerType.performClick() }
        regEdtGender.setOnClickListener { spGender.performClick() }
        regEdtSocialStatus.setOnClickListener { spSocialStatus.performClick() }
        regEdtEducationalQualification.setOnClickListener { spEducationalQualification.performClick() }

        regEdtState.setOnClickListener { spState.performClick() }
        regEdtDistrict.setOnClickListener { spDistrict.performClick() }
        regEdtBlock.setOnClickListener { spBlock.performClick() }
        regEdtVillage.setOnClickListener { spVillage.performClick() }

        /**
         * Check Aadhar Number Exist or New
         */
        regEdtAadharNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    val input = s.toString()
                    if (input.isNotEmpty()) {
                        if (input.length == 12) {
                            /*val view = this@RegisterActivity.currentFocus
                            hideSoftKeyboard(view, this@RegisterActivity)*/
                            //if (AadharCard.validateVerhoeff(input)) {
                            regEdtAadharNumber.error = null
                            val map = HashMap<String, String>()
                            map[ParamKey.AADHAR_ID] = input

                            Connector.callBasic(
                                this@RegisterActivity,
                                map,
                                readWriteAPI!!,
                                AADHAR_CHECK
                            )
                            /* } else {
                                 showSnackView(
                                     getString(R.string.aadhar_validation),
                                     registerSnackView
                                 )
                             }*/
                        } else {
                            txtHintRegiuster.text = ""
                            txtHintRegiuster.visibility = View.GONE
                            regEdtAadharNumber.error = getString(R.string.aadhar_validation)
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    showSnackView(ex.message!!, registerSnackView)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })


        spState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val state = spState.selectedItem.toString()
                    if (state != null && state.isNotEmpty() && state != getString(R.string.select_state)) {
                        val map = HashMap<String, String>()
                        map[ParamKey.STATE] = state

                        Connector.callBasic(
                            this@RegisterActivity,
                            map,
                            readWriteAPI!!,
                            DISTRICT_LIST
                        )
                    } else {
                        setDistrictDefault()

                        setBlockDefault()

                        setVillageDefault()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }

        spDistrict.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val districtName = spDistrict.selectedItem.toString()
                    if (districtName != null && districtName.isNotEmpty() && districtName != getString(
                            R.string.select_district
                        )
                    ) {
                        val map = HashMap<String, String>()
                        for ((index, item) in districtDetails.withIndex()) {
                            if (item!!.district_name == districtName) {
                                districtId = item.id!!
                                break
                            }
                        }
                        if (districtId != -1) {
                            map[ParamKey.DISTRICT_ID] = districtId.toString()
                            Connector.callBasic(
                                this@RegisterActivity,
                                map,
                                readWriteAPI!!,
                                BLOCK_LIST
                            )
                        } else {
                            showSnackView(
                                getString(R.string.please_select_district),
                                registerSnackView
                            )
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }


        spBlock.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    val blockName = spBlock.selectedItem.toString()
                    if (blockName != null && blockName.isNotEmpty() && blockName != getString(
                            R.string.select_block
                        )
                    ) {
                        val map = HashMap<String, String>()
                        for ((index, item) in blockDetails.withIndex()) {
                            if (item!!.block_name == blockName) {
                                blockId = item.id!!
                                break
                            }
                        }
                        if (blockId != -1) {
                            map[ParamKey.BLOCK_ID] = blockId.toString()
                            Connector.callBasic(
                                this@RegisterActivity,
                                map,
                                readWriteAPI!!,
                                VILLAGE_LIST
                            )
                        } else {
                            showSnackView(
                                getString(R.string.please_select_block),
                                registerSnackView
                            )
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
        imgProfileImage.setOnClickListener {
            showImage(
                this,
                profile_pic,
                imgProfileImage,
                "profile_image",
                "local",
                getString(R.string.profile_info)
            )
        }

        imgRationCardCopy.setOnClickListener {
            showImage(
                this,
                ration_copy,
                imgRationCardCopy,
                "profile_image",
                "local",
                getString(R.string.ration_card_copy)
            )
        }
        imgAadharCard.setOnClickListener {
            showImage(
                this,
                aadhaar_copy,
                imgAadharCard,
                "profile_image",
                "local",
                getString(R.string.aadhaar_cardC_copy)
            )
        }

    }

    @SuppressLint("DefaultLocale")
    private fun updateProfile(farmerDetails: FarmerDetails?) {
        if (farmerDetails != null) {
            regEdtAadharNumber.setText(farmerDetails.aadhaar_id)

            regEdtMobileNUmber.setText(farmerDetails.mobile_number)
            regEdtApplicantName.setText(farmerDetails.name)
            regEdtEmail.setText(farmerDetails.email) //
            regEdtGarden.setText(farmerDetails.gname)

            regEdtDateOfBirth.setText(farmerDetails.dob)
            regEdtAge.setText(farmerDetails.age)
            spEducationalQualification.selectedItem.toString()
            regEdtIncomePerAnnum.setText(farmerDetails.income) //
            regEdtPANCard.setText(farmerDetails.pan_no)//
            regEdtRationCardNumber.setText(farmerDetails.ration_card)
            spState.setSelection(1)

            try {
                if (farmerDetails.gender.toString().toLowerCase() == "female") {
                    spGender.setSelection(2)
                } else if (farmerDetails.gender.toString().toLowerCase() == "male") {
                    spGender.setSelection(1)
                }
                when {
                    farmerDetails.farmer_type.toString().toLowerCase().contains("marginal farmer") -> spFarmerType.setSelection(
                        1
                    )
                    farmerDetails.farmer_type.toString().toLowerCase().contains("small farmer") -> spFarmerType.setSelection(
                        2
                    )
                    farmerDetails.farmer_type.toString().toLowerCase().contains("other farmer") -> spFarmerType.setSelection(
                        3
                    )
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            val socialSatus = resources.getStringArray(R.array.social_status)
            try {
                when (farmerDetails.social_status) {
                    socialSatus[0] -> {
                        spSocialStatus.setSelection(0)
                    }
                    socialSatus[1] -> {
                        spSocialStatus.setSelection(1)
                    }
                    socialSatus[2] -> {
                        spSocialStatus.setSelection(2)
                    }
                    socialSatus[3] -> {
                        spSocialStatus.setSelection(3)
                    }
                    socialSatus[4] -> {
                        spSocialStatus.setSelection(4)
                    }
                    socialSatus[5] -> {
                        spSocialStatus.setSelection(5)
                    }
                    socialSatus[6] -> {
                        spSocialStatus.setSelection(6)
                    }
                    socialSatus[7] -> {
                        spSocialStatus.setSelection(7)
                    }
                    socialSatus[8] -> {
                        spSocialStatus.setSelection(8)
                    }
                    socialSatus[9] -> {
                        spSocialStatus.setSelection(9)
                    }
                    socialSatus[10] -> {
                        spSocialStatus.setSelection(10)
                    }

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            val educationStatus = resources.getStringArray(R.array.education)
            try {
                when (farmerDetails.qualification) {
                    educationStatus[0] -> {
                        spEducationalQualification.setSelection(0)
                    }
                    educationStatus[1] -> {
                        spEducationalQualification.setSelection(1)
                    }
                    educationStatus[2] -> {
                        spEducationalQualification.setSelection(2)
                    }
                    educationStatus[3] -> {
                        spEducationalQualification.setSelection(3)
                    }
                    educationStatus[4] -> {
                        spEducationalQualification.setSelection(4)
                    }
                    educationStatus[5] -> {
                        spEducationalQualification.setSelection(5)
                    }
                    educationStatus[6] -> {
                        spEducationalQualification.setSelection(6)
                    }
                    educationStatus[7] -> {
                        spEducationalQualification.setSelection(7)
                    }
                    educationStatus[8] -> {
                        spEducationalQualification.setSelection(8)
                    }
                    educationStatus[9] -> {
                        spEducationalQualification.setSelection(9)
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }


            showImages(farmerDetails)

            regEdtHalmentVillage.setText(farmerDetails.village)
            regEdtHouseNo.setText(farmerDetails.house_no)
            regEdtPinCode.setText(farmerDetails.pincode)
            regEdtStreet.setText(farmerDetails.street)
            regEdtPhoneResidence.setText(farmerDetails.phone_number)



            try {
                Utils.editTextFocusable(regEdtApplicantName)
                Utils.editTextFocusable(regEdtEmail)
                Utils.editTextFocusable(regEdtGarden)
                Utils.editTextFocusable(regEdtIncomePerAnnum)
                Utils.editTextFocusable(regEdtRationCardNumber)
                Utils.editTextFocusable(regEdtPANCard)
                Utils.editTextFocusable(regEdtHalmentVillage)

                Utils.editTextFocusable(regEdtHouseNo)
                Utils.editTextFocusable(regEdtStreet)
                Utils.editTextFocusable(regEdtPinCode)
                Utils.editTextFocusable(regEdtPhoneResidence)

                txtHintRegiuster.text = ""
                Utils.checkMobileNumber(regEdtMobileNUmber)
                Utils.checkMobileNumber(regEdtPhoneResidence)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    private fun showImages(farmerDetails: FarmerDetails) {
        val aadharCopy = farmerDetails.aadhaar_copy!!
        val rationCopy = farmerDetails.ration_copy!!
        val profilePic = farmerDetails.profile_pic!!

        if (aadharCopy.isNotEmpty()) {
            layOfAadharCardLayLive.visibility = View.VISIBLE
            ImageUtils.setImageLiveRealSize(
                imgAadharCardLive,
                Utils.IMAGE_URL_PATH + aadharCopy,
                this
            )
        } else {
            layOfAadharCardLayLive.visibility = View.GONE
        }

        if (aadharCopy.isNotEmpty()) {
            layOfRationCardCopyLayLive.visibility = View.VISIBLE
            ImageUtils.setImageLiveRealSize(
                imgRationCardCopyLive,
                Utils.IMAGE_URL_PATH + rationCopy,
                this
            )
        } else {
            layOfRationCardCopyLayLive.visibility = View.GONE
        }

        if (aadharCopy.isNotEmpty()) {
            layOfPofileLayLive.visibility = View.VISIBLE
            ImageUtils.setImageLiveRealSize(
                imgProfileImageLive,
                Utils.IMAGE_URL_PATH_PROFILE + profilePic,
                this
            )
        } else {
            layOfPofileLayLive.visibility = View.GONE
        }

        imgProfileImageLive.setOnClickListener {
            showImage(
                this,
                Utils.IMAGE_URL_PATH_PROFILE + profilePic,
                imgProfileImageLive,
                "profile_image",
                "live",
                getString(R.string.profile_info)
            )
        }

        imgRationCardCopyLive.setOnClickListener {
            showImage(
                this,
                Utils.IMAGE_URL_PATH + rationCopy,
                imgRationCardCopyLive,
                "profile_image",
                "live",
                getString(R.string.ration_card_copy)
            )
        }
        imgAadharCardLive.setOnClickListener {
            showImage(
                this,
                Utils.IMAGE_URL_PATH + aadharCopy,
                imgAadharCardLive,
                "profile_image",
                "live",
                getString(R.string.aadhaar_cardC_copy)
            )
        }

    }

    private fun validation(map: HashMap<String, Any>): Boolean {
        MessageUtils.dismissSnackBar(snackView)
        logMessage("mapsd", "" + map)
        if (map[ParamKey.AADHAR_ID].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_aadhar_number), registerSnackView)
            return false
        } else if (map[ParamKey.AADHAR_ID].toString().length < 12) {
            showSnackView(getString(R.string.enter_12_didgit_aadhar), registerSnackView)
            return false
        } /*else if (!AadharCard.validateVerhoeff(map[ParamKey.AADHAR_ID].toString())) {
            showSnackView("Enter Valid Aadhar Number",registerSnackView)
            return false
        }*/ else if (map[ParamKey.FARMER_TYPE].toString().isEmpty() || map[ParamKey.FARMER_TYPE].toString().toLowerCase() == getString(
                R.string.select_farmer_type
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.select_farmer_type), registerSnackView)
            return false
        } else if (map[ParamKey.MOBILE_NUMBER].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_mobile_number), registerSnackView)
            return false
        } else if (map[ParamKey.MOBILE_NUMBER].toString().length < 10) {
            showSnackView(getString(R.string.enter_10_digit_mobile), registerSnackView)
            return false
        } else if (map[ParamKey.NAME].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_applicant_name), registerSnackView)
            return false
        } else if (map[ParamKey.G_NAME].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_garden_name), registerSnackView)
            return false
        } else if (map[ParamKey.GENDER].toString().isEmpty() || map[ParamKey.GENDER].toString().toLowerCase() == getString(
                R.string.select_gender
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.selecrt_your_gender), registerSnackView)
            return false
        } else if (map[ParamKey.SOCIAL_STATUS].toString().isEmpty() || map[ParamKey.SOCIAL_STATUS].toString().toLowerCase() == getString(
                R.string.select_caste
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.select_your_social_status), registerSnackView)
            return false
        } else if (map[ParamKey.DATE_OF_BIRTH].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_date_of_birth), registerSnackView)
            return false
        } /*else if (map[ParamKey.AGE].toString().isEmpty()) {
            showSnackView("Enter Your Age")
            return false
        } */ else if (map[ParamKey.QUALIFICATION].toString().isEmpty() || map[ParamKey.QUALIFICATION].toString().toLowerCase() == getString(
                R.string.select_qualification
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.enter_your_edu_qualification), registerSnackView)
            return false
        } else if (map[ParamKey.RATION_CARD].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_ration_card_number), registerSnackView)
            return false
        } /*else if (map["p_copy"].toString().isEmpty()) {
            showSnackView(getString(R.string.select_profile_photo), registerSnackView)
            return false
        }*/ else if (map[ParamKey.STATE].toString() == "-1" || map[ParamKey.STATE].toString().toLowerCase() == getString(
                R.string.select_state
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.select_your_state), registerSnackView)
            return false
        } else if (map[ParamKey.DISTRICT].toString() == "-1" || map[ParamKey.DISTRICT].toString().toLowerCase() == getString(
                R.string.select_district
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.select_your_district), registerSnackView)
            return false
        } else if (map[ParamKey.BLOCK].toString() == "-1" || map[ParamKey.BLOCK].toString().toLowerCase() == getString(
                R.string.select_block
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.select_your_block), registerSnackView)
            return false
        } else if (map[ParamKey.VILLAGE].toString() == "-1" || map[ParamKey.VILLAGE].toString().toLowerCase() == getString(
                R.string.select_village
            ).toLowerCase()
        ) {
            showSnackView(getString(R.string.select_your_village), registerSnackView)
            return false
        } else if (map[ParamKey.HALMET_VILLAGE].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_halment_village), registerSnackView)
            return false
        } else if (map[ParamKey.HOUSE_NUMBER].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_house_number), registerSnackView)
            return false
        } else if (map[ParamKey.STREET].toString().isEmpty()) {
            showSnackView(
                getString(R.string.enter_your_street_location_name),
                registerSnackView
            )
            return false
        } else if (map[ParamKey.PIN_CODE].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_pincode), registerSnackView)
            return false
        } else if (map[ParamKey.PIN_CODE].toString().length < 6) {
            showSnackView(getString(R.string.enter_6_digit_pin), registerSnackView)
            return false
        } else if (map[ParamKey.RATION_CARD].toString().isEmpty()) {
            showSnackView(getString(R.string.select_your_ration_card), registerSnackView)
            return false
        } /*else if (map["a_copy"].toString().isEmpty()) {
            showSnackView(getString(R.string.select_your_aadhar_copy), registerSnackView)
            return false
        } else if (map["r_copy"].toString().isEmpty()) {
            showSnackView(getString(R.string.seklect_your_ration_card_copy), registerSnackView)
            return false
        }*/

        return true
    }

    private fun setVillageDefault() {
        val village = resources.getStringArray(R.array.village_list)
        spVillage.adapter = ArrayAdapter<String>(
            this@RegisterActivity,
            R.layout.single_view,
            R.id.singe_item,
            village
        )
    }

    private fun setBlockDefault() {
        val block = resources.getStringArray(R.array.block_list)
        spBlock.adapter = ArrayAdapter<String>(
            this@RegisterActivity,
            R.layout.single_view,
            R.id.singe_item,
            block
        )
    }

    private fun setDistrictDefault() {
        val district = resources.getStringArray(R.array.district_list)
        spDistrict.adapter = ArrayAdapter<String>(
            this@RegisterActivity,
            R.layout.single_view,
            R.id.singe_item,
            district
        )
    }


    private fun setVillageSpinner(responseBody: Response<ResponseBody>) {
        val modelVillage = Gson().fromJson(
            responseBody.body()?.string(),
            ModelVillageDTO::class.java
        )

        if (modelVillage != null) {
            if (modelVillage.status!!) {
                villageDetails = modelVillage.data as ArrayList<VillageDTO?>
                val count =
                    if (villageDetails != null) villageDetails.size else 0
                if (count != 0) {
                    villageInfo.clear()
                    villageInfo.add(getString(R.string.select_village))
                    var index = -1
                    for ((indext, values) in villageDetails.withIndex()) {
                        villageInfo.add(values?.village_name!!)

                        try {
                            if (values.id!!.toString() == farmerDetails!!.village.toString()) {
                                index = indext
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                    spVillage.adapter = ArrayAdapter(
                        this@RegisterActivity,
                        R.layout.single_view,
                        R.id.singe_item, villageInfo
                    )
                    if (index != -1) {
                        spVillage.setSelection(index + 1)
                    }
                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        registerSnackView
                    )
                }
            } else {
                showSnackView(modelVillage.message!!, registerSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), registerSnackView)
        }
    }

    private fun setBlockSpinner(responseBody: Response<ResponseBody>) {
        val modelBlock = Gson().fromJson(
            responseBody.body()?.string(),
            ModelBlockDTO::class.java
        )

        if (modelBlock != null) {
            if (modelBlock.status!!) {
                blockDetails = modelBlock.data as ArrayList<BlockDTO?>
                val count =
                    if (blockDetails != null) blockDetails.size else 0
                if (count != 0) {
                    blockInfo.clear()
                    blockInfo.add(getString(R.string.select_block))
                    var index = -1
                    for ((indext, values) in blockDetails.withIndex()) {
                        blockInfo.add(values?.block_name!!)
                        try {
                            if (values.id!!.toString() == farmerDetails!!.block.toString()) {
                                index = indext
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }

                    spBlock.adapter = ArrayAdapter(
                        this@RegisterActivity,
                        R.layout.single_view,
                        R.id.singe_item, blockInfo
                    )

                    if (index != -1) {
                        spBlock.setSelection(index + 1)
                    }
                    setVillageDefault()
                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        registerSnackView
                    )
                }
            } else {
                showSnackView(modelBlock.message!!, registerSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), registerSnackView)
        }
    }

    private fun setDistrictSpinner(responseBody: Response<ResponseBody>) {
        val modelDistrict: ModelDistrictDTO = Gson().fromJson(
            responseBody.body()?.string(),
            ModelDistrictDTO::class.java
        )

        if (modelDistrict != null) {

            if (modelDistrict.status!!) {
                districtDetails = modelDistrict.data as ArrayList<DistrictDTO?>
                val count =
                    if (districtDetails != null) districtDetails.size else 0
                if (count != 0) {
                    districtInfo.clear()
                    districtInfo.add(getString(R.string.select_district))
                    var index = -1
                    for ((indext, values) in districtDetails.withIndex()) {
                        districtInfo.add(values?.district_name!!)
                        try {
                            if (farmerDetails!!.district.toString() == values.id.toString()) {
                                index = indext
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }

                    spDistrict.adapter = ArrayAdapter(
                        this@RegisterActivity,
                        R.layout.single_view,
                        R.id.singe_item, districtInfo
                    )

                    if (index != -1) {
                        spDistrict.setSelection(index + 1)
                    }

                    setBlockDefault()
                    setVillageDefault()

                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        registerSnackView
                    )
                }
            } else {
                showSnackView(modelDistrict.message!!, registerSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), registerSnackView)
        }
    }

    override fun activityResultCallback(requestCode: Int, resultCode: Int, data: Intent?) {

        if (data != null) {
            val contentURI = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(
                contentResolver,
                contentURI
            )
            when (requestCode) {
                PROFILE_IMAGE -> {

                    layOfPofileLayLive.visibility = View.GONE
                    profile_pic = ImageUtils.saveImage(this, bitmap)
                    val size: Double = Utils.getFileSize(profile_pic)

                    if (size <= Utils.IMAGE_SIZE) {
                        layOfPofileLay.visibility = View.VISIBLE
                        imgProfileImage.setImageBitmap(bitmap)
                        txtProfilePictureName.text = File(profile_pic).name
                        sizeProfile.visibility = View.GONE

                    } else {
                        layOfAadharCardLay.visibility = View.GONE
                        profile_pic = ""
                        imgProfileImage.setImageResource(R.drawable.ic_menu_camera)
                        txtProfilePictureName.text = ""
                        sizeProfile.visibility = View.VISIBLE

                    }
                }
                AADHAR_CARD -> {

                    layOfAadharCardLayLive.visibility = View.GONE
                    aadhaar_copy = ImageUtils.saveImage(this, bitmap)

                    val size: Double = Utils.getFileSize(aadhaar_copy)
                    if (size <= Utils.IMAGE_SIZE) {
                        layOfAadharCardLay.visibility = View.VISIBLE
                        imgAadharCard.setImageBitmap(bitmap)
                        txtAadharCardName.text = File(aadhaar_copy).name
                        sizeAadharCard.visibility = View.GONE
                    } else {
                        layOfAadharCardLay.visibility = View.GONE
                        aadhaar_copy = ""
                        imgAadharCard.setImageResource(R.drawable.ic_menu_camera)
                        txtAadharCardName.text = ""
                        sizeAadharCard.visibility = View.VISIBLE
                    }
                }
                RATION_CARD -> {

                    layOfRationCardCopyLayLive.visibility = View.GONE
                    ration_copy = ImageUtils.saveImage(this, bitmap)

                    val size: Double = Utils.getFileSize(ration_copy)
                    if (size <= Utils.IMAGE_SIZE) {
                        layOfRationCardCopyLay.visibility = View.VISIBLE
                        imgRationCardCopy.setImageBitmap(bitmap)
                        txtRationCardCopyName.text = File(ration_copy).name
                        sizeRationCard.visibility = View.GONE
                    } else {
                        layOfRationCardCopyLay.visibility = View.GONE
                        ration_copy = ""
                        imgRationCardCopy.setImageResource(R.drawable.ic_menu_camera)
                        txtRationCardCopyName.text = ""
                        sizeRationCard.visibility = View.VISIBLE
                    }
                }
            }
        } else {
            when (requestCode) {
                PROFILE_IMAGE -> {
                    layOfPofileLay.visibility = View.GONE
                    layOfPofileLayLive.visibility = View.GONE
                    profile_pic = ""
                }
                AADHAR_CARD -> {
                    layOfAadharCardLay.visibility = View.GONE
                    layOfAadharCardLayLive.visibility = View.GONE
                    aadhaar_copy = ""
                }
                RATION_CARD -> {
                    layOfRationCardCopyLay.visibility = View.GONE
                    layOfRationCardCopyLayLive.visibility = View.GONE
                    ration_copy = ""
                }
            }
        }
    }

    override fun onError(message: String, api: String) {
        showSnackView(message, registerSnackView)
    }

    override fun onActionAfterPermissionGrand(requestCode: Int) {
        choosePhotoFromGallery(this, requestCode)
    }

    /**
     * All Failure ModelApplicationDetails Comes here to this Page
     */
    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, registerSnackView)
    }


    /**
     * All Success ModelApplicationDetails Comes Here  to this Page
     */
    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        when (api) {
            DISTRICT_LIST -> {
                setDistrictSpinner(responseBody)
            }
            BLOCK_LIST -> {
                setBlockSpinner(responseBody)
            }
            VILLAGE_LIST -> {
                setVillageSpinner(responseBody)
            }
            REGISTER -> {
                registerSuccess(responseBody)
            }
            AADHAR_CHECK -> {
                val modelValidateRegister =
                    Gson().fromJson(responseBody.body()?.string(), AadharValidate::class.java)
                if (modelValidateRegister.status!!) {
                    val view = this@RegisterActivity.currentFocus
                    hideSoftKeyboard(view, this@RegisterActivity)
                    txtHintRegiuster.text = ""
                    txtHintRegiuster.visibility = View.GONE
                } else {
                    txtHintRegiuster.visibility = View.VISIBLE
                    txtHintRegiuster.text = modelValidateRegister.message!!
                    regEdtAadharNumber.setText("")
                    regEdtAadharNumber.error = modelValidateRegister.message!!
                }
            }
            FARMER_UPDATE -> {
                try {
                    val modelSuccess =
                        Gson().fromJson(
                            responseBody.body()!!.string(),
                            ModelProfileUpdate::class.java
                        )
                    if (modelSuccess != null) {
                        if (modelSuccess.status!!) {
                            sessionManager?.createLoginSession(
                                modelSuccess.farmerdetails!!.name,
                                modelSuccess.farmerdetails!!.email,
                                modelSuccess.farmerdetails!!.mobile_number,
                                modelSuccess.farmerdetails!!.id.toString(),
                                SessionManager.getToken(this).toString(),
                                modelSuccess.farmerdetails.profile_pic,
                                modelSuccess.farmerdetails
                            )

                            MessageUtils.showToastMessage(this, modelSuccess.message!!, false)
                            val wallpaperDirectory =
                                File(Environment.getExternalStorageDirectory().toString() + ImageUtils.IMAGE_DIRECTORY)
                            wallpaperDirectory.delete()
                            finish()

                        } else {
                            showSnackView(modelSuccess.message!!, registerSnackView)
                        }
                    } else {
                        showSnackView(getString(R.string.something_went_wrong), registerSnackView)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun registerSuccess(responseBody: Response<ResponseBody>) {
        val modelRegister =
            Gson().fromJson(responseBody.body()?.string(), ModelRegisterSuccess::class.java)
        if (modelRegister != null) {
            if (modelRegister.status!!) {

                /*sessionManager?.createLoginSession(
                    modelRegister.data!!.name,
                    modelRegister.data!!.email,
                    modelRegister.data!!.mobile_number,
                    modelRegister.data!!.id.toString(),
                    "",
                    modelRegister.data!!.profile_pic
                )
                startActivity(Intent(this, MainActivity::class.java))*/

                MessageUtils.showToastMessage(this, modelRegister.message, false)
                val wallpaperDirectory =
                    File(Environment.getExternalStorageDirectory().toString() + ImageUtils.IMAGE_DIRECTORY)
                wallpaperDirectory.delete()
                finish()

            } else {
                showSnackView(modelRegister.message!!, registerSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), registerSnackView)
        }
    }
}