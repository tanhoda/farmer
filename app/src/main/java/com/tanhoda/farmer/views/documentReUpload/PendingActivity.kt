package com.tanhoda.farmer.views.documentReUpload

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.applicationViewDetails.ModelPendingDocument
import com.tanhoda.farmer.model.register.registerSuccess.ModelRegisterSuccess
import com.tanhoda.farmer.utils.ImageUtils
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.SessionManager
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.utils.permissions.CallbackPermission
import com.tanhoda.farmer.utils.permissions.PermissionDescription
import com.tanhoda.farmer.utils.permissions.RequestPermission
import kotlinx.android.synthetic.main.activity_pending.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File
import android.view.ViewGroup as ViewGroup1

class PendingActivity : BaseActivity(), ReadWriteAPI {

    var listOfPart = ArrayList<MultipartBody.Part>()
    var readWriteAPI: ReadWriteAPI? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending)

        readWriteAPI = this

        title = getString(R.string.pending_document)
        val pendingDocument =
            intent.getSerializableExtra(ParamKey.PENDING_DOCUMENT) as ArrayList<ModelPendingDocument>?

        btnSuccess.text = getString(R.string.upload)

        if (pendingDocument!!.size != 0) {
            txtNoDocuments.visibility = View.GONE
            rcyRejectedDocument.visibility = View.VISIBLE

            rcyRejectedDocument.layoutManager = LinearLayoutManager(this)
            rcyRejectedDocument.adapter =
                ItemPendingDocuments(this, pendingDocument, documentReupload)

        } else {
            txtNoDocuments.visibility = View.VISIBLE
            rcyRejectedDocument.visibility = View.GONE
        }


        btnSuccess.setOnClickListener {
            val lisOfId = ArrayList<HashMap<String, String>>()
            try {
                pendingDocument.forEachIndexed { _, item ->
                    if (item.imageName.toString().contains("/storage")) {
                        val idMap = HashMap<String, String>()
                        idMap[ParamKey.ID] = item.id!!.toString()
                        idMap[ParamKey.PENDING_DOCUMENTS] = File(item.imageName.toString()).name
                        lisOfId.add(idMap)

                        listOfPart.add(
                            Utils.multiPartFromFile(
                                item.imageName.toString(),
                                ParamKey.PENDING_DOCUMENT_ARRAY
                            )
                        )
                    }
                }

                if (listOfPart.size != 0) {
                    listOfPart = Utils.findDuplicatesFromPart(listOfPart)
                    Log.d("listOfPartsssss", "" + listOfPart.size)
                    val map = HashMap<String, Any>()
                    map[ParamKey.DOCUMENT] = lisOfId
                    map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(this)
                    Connector.callBasicWithPart(
                        this,
                        map,
                        listOfPart,
                        this,
                        ParamAPI.PENDING_DOCUMENTS_UPLOAD
                    )

                } else {
                    showSnackView("Please Select Pending Document", documentReupload)
                }
            } catch (eX: java.lang.Exception) {
                eX.printStackTrace()
            }
        }


        btnCancel.setOnClickListener {
            onBackPressed()
        }

    }


    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        when (api) {
            ParamAPI.PENDING_DOCUMENTS_UPLOAD -> {
                if (responseBody.isSuccessful) {
                    val modelSuccess = Gson().fromJson(
                        responseBody.body()!!.string(),
                        ModelRegisterSuccess::class.java
                    )

                    if (modelSuccess!!.status!!) {
                        MessageUtils.showToastMessage(this, modelSuccess.message!!, false)
                        finish()
                    } else {
                        showSnackView(
                            modelSuccess.message!!,
                            documentReupload
                        )
                    }
                } else {
                    showSnackView(
                        MessageUtils.getFailureCode(responseBody.code()),
                        documentReupload
                    )
                }
            }
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, documentReupload)

    }


    class ItemPendingDocuments(
        val activity: BaseActivity,
        val rejectedDocument: ArrayList<ModelPendingDocument>?,
        val documentReupload: View
    ) : RecyclerView.Adapter<ViewHolderDocument>(), CallbackPermission {

        var holderItem: ViewHolderDocument? = null
        var positionItem: Int = 0


        override fun onActionAfterPermissionGrand(requestCode: Int) {
            RequestPermission.choosePhotoFromGallery(activity, requestCode)
        }

        override fun activityResultCallback(requestCode: Int, resultCode: Int, data: Intent?) {
            try {
                val contentURI = data!!.data
                val bitmap = MediaStore.Images.Media.getBitmap(
                    activity.contentResolver,
                    contentURI
                )
                var documentName = ""
                Log.d("contentURIsd", "$positionItem asdsd  " + contentURI)

                if (contentURI != null) {
                    //imgBankPassBook.visibility = View.VISIBLE
                    //imgBankPassBookLive.visibility = View.GONE
                    documentName = ImageUtils.saveImage(activity, bitmap)
                    if (Utils.getFileSize(documentName) <= Utils.IMAGE_SIZE) {
                        holderItem!!.imgDocument.setImageBitmap(bitmap)
                    } else {
                        documentName = ""
                        holderItem!!.imgDocument.setImageResource(R.drawable.ic_menu_camera)
                    }

                    if (Utils.checkFile(documentName.toString())) {
                        rejectedDocument!![positionItem].imageName = documentName
                        holderItem!!.txtImgLocation.visibility = View.GONE
                    } else {
                        rejectedDocument!![positionItem].imageName = ""
                        holderItem!!.txtImgLocation.visibility = View.VISIBLE
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        override fun onError(message: String, api: String) {
            activity.showSnackView(message, documentReupload)
        }

        override fun onCreateViewHolder(parent: ViewGroup1, viewType: Int): ViewHolderDocument {
            return ViewHolderDocument(
                LayoutInflater.from(activity).inflate(
                    R.layout.item_document_re_upload,
                    parent,
                    false
                )
            )
        }

        override fun getItemCount(): Int {
            return rejectedDocument!!.size
        }

        override fun onBindViewHolder(holder: ViewHolderDocument, position: Int) {
            try {

                holder.itemViewId.visibility = View.VISIBLE
                holder.edtDocumentName.setText(rejectedDocument!![position].documentName!!.toString())
                holder.iplRemarksInPending.visibility = View.GONE

                if (rejectedDocument!![position].imageName!!.toLowerCase() == "jpg" || rejectedDocument!![position].imageName!!.toLowerCase() == "png" || rejectedDocument!![position].imageName!!.toLowerCase() == "jpeg")
                    ImageUtils.setImageLive(
                        holder.imgDocument,
                        Utils.IMAGE_URL_PATH + rejectedDocument!![position].imageName,
                        activity
                    )

                holder.edtDocumentName.setOnClickListener {

                    positionItem = position
                    holderItem = holder

                    initCallbackPermission(this)
                    try {
                        RequestPermission.checkPermissions(
                            RequestPermission.BANK_PASS_BOOK,
                            activity,
                            this,
                            PermissionDescription.permissionForCameraAndStorage(activity),
                            arrayOf(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA
                            )
                        )
                    } catch (ex: java.lang.Exception) {
                        ex.printStackTrace()
                    }
                }

                holder.imgDocument.setOnClickListener {

                    holderItem = holder
                    positionItem = position

                    initCallbackPermission(this)
                    try {
                        RequestPermission.checkPermissions(
                            RequestPermission.BANK_PASS_BOOK,
                            activity,
                            this,
                            PermissionDescription.permissionForCameraAndStorage(activity),
                            arrayOf(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA
                            )
                        )
                    } catch (ex: java.lang.Exception) {
                        ex.printStackTrace()
                    }

                }
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
        }

    }

    class ViewHolderDocument(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val edtDocumentName = itemView.findViewById<EditText>(R.id.edtDocumentName)
        val imgDocument = itemView.findViewById<ImageView>(R.id.imgDocument)
        val iplRemarksInPending = itemView.findViewById<TextInputLayout>(R.id.iplRemarksInPending)
        val edtRemarks = itemView.findViewById<EditText>(R.id.edtRemarks)
        val itemViewId = itemView.findViewById<View>(R.id.itemViewId)
        val txtImgLocation = itemView.findViewById<TextView>(R.id.txtSizeDocument)


    }
}
