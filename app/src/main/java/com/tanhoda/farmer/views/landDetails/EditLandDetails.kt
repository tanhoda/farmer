package com.tanhoda.farmer.views.landDetails


import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ParamKey.Companion.LAND_OWNER
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.landAndEditLand.DataItem
import com.tanhoda.farmer.model.modify.ModelEditSuccessfully
import com.tanhoda.farmer.model.register.blockDetails.BlockDTO
import com.tanhoda.farmer.model.register.blockDetails.ModelBlockDTO
import com.tanhoda.farmer.model.register.district.DistrictDTO
import com.tanhoda.farmer.model.register.district.ModelDistrictDTO
import com.tanhoda.farmer.model.register.village.ModelVillageDTO
import com.tanhoda.farmer.model.register.village.VillageDTO
import com.tanhoda.farmer.utils.*
import com.tanhoda.farmer.utils.permissions.CallbackPermission
import com.tanhoda.farmer.utils.permissions.PermissionDescription
import com.tanhoda.farmer.utils.permissions.RequestPermission
import kotlinx.android.synthetic.main.activity_land_details.*
import kotlinx.android.synthetic.main.add_more_lay_land_details.*
import kotlinx.android.synthetic.main.submit_cancel_horizontal_view.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File
import android.widget.TextView
import android.widget.EditText


class EditLandDetails : BaseActivity(), ReadWriteAPI, View.OnClickListener, CallbackPermission {

    var landDetails: DataItem? = null
    //var appDetailsToAddMore = ArrayList<HashMap<String, Any>>()
    var districtDetails = ArrayList<DistrictDTO?>()
    var districtInfo = ArrayList<String>()

    var blockDetails = ArrayList<BlockDTO?>()
    var blockInfo = ArrayList<String>()

    var villageDetails = ArrayList<VillageDTO?>()
    var villageInfo = ArrayList<String>()
    var districtId = -1
    var blockId = -1
    var villageId = -1
    var pattaChittaCopy = ""
    var FMBCopy = ""
    var leaseAgreementCopy = ""
    var addMoreItem: ArrayList<AddMoreItems>? = null
    var listOfPart = ArrayList<MultipartBody.Part>()


    companion object {
        var itemLandInfo: ItemLandInfo? = null
        var fmbCopyMore = ""
        var leaseAgreementCopyMore = ""
        var readWriteAPI: ReadWriteAPI? = null
        var callbackPermission: CallbackPermission? = null
        var itemUpdate: ItemUpdate? = null
        var holder: ItemLandInfo.HolderView? = null
        var position: Int = 0

        fun editImages(itemUpdate: ItemUpdate, holder: ItemLandInfo.HolderView, position: Int) {
            this.itemUpdate = itemUpdate
            this.holder = holder
            this.position = position
        }


        fun checkPermission(
            requestCode: Int,
            activity: BaseActivity,
            callbackPermission: CallbackPermission
        ) {
            try {
                RequestPermission.checkPermissions(
                    requestCode, activity, callbackPermission,
                    PermissionDescription.permissionForCameraAndStorage(activity),
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                    )
                )
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_land_details)
        Log.d("REGISTERED_LAND_COUNTsd", "" + Utils.REGISTERED_LAND_COUNT)
        initCallbackPermission(this)
        callbackPermission = this
        btnSuccess.text = getString(R.string.update)
        layOfAddMoreInit.visibility = View.GONE
        txtAddMoreEnable.visibility = View.VISIBLE
        readWriteAPI = this
        landDetails = intent.getSerializableExtra(ParamKey.LAND_INFO_TO_EDIT) as DataItem?

        rcyLandInfo.visibility = View.VISIBLE
        btnSuccess.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
        imgChittaInLand.setOnClickListener(this)
        imgFMBCopyInLand.setOnClickListener(this)
        imgLeaseAgreementInLand.setOnClickListener(this)

        addMoreItem = ArrayList()

        for ((index, itemLand) in landDetails!!.landdetailfarmer!!.withIndex()) {
            /*val addMoreMap = HashMap<String, Any>()
            addMoreMap[ParamKey.LAND_OWNER] = itemLand!!.landOwnership.toString()
            addMoreMap[ParamKey.SURVEY_NO] = itemLand.serveyNo.toString()
            addMoreMap[ParamKey.SUB_DIVISION] = itemLand.subdivision.toString()
            addMoreMap[ParamKey.LAND_AREA] = itemLand.land_area.toString()
            addMoreMap[ParamKey.LEASE_AGREEMENT_COPY] = itemLand.leaseAgreement.toString()
            addMoreMap[ParamKey.LAND_FMB_COPY] = itemLand.fmbCopy.toString()
            addMoreMap[ParamKey.SOURCE_OF_IRRIGATION] = itemLand.source_of_irrigation.toString()
            addMoreMap[ParamKey.ID] = itemLand.id!!.toString()
            addMoreMap[ParamKey.IS_VALIDATE] = true*/

            val addMoreItems = AddMoreItems(
                itemLand!!.landOwnership.toString(),
                itemLand.serveyNo.toString(),
                itemLand.subdivision.toString(),
                itemLand.landArea.toString(),
                itemLand.leaseAgreement.toString(),
                itemLand.fmbCopy.toString(),
                itemLand.sourceOfIrrigation.toString(),
                itemLand.id!!.toString(),
                true
            )
            addMoreItem!!.add(addMoreItems)
        }

        rcyLandInfo.layoutManager = LinearLayoutManager(this)
        itemLandInfo = ItemLandInfo(this, addMoreItem!!)
        rcyLandInfo.adapter = itemLandInfo

        spStateInLand.setSelection(1)
        txtTotalLandAreaInLand.setText("" + Utils.convertDouble(landDetails!!.totalArea.toString()))
        edtCittaNoInLand.setText(landDetails!!.chittaNo.toString())
        Utils.editTextFocusable(edtCittaNoInLand)

        pattaChittaCopy = landDetails!!.chittaCopy!!.toString()

        ImageUtils.setImageWithCatch(
            imgChittaInLand,
            Utils.IMAGE_URL_PATH_SCHEME + pattaChittaCopy, this
        )


        //setInitValues(landDetails)
        txtAddMoreEnable.setOnClickListener {

            /* Log.d("totalAreasdsTotal", "size " + addMoreItem!!.size)
             var totalArea = 0.0
             for (items in addMoreItem!!) {
                 val land_area = items.land_area
                 Log.d("totalAreasdsTotal", "land_area $land_area")
                 if (land_area.isNotEmpty()) {
                     totalArea += items.land_area.toDouble()
                 }
             }
             Log.d("totalAreasdsTotal", "tt $totalArea")
             if (Utils.validateFarmerTypeLandAllow(
                     totalArea,
                     SessionManager.getObject(this).farmer_type.toString(),
                     landSnackView,
                     this
                 )
             ) {*/

            val addMoreItems = AddMoreItems("", "", "", "0", "", "", "", "new", false)
            addMoreItem!!.add(addMoreItems)
            //itemLandInfo!!.notifyDataSetChanged()

            itemLandInfo = ItemLandInfo(this, addMoreItem!!)
            rcyLandInfo.adapter = itemLandInfo

            // }
            //txtTotalLandAreaInLand.setText("" + Utils.convertDouble("" + totalArea))
        }


        edtLandAreaInLand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    var totalAreaForText = 0.0
                    var input = s.toString()
                    if (input.isNotEmpty()) {
                        if (input == ".") {
                            input = "0$input"
                        }
                        val inputArea = input.toDouble()
                        val farmerDetails = SessionManager.getObject(this@EditLandDetails)
                        if (farmerDetails != null) {
                            if (landDetails!!.landdetailfarmer!!.size != 0) {
                                for ((index, items) in landDetails!!.landdetailfarmer!!.withIndex()) {
                                    if (items!!.landArea.toString().isNotEmpty()) {
                                        val landAreaInLand = items!!.landArea.toString().toDouble()
                                        totalAreaForText += landAreaInLand
                                    }
                                }
                            } else {
                                totalAreaForText = inputArea
                            }
                        }
                    } else {
                        if (landDetails!!.landdetailfarmer!!.size != 0) {
                            for ((index, items) in landDetails!!.landdetailfarmer!!.withIndex()) {
                                if (items!!.landArea.toString().isNotEmpty()) {
                                    totalAreaForText += items.landArea.toString().toDouble()
                                }
                            }
                        }
                    }

                    logMessage(
                        "totalAreaForText", "" + addMoreItem!!.size + "" + totalAreaForText
                    )

                    if (addMoreItem!!.size != 0) {
                        txtTotalLandAreaInLand.setText("" + Utils.convertDouble("" + totalAreaForText))
                    } else {
                        txtTotalLandAreaInLand.setText("")
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
        })


        spStateInLand.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val state = spStateInLand.selectedItem.toString()
                    if (state != null && state.isNotEmpty() && state != getString(R.string.select_state)) {
                        val map = HashMap<String, String>()
                        map[ParamKey.STATE] = state
                        Connector.callBasic(
                            this@EditLandDetails,
                            map,
                            readWriteAPI!!,
                            ParamAPI.DISTRICT_LIST
                        )
                    } else {
                        setDistrictDefault()
                        setBlockDefault()
                        setVillageDefault()
                    }
                }
            }
        spDistrictInLand.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val districtName = spDistrictInLand.selectedItem.toString()
                        if (districtName != null && districtName.isNotEmpty() && districtName != getString(
                                R.string.select_district
                            )
                        ) {
                            val map = HashMap<String, String>()
                            for ((index, item) in districtDetails.withIndex()) {
                                if (item!!.district_name == districtName) {
                                    districtId = item.id!!
                                    break
                                }
                            }
                            if (districtId != -1) {
                                map[ParamKey.DISTRICT_ID] = districtId.toString()
                                Connector.callBasic(
                                    this@EditLandDetails,
                                    map,
                                    readWriteAPI!!,
                                    ParamAPI.BLOCK_LIST
                                )
                            } else {
                                showSnackView(
                                    getString(R.string.please_select_district),
                                    landSnackView
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }
        spBlockInLand.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val blockName = spBlockInLand.selectedItem.toString()
                        if (blockName != null && blockName.isNotEmpty() && blockName != getString(
                                R.string.select_block
                            )
                        ) {
                            val map = HashMap<String, String>()
                            for ((index, item) in blockDetails.withIndex()) {
                                if (item!!.block_name == blockName) {
                                    blockId = item.id!!
                                    break
                                }
                            }
                            if (blockId != -1) {
                                map[ParamKey.BLOCK_ID] = blockId.toString()
                                Connector.callBasic(
                                    this@EditLandDetails,
                                    map,
                                    readWriteAPI!!,
                                    ParamAPI.VILLAGE_LIST
                                )
                            } else {
                                showSnackView(
                                    getString(R.string.please_select_block),
                                    landSnackView
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }

        spVillageInLand.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        val village = spVillageInLand.selectedItem.toString()
                        for ((index, items) in villageDetails.withIndex()) {
                            if (village == items?.village_name) {
                                villageId = items.id!!
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        showSnackView(ex.message!!, landSnackView)
                    }
                }
            }

        btnCancel.setOnClickListener {
            finish()
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSuccess -> {
                try {

                    val map = HashMap<String, Any>()
                    map!![ParamKey.FARMER_ID] = SessionManager.getFarmerId(this).toInt()
                    map!![ParamKey.LAND_ID] = landDetails!!.id!!.toInt()
                    map!![ParamKey.STATE] = 1
                    map!![ParamKey.DISTRICT] = districtId
                    map!![ParamKey.BLOCK] = blockId
                    map!![ParamKey.VILLAGE] = villageId
                    map!![ParamKey.CHITTA_NUMBER] = edtCittaNoInLand.text.toString()
                    map!![ParamKey.PATTA_CHITTA_COPY] = pattaChittaCopy
                    if (txtTotalLandAreaInLand.text.toString().isNotEmpty()) {
                        map!![ParamKey.AREA_PROPOSED] =
                            txtTotalLandAreaInLand.text.toString().toDouble()
                    } else {
                        map!![ParamKey.AREA_PROPOSED] = ""
                    }
                    if (txtTotalLandAreaInLand.text.toString().isNotEmpty()) {
                        map!![ParamKey.TOTAL_AREA] =
                            txtTotalLandAreaInLand.text.toString().toDouble()
                    } else {
                        map!![ParamKey.TOTAL_AREA] = ""
                    }

                    if (landAddValidation(map)) {
                        if (addMoreItem!!.size != 0) {
                            if (validateEditMoreItem(addMoreItem)) {

                                if (map[LAND_OWNER].toString() == "Own") {
                                    if (map[ParamKey.LAND_FMB_COPY].toString() == "") {
                                        showSnackView(
                                            getString(R.string.select_your_FMB_copy),
                                            landSnackView
                                        )
                                        return
                                    }
                                } else if (map[LAND_OWNER].toString() == "Lease") {
                                    if (map[ParamKey.LAND_FMB_COPY].toString() == "") {
                                        showSnackView(
                                            getString(R.string.select_your_FMB_copy),
                                            landSnackView
                                        )
                                        return
                                    } else if (map[ParamKey.LEASE_AGREEMENT_COPY].toString().isEmpty()) {
                                        showSnackView(
                                            getString(R.string.select_leaseagreement),
                                            landSnackView
                                        )
                                        return
                                    }
                                }

                                if (map[ParamKey.SOURCE_OF_IRRIGATION].toString() == "Select Irrigation") {
                                    showSnackView(
                                        getString(R.string.select_your_irrigation),
                                        landSnackView
                                    )
                                    return
                                }

                                if (Utils.checkFile(pattaChittaCopy)) {
                                    listOfPart.add(
                                        Utils.multiPartFromFile(
                                            pattaChittaCopy,
                                            ParamKey.PATTA_CHITTA_COPY
                                        )
                                    )
                                }
                                try {
                                    if (spLandOwnership.selectedItem.toString() == "Lease") {
                                        if (Utils.checkFile(FMBCopy)) {
                                            listOfPart.add(
                                                Utils.multiPartFromFile(
                                                    FMBCopy,
                                                    ParamKey.LAND_FMB_COPY + "[]"
                                                )
                                            )
                                        }
                                        if (Utils.checkFile(leaseAgreementCopy)) {
                                            listOfPart.add(
                                                Utils.multiPartFromFile(
                                                    leaseAgreementCopy,
                                                    ParamKey.LEASE_AGREEMENT_COPY + "[]"
                                                )
                                            )
                                        }
                                    } else {
                                        if (Utils.checkFile(FMBCopy)) {
                                            listOfPart.add(
                                                Utils.multiPartFromFile(
                                                    FMBCopy,
                                                    ParamKey.LAND_FMB_COPY + "[]"
                                                )
                                            )
                                        }
                                    }
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }

                                val FMBCopyArray = ArrayList<MultipartBody.Part>()
                                val LeaseCopyArray = ArrayList<MultipartBody.Part>()
                                for ((index, items) in addMoreItem!!.withIndex()) {
                                    val lease = items.lease_agreement_copy.toString()
                                    val fmb = items.FMB_copy.toString()
                                    if (lease.contains("/storage/")) {
                                        LeaseCopyArray.add(
                                            Utils.multiPartFromFile(
                                                lease,
                                                ParamKey.LEASE_AGREEMENT_COPY + "[]"
                                            )
                                        )
                                    }

                                    if (fmb.contains("/storage/")) {
                                        FMBCopyArray.add(
                                            Utils.multiPartFromFile(
                                                fmb,
                                                ParamKey.LAND_FMB_COPY + "[]"
                                            )
                                        )
                                    }
                                    items.FMB_copy = File(fmb).name
                                    items.lease_agreement_copy = File(lease).name
                                }



                                listOfPart = Utils.findDuplicatesFromPart(listOfPart)
                                map[ParamKey.LAND_DETAILS] = addMoreItem!!
                                Connector.callBasicWithMultiPart(
                                    this@EditLandDetails,
                                    map,
                                    listOfPart,
                                    FMBCopyArray,
                                    LeaseCopyArray,
                                    readWriteAPI!!,
                                    ParamAPI.LAND_EDIT
                                )
                            } else {
                                ///(rcyLandInfo.adapter as ItemLandInfo).notifyDataSetChanged()
                                showSnackView(
                                    getString(R.string.please_enter_missing_fileds),
                                    landSnackView
                                )
                            }
                        } else {
                            //(rcyLandInfo.adapter as ItemLandInfo).notifyDataSetChanged()
                            showSnackView(
                                getString(R.string.please_add_one_land_details),
                                landSnackView
                            )
                        }
                    }

                } catch (eX: Exception) {
                    eX.printStackTrace()
                }
            }

            R.id.btnCancel -> {
                onBackPressed()
            }

            R.id.imgChittaInLand -> {
                try {
                    checkPermission(RequestPermission.PATTA_CHITTA)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

            R.id.imgFMBCopyInLand -> {
                try {
                    checkPermission(RequestPermission.FMB_COPY)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

            R.id.imgLeaseAgreementInLand -> {
                try {
                    checkPermission(RequestPermission.LEASE_AGREMENT)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun validateEditMoreItem(addMoreItem: java.util.ArrayList<AddMoreItems>?): Boolean {
        var isValidate = true
        var itemVear = "no"
        for (addMoreMap in addMoreItem!!) {
            when {
                addMoreMap.land_ownership == getString(R.string.select_land) -> {
                    addMoreMap.isCheck = true
                    isValidate = false
                    itemVear = "LAND_OWNER"
                }
                addMoreMap.survey_no == "" -> {
                    addMoreMap.isCheck = true
                    isValidate = false
                    itemVear = "SURVEY_NO"
                }
                addMoreMap.sub_division == "" -> {
                    addMoreMap.isCheck = true
                    isValidate = false
                    itemVear = "SUB_DIVISION"
                }
                addMoreMap.land_area == "" -> {
                    addMoreMap.isCheck = true
                    isValidate = false
                    itemVear = "LAND_AREA"
                }

                addMoreMap.FMB_copy == "" -> {
                    addMoreMap.isCheck = true
                    isValidate = false
                    itemVear = "LAND_FMB_COPY"
                }
                addMoreMap.source_of_irrigation == "Select Irrigation" -> {
                    addMoreMap.isCheck = true
                    isValidate = false
                    itemVear = "SOURCE_OF_IRRIGATION"
                }
            }
        }

        Log.d("itemVearsd", "" + itemVear)
        return isValidate
    }


    private fun landAddValidation(map: java.util.HashMap<String, Any>): Boolean {

        if (map[ParamKey.STATE].toString().isEmpty() || map[ParamKey.STATE] == getString(R.string.select_state)) {
            showSnackView(getString(R.string.select_your_state), landSnackView)
            return false
        } else if (map[ParamKey.DISTRICT].toString().isEmpty() || map[ParamKey.DISTRICT] == getString(
                R.string.select_district
            )
        ) {
            showSnackView(getString(R.string.select_your_district), landSnackView)
            return false
        } else if (map[ParamKey.BLOCK].toString().isEmpty() || map[ParamKey.BLOCK] == getString(
                R.string.select_block
            )
        ) {
            showSnackView(getString(R.string.select_your_block), landSnackView)
            return false
        } else if (map[ParamKey.VILLAGE].toString().isEmpty() || map[ParamKey.VILLAGE] == getString(
                R.string.select_village
            )
        ) {
            showSnackView(getString(R.string.select_your_village), landSnackView)
            return false
        } else if (map[ParamKey.CHITTA_NUMBER].toString().isEmpty()) {
            showSnackView(getString(R.string.enter_your_chitta_number), landSnackView)
            return false
        } else if (map[ParamKey.PATTA_CHITTA_COPY].toString().isEmpty()) {
            showSnackView(getString(R.string.select_your_chitta_file), landSnackView)
            return false
        } else if (map[ParamKey.AREA_PROPOSED].toString().isEmpty()) {
            showSnackView(getString(R.string.total_area_cant_be_empty), landSnackView)
            return false
        }
        return true
    }

    fun checkPermission(requestCode: Int) {
        try {
            RequestPermission.checkPermissions(
                requestCode, this, this as CallbackPermission,
                PermissionDescription.permissionForCameraAndStorage(this),
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                )
            )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validateAddMore(addMoreMap: java.util.HashMap<String, Any>): Boolean {
        when {
            addMoreMap[LAND_OWNER].toString().isEmpty() -> {
                showSnackView(getString(R.string.select_your_land_ownership), landSnackView)
                return false
            }
            addMoreMap[ParamKey.SURVEY_NO].toString().isEmpty() -> {
                showSnackView(
                    getString(R.string.please_enter_your_surveyNumber),
                    landSnackView
                )
                return false
            }
            addMoreMap[ParamKey.SUB_DIVISION].toString().isEmpty() -> {
                showSnackView(getString(R.string.enter_your_sub_division), landSnackView)
                return false
            }
            addMoreMap[ParamKey.LAND_AREA].toString().isEmpty() -> {
                showSnackView(getString(R.string.enter_your_land_area), landSnackView)
                return false
            }
        }
        return true
    }

    override fun onResponseSuccess(
        responseBody: Response<ResponseBody>,
        api: String
    ) {
        when (api) {
            ParamAPI.DISTRICT_LIST -> {
                setDistrictSpinner(responseBody)
            }
            ParamAPI.BLOCK_LIST -> {
                setBlockSpinner(responseBody)
            }
            ParamAPI.VILLAGE_LIST -> {
                setVillageSpinner(responseBody)
            }
            ParamAPI.ADD_LAND -> {
                logMessage("AddNew land", "waiting")
                //appDetailsToAddMore.remove(map)
            }
            ParamAPI.LAND_EDIT -> {
                val modelEditSuccessfully = Gson().fromJson(
                    responseBody.body()?.string(),
                    ModelEditSuccessfully::class.java
                )
                if (modelEditSuccessfully != null) {
                    if (modelEditSuccessfully.status!!) {
                        MessageUtils.showToastMessage(
                            this,
                            modelEditSuccessfully.message!!,
                            false
                        )
                        finish()
                    } else {
                        showSnackView(modelEditSuccessfully.message!!, landSnackView)
                    }
                } else {
                    showSnackView(getString(R.string.something_went_wrong), landSnackView)
                }
            }
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, landSnackView)
    }


    /**
     * Get Values From Server and Set in Adapter for Village
     */

    private fun setVillageSpinner(responseBody: Response<ResponseBody>) {
        val modelVillage = Gson().fromJson(
            responseBody.body()?.string(),
            ModelVillageDTO::class.java
        )
        if (modelVillage != null) {
            if (modelVillage.status!!) {
                villageDetails = modelVillage.data as ArrayList<VillageDTO?>
                val count =
                    if (villageDetails != null) villageDetails.size else 0
                if (count != 0) {
                    villageInfo.clear()
                    villageInfo.add(getString(R.string.select_village))
                    for ((indext, values) in villageDetails.withIndex()) {
                        villageInfo.add(values?.village_name!!)
                    }
                    spVillageInLand.adapter = ArrayAdapter(
                        this@EditLandDetails,
                        R.layout.single_view,
                        R.id.singe_item, villageInfo
                    )
                    spVillageInLand.setSelection(villageInfo.indexOf(landDetails!!.villagename!!.villageName!!))
                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        landSnackView
                    )
                }
            } else {
                showSnackView(modelVillage.message!!, landSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), landSnackView)
        }
    }

    /**
     * Get Values From Server and Set in Adapter for Block Spinner
     */
    private fun setBlockSpinner(responseBody: Response<ResponseBody>) {
        val modelBlock = Gson().fromJson(
            responseBody.body()?.string(),
            ModelBlockDTO::class.java
        )

        if (modelBlock != null) {
            if (modelBlock.status!!) {
                blockDetails = modelBlock.data as ArrayList<BlockDTO?>
                val count =
                    if (blockDetails != null) blockDetails.size else 0
                if (count != 0) {
                    blockInfo.clear()
                    blockInfo.add(getString(R.string.select_block))
                    for ((indext, values) in blockDetails.withIndex()) {
                        blockInfo.add(values?.block_name!!)
                    }

                    spBlockInLand.adapter = ArrayAdapter(
                        this@EditLandDetails,
                        R.layout.single_view,
                        R.id.singe_item, blockInfo
                    )
                    spBlockInLand.setSelection(blockInfo.indexOf(landDetails!!.appblock!!.blockName!!))
                    setVillageDefault()
                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        landSnackView
                    )
                }
            } else {
                showSnackView(modelBlock.message!!, landSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), landSnackView)
        }
    }


    /**
     * Get Values From Server and Set in Adapter for District Spinner
     */
    private fun setDistrictSpinner(responseBody: Response<ResponseBody>) {
        val modelDistrict: ModelDistrictDTO = Gson().fromJson(
            responseBody.body()?.string(),
            ModelDistrictDTO::class.java
        )

        if (modelDistrict != null) {
            if (modelDistrict.status!!) {
                districtDetails = modelDistrict.data as ArrayList<DistrictDTO?>
                val count =
                    if (districtDetails != null) districtDetails.size else 0
                if (count != 0) {
                    districtInfo.clear()
                    districtInfo.add(getString(R.string.select_district))
                    for ((indext, values) in districtDetails.withIndex()) {
                        districtInfo.add(values?.district_name!!)
                    }

                    spDistrictInLand.adapter = ArrayAdapter(
                        this@EditLandDetails,
                        R.layout.single_view,
                        R.id.singe_item, districtInfo
                    )
                    spDistrictInLand.setSelection(districtInfo.indexOf(landDetails!!.appdistrict!!.districtName!!))
                    setBlockDefault()
                    setVillageDefault()

                } else {
                    showSnackView(
                        getString(R.string.district_details_not_found),
                        landSnackView
                    )
                }
            } else {
                showSnackView(modelDistrict.message!!, landSnackView)
            }
        } else {
            showSnackView(getString(R.string.something_went_wrong), landSnackView)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        MessageUtils.dismissSnackBar(snackView)
    }


    private fun setVillageDefault() {
        val village = resources.getStringArray(R.array.village_list)
        spVillageInLand.adapter = ArrayAdapter<String>(
            this@EditLandDetails,
            R.layout.single_view,
            R.id.singe_item,
            village
        )
    }

    private fun setBlockDefault() {
        val block = resources.getStringArray(R.array.block_list)
        spBlockInLand.adapter = ArrayAdapter<String>(
            this@EditLandDetails,
            R.layout.single_view,
            R.id.singe_item,
            block
        )
    }

    private fun setDistrictDefault() {
        val district = resources.getStringArray(R.array.district_list)
        spDistrictInLand.adapter = ArrayAdapter<String>(
            this@EditLandDetails,
            R.layout.single_view,
            R.id.singe_item,
            district
        )
    }

    override fun onActionAfterPermissionGrand(requestCode: Int) {
        RequestPermission.choosePhotoFromGallery(this, requestCode)
    }

    override fun activityResultCallback(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (data != null) {
            val contentURI = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(
                contentResolver,
                contentURI
            )
            Log.d("parent", "" + contentURI)
            when (requestCode) {
                RequestPermission.PATTA_CHITTA -> {
                    pattaChittaCopy = ImageUtils.saveImage(this, bitmap)
                    if (Utils.getFileSize(pattaChittaCopy) <= Utils.IMAGE_SIZE) {
                        imgChittaInLand.setImageBitmap(bitmap)
                        txtChittaName.setText(File(pattaChittaCopy).name)
                        pattaCopySize.visibility = View.GONE
                    } else {
                        pattaChittaCopy = ""
                        imgChittaInLand.setImageResource(R.drawable.ic_menu_camera)
                        txtChittaName.setText("")
                        pattaCopySize.visibility = View.VISIBLE
                    }
                }
                RequestPermission.FMB_COPY -> {

                    FMBCopy = ImageUtils.saveImage(this, bitmap)
                    if (Utils.getFileSize(FMBCopy) <= Utils.IMAGE_SIZE) {
                        imgFMBCopyInLand.setImageBitmap(bitmap)
                        txtFMBCopyInLand.setText(File(FMBCopy).name)
                        fmbCopySize.visibility = View.GONE
                    } else {
                        FMBCopy = ""
                        imgFMBCopyInLand.setImageResource(R.drawable.ic_menu_camera)
                        txtFMBCopyInLand.setText("")
                        fmbCopySize.visibility = View.VISIBLE
                    }
                }
                RequestPermission.LEASE_AGREMENT -> {
                    leaseAgreementCopy = ImageUtils.saveImage(this, bitmap)
                    if (Utils.getFileSize(leaseAgreementCopy) <= Utils.IMAGE_SIZE) {
                        imgLeaseAgreementInLand.setImageBitmap(bitmap)
                        txtLeaseAgreementInLand.setText(File(leaseAgreementCopy).name)
                        leaseCopySize.visibility = View.GONE
                    } else {
                        leaseAgreementCopy = ""
                        imgLeaseAgreementInLand.setImageResource(R.drawable.ic_menu_camera)
                        txtLeaseAgreementInLand.setText("")
                        leaseCopySize.visibility = View.VISIBLE
                    }
                }

                else -> {
                    itemUpdate!!.updateItems(
                        requestCode,
                        resultCode,
                        data,
                        holder!!,
                        position
                    )
                }

            }
        }
    }

    override fun onError(message: String, api: String) {
        showSnackView(
            message,
            landSnackView
        )
    }

    class ItemLandInfo(
        val activity: BaseActivity,
        val addMoreItem: ArrayList<AddMoreItems>?
    ) : RecyclerView.Adapter<ItemLandInfo.HolderView>(), ItemUpdate {

        override fun updateItems(
            requestCode: Int,
            resultCode: Int,
            data: Intent?,
            holder: HolderView,
            position: Int
        ) {
            Log.d("askalks", "" + requestCode)
            try {
                val contentURI = data!!.data
                val bitmap = MediaStore.Images.Media.getBitmap(
                    activity.contentResolver,
                    contentURI
                )
                when (requestCode) {
                    //addMoreMap[ParamKey.LEASE_AGREEMENT_COPY] = itemLand.leaseAgreement.toString()
                    //addMoreMap[ParamKey.LAND_FMB_COPY] = itemLand.fmbCopy.toString()
                    RequestPermission.LEASE_AGREMENT_MORE -> {
                        if (holder != null) {
                            leaseAgreementCopyMore = ImageUtils.saveImage(activity, bitmap)
                            if (Utils.getFileSize(leaseAgreementCopyMore) <= Utils.IMAGE_SIZE) {
                                holder.imgLeaseAgreementInLandMore!!.setImageBitmap(bitmap)
                                holder.txtLeaseAgreementInLandMore!!.text =
                                    File(leaseAgreementCopyMore).name
                            } else {
                                leaseAgreementCopyMore = ""
                                holder.imgLeaseAgreementInLandMore!!.setImageResource(R.drawable.ic_menu_camera)
                                holder.txtLeaseAgreementInLandMore!!.text = ""
                            }
                            addMoreItem!![position].lease_agreement_copy =
                                leaseAgreementCopyMore
                        }
                    }

                    RequestPermission.FMB_COPY_MORE -> {
                        if (holder != null) {
                            fmbCopyMore = ImageUtils.saveImage(activity, bitmap)
                            if (Utils.getFileSize(fmbCopyMore!!) <= Utils.IMAGE_SIZE) {
                                holder.imgFMBCopyInLandMore!!.setImageBitmap(bitmap)
                                holder.txtFMBCopyInLandMore!!.text = File(fmbCopyMore!!).name
                            } else {
                                fmbCopyMore = ""
                                holder.imgFMBCopyInLandMore!!.setImageResource(R.drawable.ic_menu_camera)
                                holder.txtFMBCopyInLandMore!!.text = ""
                            }
                            addMoreItem!![position].FMB_copy = fmbCopyMore
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderView {
            return HolderView(
                LayoutInflater.from(activity).inflate(
                    R.layout.add_more_lay_land_details,
                    null
                )
            )
        }

        override fun getItemCount(): Int {
            return addMoreItem!!.size
        }

        @RequiresApi(Build.VERSION_CODES.O)
        override fun onBindViewHolder(holder: HolderView, position: Int) {

            //val addMoreMap = addMoreItem!![position]
            val landOwner = activity.resources.getStringArray(R.array.land_owner)
            val sourceOfIrrigation =
                activity.resources.getStringArray(R.array.source_of_irrigation)
            try {
                if (addMoreItem!![position].survey_no.isNotEmpty()) {
                    holder.edtSurveyNoInLandMore.setText(addMoreItem[position].survey_no.toString())
                } else {
                    holder.edtSurveyNoInLandMore.setText("")
                }
                if (addMoreItem[position].sub_division.isNotEmpty()) {
                    holder.edtSubDivisionInLandMore.setText(addMoreItem[position].sub_division.toString())
                } else {
                    holder.edtSubDivisionInLandMore.setText("")
                }
                if (addMoreItem[position].land_area.isNotEmpty()) {
                    holder.edtLandAreaInLandMore.setText(addMoreItem[position].land_area.toString())
                } else {
                    holder.edtLandAreaInLandMore.setText("")
                }


            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            try {
                Utils.editTextFocusable(holder.edtLandAreaInLandMore)
                Utils.editTextFocusable(holder.edtSubDivisionInLandMore)
                Utils.editTextFocusable(holder.edtSurveyNoInLandMore)
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }

            holder.nos.text = "" + position

            try {
                if (addMoreItem!![position].FMB_copy!!.toString().isNotEmpty()) {
                    holder.layOfOwnMore!!.visibility = View.VISIBLE
                    ImageUtils.setImageWithCatch(
                        holder.imgFMBCopyInLandMore,
                        Utils.IMAGE_URL_PATH + addMoreItem!![position].FMB_copy!!.toString(),
                        activity
                    )
                } else {
                    holder.imgFMBCopyInLandMore!!.setImageResource(R.drawable.ic_menu_camera)
                    holder.layOfOwnMore!!.visibility = View.VISIBLE
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                if (addMoreItem!![position].lease_agreement_copy.toString().isNotEmpty()) {
                    holder.layOfLeaseMore.visibility = View.VISIBLE
                    ImageUtils.setImageWithCatch(
                        holder.imgLeaseAgreementInLandMore,
                        Utils.IMAGE_URL_PATH + addMoreItem[position].lease_agreement_copy.toString(),
                        activity
                    )
                } else {
                    if (addMoreItem[position].land_ownership.toString() == "Own") {
                        holder.layOfLeaseMore.visibility = View.GONE
                    } else {
                        holder.layOfLeaseMore.visibility = View.VISIBLE
                    }
                    holder.imgLeaseAgreementInLandMore.setImageResource(R.drawable.ic_menu_camera)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                if (addMoreItem!![position].source_of_irrigation!!.toString().isNotEmpty()) {
                    //childView.edtSourceOfIrrigationInLand.setText(addMoreMap[ParamKey.SOURCE_OF_IRRIGATION]!!.toString())
                    holder.spSourceOfIrrigationInLandMore.setSelection(
                        sourceOfIrrigation.indexOf(addMoreItem!![position].source_of_irrigation!!.toString())
                    )
                } else {
                    holder.spSourceOfIrrigationInLandMore.setSelection(0)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                if (addMoreItem!![position].land_ownership!!.toString().isNotEmpty()) {
                    // childView.edtLandOwnershipInLand.setText(addMoreMap[ParamKey.LAND_OWNER]!!.toString())
                    holder.spLandOwnershipMore.setSelection(landOwner.indexOf(addMoreItem!![position].land_ownership!!.toString()))
                    /*if (addMoreMap[ParamKey.LAND_OWNER]!!.toString() == "Own") {
                        holder.layOfOwnMore.visibility = View.VISIBLE
                        holder.layOfLeaseMore.visibility = View.GONE
                    } else {
                        holder.layOfOwnMore.visibility = View.VISIBLE
                        holder.layOfLeaseMore.visibility = View.VISIBLE
                    }*/
                } else {
                    holder.spLandOwnershipMore.setSelection(0)
                    /*holder.layOfOwnMore.visibility = View.GONE
                    holder.layOfLeaseMore.visibility = View.GONE*/
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            holder.spLandOwnershipMore.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(
                        p0: AdapterView<*>?,
                        p1: View?,
                        p2: Int,
                        p3: Long
                    ) {
                        try {
                            val spLandOwner = holder.spLandOwnershipMore.selectedItem.toString()
                            if (spLandOwner != activity.getString(R.string.select_land)) {
                                addMoreItem!![position].land_ownership = spLandOwner
                                if (spLandOwner == "Own") {
                                    holder.layOfOwnMore.visibility = View.VISIBLE
                                    holder.layOfLeaseMore.visibility = View.GONE
                                } else {
                                    holder.layOfOwnMore.visibility = View.VISIBLE
                                    holder.layOfLeaseMore.visibility = View.VISIBLE
                                }
                            }
                        } catch (ex: java.lang.Exception) {
                            ex.printStackTrace()
                        }
                    }

                }


            holder.spSourceOfIrrigationInLandMore.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(
                        p0: AdapterView<*>?,
                        p1: View?,
                        p2: Int,
                        p3: Long
                    ) {
                        try {
                            val spSocial =
                                holder.spSourceOfIrrigationInLandMore.selectedItem.toString()
                            if (spSocial != "Select Irrigation") {
                                addMoreItem!![position].source_of_irrigation = spSocial
                            }
                        } catch (ex: java.lang.Exception) {
                            ex.printStackTrace()
                        }
                    }

                }

            /* holder.edtSubDivisionInLandMore.addTextChangedListener(object : TextWatcher {
                 override fun afterTextChanged(s: Editable?) {
                     try {
                         val input = s.toString()
                         if (input.isNotEmpty()) {
                             addMoreMap.sub_division = input
                         }
                     } catch (ex: Exception) {
                         ex.printStackTrace()
                     }
                 }

                 override fun beforeTextChanged(
                     s: CharSequence?,
                     start: Int,
                     count: Int,
                     after: Int
                 ) {
                 }

                 override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                 }
             })*/
            holder.edtSubDivisionInLandMore.setOnFocusChangeListener { v, hasFocus ->
                Log.d("addMoreItemsds", "" + addMoreItem!!.size)
                if (!hasFocus) {
                    try {
                        val input = holder.edtSubDivisionInLandMore.text.toString()
                        val surveyNo = holder.edtSurveyNoInLandMore.text.toString()

                        addMoreItem.forEachIndexed { index, addMoreItems ->
                            Log.d("sdssdds", "" + addMoreItems.sub_division + "  " + input)
                            if (addMoreItems.survey_no.isNotEmpty() && surveyNo.isNotEmpty()) {
                                if (addMoreItems.survey_no == surveyNo) {
                                    if (addMoreItems.sub_division.isNotEmpty() && input.isNotEmpty())
                                        if (addMoreItems.sub_division == input) {
                                            holder.edtSubDivisionInLandMore.setText("")
                                            activity.showSnackView(
                                                activity.getString(R.string.sub_division_number_exist),
                                                activity.landSnackView
                                            )

                                            MessageUtils.showToastMessage(
                                                activity,
                                                activity.getString(R.string.sub_division_number_exist),
                                                false
                                            )
                                            return@forEachIndexed
                                        }
                                }
                            }

                        }
                        Log.d("inputsd", "subdivision $input")
                        addMoreItem[position].sub_division = input

                    } catch (ex: java.lang.Exception) {
                        ex.printStackTrace()
                    }
                }
            }

            holder.edtSurveyNoInLandMore.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    try {
                        val input = s.toString()
                        if (input.isNotEmpty()) {
                            addMoreItem!![position].survey_no = input
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }
            })

            /*holder.edtSurveyNoInLandMore.setOnFocusChangeListener { v, hasFocus ->
                Log.d("addMoreItemsdssdsdfssd", "" + addMoreItem.size)
                if (!hasFocus) {
                    try {
                        val input = holder.edtSurveyNoInLandMore.text.toString()
                        addMoreItem.forEachIndexed { index, addMoreItems ->
                            Log.d("sdssdds", "nodata " + addMoreItems.survey_no + "  " + input)
                            if (addMoreItems.survey_no.isNotEmpty() && input.isNotEmpty())
                                if (addMoreItems.survey_no == input) {
                                    Log.d("sdssdds", "sdssdds is come")
                                    holder.edtSurveyNoInLandMore.setText("")
                                    activity.showSnackView(
                                        activity.getString(R.string.survey_number_exist),
                                        activity.landSnackView
                                    )
                                    return@forEachIndexed
                                }
                        }

                        addMoreMap.survey_no = input
                    } catch (ex: java.lang.Exception) {
                        ex.printStackTrace()
                    }
                }
            }*/
            holder.edtLandAreaInLandMore.isFocusedByDefault = false

            holder.edtLandAreaInLandMore.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    try {
                        var input = s.toString()
                        if (input.isNotEmpty()) {
                            if (input == ".") {
                                input = "0$input"
                            }
                            val inputArea = input.toDouble()
                            var totalLand = 0.0
                            addMoreItem!![position].land_area = inputArea.toString()
                            addMoreItem.forEachIndexed { index, addMoreItems ->
                                Log.d("totalLand___", "before" + addMoreItems.land_area)
                                if (addMoreItems.land_area.isNotEmpty()) {
                                    totalLand += addMoreItems.land_area.toDouble()
                                }
                                Log.d("totalLand____", "after" + addMoreItems.land_area)
                            }
                            Log.d("totalLand", "before $totalLand")
                            //totalLand += inputArea
                            totalLand += Utils.REGISTERED_LAND_COUNT
                            Log.d("totalLand", "after $totalLand")
                            if (Utils.validateFarmerTypeLandAllow(
                                    totalLand,
                                    SessionManager.getObject(activity).farmer_type.toString(),
                                    activity.landSnackView,
                                    activity
                                )
                            ) {

                            } else {
                                holder.edtLandAreaInLandMore.setText("")
                                addMoreItem[position].land_area = "0"
                            }
                        } else {
                            addMoreItem!![position].land_area = "0"
                        }
                        calculateLandArea("ooo", position)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })

            holder.txtAddMore.text = activity.getString(R.string.remove_item)
            holder.txtAddMore.setTextColor(activity.resources.getColor(R.color.colorRed))

            holder.txtAddMore.setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_remove_circle_red,
                0
            )

            holder.layOfAddOrRemove.setOnClickListener {
                try {
                    addMoreItem!!.removeAt(position)
                    if (addMoreItem.size == 0) {
                        activity.edtLandAreaInLand.setText("")
                    }
                    notifyDataSetChanged()
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }

            holder.imgFMBCopyInLandMore.setOnClickListener {

                try {
                    initCallbackPermission(callbackPermission!!)
                    editImages(this, holder, position)
                    checkPermission(
                        RequestPermission.FMB_COPY_MORE,
                        activity,
                        callbackPermission!!
                    )
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }

            holder.imgLeaseAgreementInLandMore.setOnClickListener {
                try {
                    initCallbackPermission(callbackPermission!!)
                    editImages(this, holder, position)
                    checkPermission(
                        RequestPermission.LEASE_AGREMENT_MORE,
                        activity,
                        callbackPermission!!
                    )
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
        }

        private fun calculateLandArea(input: String, position: Int) {
            var totalArea = 0.0
            addMoreItem!!.forEachIndexed { index, addMoreItems ->
                val land_area = addMoreItems.land_area
                if (land_area.isNotEmpty()) {
                    totalArea += land_area.toDouble()
                }
            }
            activity.txtTotalLandAreaInLand.setText("" + Utils.convertDouble("" + totalArea))
        }

        class HolderView(childView: View) : RecyclerView.ViewHolder(childView) {
            val layOfAddOrRemove = childView.findViewById<LinearLayout>(R.id.layOfAddMore)
            val txtAddMore = childView.findViewById<TextView>(R.id.txtAddMore)
            val spLandOwnershipMore = childView.findViewById(R.id.spLandOwnership) as Spinner
            val edtSurveyNoInLandMore =
                childView.findViewById(R.id.edtSurveyNoInLand) as EditText
            val edtSubDivisionInLandMore =
                childView.findViewById(R.id.edtSubDivisionInLand) as EditText
            val spSourceOfIrrigationInLandMore =
                childView.findViewById(R.id.spSourceOfIrrigationInLand) as Spinner
            val layOfLandBaseImagesMore =
                childView.findViewById(R.id.layOfLandBaseImages) as LinearLayout
            val layOfOwnMore = childView.findViewById(R.id.layOfOwn) as LinearLayout
            val layOfLeaseMore = childView.findViewById(R.id.layOfLease) as LinearLayout
            val txtFMBCopyInLandMore = childView.findViewById(R.id.txtFMBCopyInLand) as TextView
            val edtLandAreaInLandMore =
                childView.findViewById(R.id.edtLandAreaInLand) as EditText
            val txtLeaseAgreementInLandMore =
                childView.findViewById(R.id.txtLeaseAgreementInLand) as TextView
            val imgFMBCopyInLandMore =
                childView.findViewById(R.id.imgFMBCopyInLand) as ImageView
            val nos = childView.findViewById(R.id.nos) as TextView

            val imgLeaseAgreementInLandMore =
                childView.findViewById(R.id.imgLeaseAgreementInLand) as ImageView
        }
    }

}


class AddMoreItems(
    var land_ownership: String,
    var survey_no: String,
    var sub_division: String,
    var land_area: String,
    var lease_agreement_copy: String,
    var FMB_copy: String,
    var source_of_irrigation: String,
    var id: String,
    var isCheck: Boolean
)
