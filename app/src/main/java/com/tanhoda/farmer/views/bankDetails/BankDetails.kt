package com.tanhoda.farmer.views.bankDetails

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R

import kotlinx.android.synthetic.main.activity_bank_details.*

class BankDetails : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.bank_details)
        setContentView(R.layout.activity_bank_details)


        fabAdd.setOnClickListener {
            startActivity(Intent(this, AddNewBank::class.java))

        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("activityStats", "onresum")
    }
}
