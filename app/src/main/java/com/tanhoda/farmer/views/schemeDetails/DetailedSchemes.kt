package com.tanhoda.farmer.views.schemeDetails

import android.app.Dialog
import android.media.Image
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.adapter.schemeDetails.ItemSchemeComponent
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.model.schemeDetails.SchemeComponentItem
import com.tanhoda.farmer.model.schemeDetails.SchemesItem
import com.tanhoda.farmer.utils.ImageUtils
import com.tanhoda.farmer.utils.Utils
import com.tanhoda.farmer.utils.Utils.Companion.htmlCodeConvertColor
import com.tanhoda.farmer.utils.Utils.Companion.htmlCodeConvertSample
import kotlinx.android.synthetic.main.activity_view_detailed_schemes.*


class DetailedSchemes : BaseActivity() {

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_detailed_schemes)
        val schemeDetails = intent.getSerializableExtra(ParamKey.SCHEME_NAME) as SchemesItem?
        try {
            if (schemeDetails != null) {

                txtNoDataFoundInSchemeDetails.visibility = View.GONE
                schemeDetailsScrl.visibility = View.VISIBLE

                ImageUtils.setImageLive(
                    imgScheme,
                    Utils.IMAGE_URL_PATH_SCHEME + schemeDetails.schemeImage, this
                )

                txtSchemeName.text =
                    htmlCodeConvertColor(schemeDetails.schemeName!!, schemeDetails.shortName!!)
                txtDescription.text = htmlCodeConvertSample(schemeDetails.description!!)

                if (schemeDetails.schemecomponent!!.isNotEmpty()) {
                    rcyListOfSubComponentWithApply.layoutManager = LinearLayoutManager(this)
                    schemeDetails.schemecomponent.add(0, SchemeComponentItem())
                    with(schemeDetails.schemecomponent.iterator()) {
                        forEach {
                            if (it!!.active == 0) {
                                remove()
                            }
                        }
                    }

                    rcyListOfSubComponentWithApply.adapter =
                        ItemSchemeComponent(
                            this,
                            schemeDetails.schemecomponent,
                            schemeDetails.schemecategory,
                            schemeDetails.schemeName!!.toString()
                        )

                }

            } else {
                txtNoDataFoundInSchemeDetails.visibility = View.VISIBLE
                schemeDetailsScrl.visibility = View.GONE
                txtNoDataFoundInSchemeDetails.text = "No Data Found"
            }
        } catch (eX: Exception) {
            eX.printStackTrace()
        }

    }


}