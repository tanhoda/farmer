package com.tanhoda.farmer

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamAPI.Companion.LOGIN
import com.tanhoda.farmer.api.ParamAPI.Companion.PASSWORD_HINT
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ParamKey.Companion.PASSWORD
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.login.ModelLogin
import com.tanhoda.farmer.model.login.aadharCheck.ModellAadharCheck
import com.tanhoda.farmer.utils.*
import com.tanhoda.farmer.utils.Utils.Companion.convertDate
import com.tanhoda.farmer.views.registerNewFarmer.RegisterActivity
import com.template.receiver.ConnectivityReceiver
import kotlinx.android.synthetic.main.dialog_language.*
import kotlinx.android.synthetic.main.fragment_splash.*
import okhttp3.ResponseBody
import retrofit2.Response
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode

class SplashFragment : BaseActivity(), ReadWriteAPI {

    var languageDialog: Dialog? = null
    private var mHeaderLay: LinearLayout? = null
    private var layTitle: LinearLayout? = null
    private var latMenu: LinearLayout? = null
    private var layLogo: LinearLayout? = null
    private var mSplashImg: ImageView? = null
    private var menu: Animation? = null
    private var layOfRegister: LinearLayout? = null
    private var imgLogo: ImageView? = null
    private var txtLogin: TextView? = null
    var language = ""
    var sessionManager: SessionManager? = null
    var readWriteAPI: ReadWriteAPI? = null
    var dialogNetwork: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_splash)
        BaseActivity("SplashFragment")
        sessionManager = SessionManager(this)
        readWriteAPI = this


        mHeaderLay = findViewById(R.id.layHeader)
        mSplashImg = findViewById(R.id.imgSplash)
        latMenu = findViewById(R.id.latMenu)
        layTitle = findViewById(R.id.layTitle)
        layLogo = findViewById(R.id.layLogo)
        layOfRegister = findViewById(R.id.layOfRegister)
        imgLogo = findViewById(R.id.imgLogo)
        txtLogin = findViewById(R.id.txtLogin)
        if (savedInstanceState != null) {
            edtAadharNumber.setText(savedInstanceState.getString("aadhar"))
            edtPasswordNumber.setText(savedInstanceState.getString("password"))
        }
        txtHint.text = ""
        mSplashImg?.animate()?.translationY(-400f)?.setDuration(800)?.startDelay = 600
        mHeaderLay?.animate()?.alpha(0f)
        layLogo?.animate()?.alpha(0f)
        /*latMenu?.animate()?.alpha(0f)
        layOfRegister?.animate()?.alpha(0f)
        languageToggle?.animate()?.alpha(0f)*/

       
        if (sessionManager!!.checkLogin()) {
            //latMenu.animate().translationY(0).translationYBy(100).setDuration(600).setStartDelay(300);
            /* Handler().postDelayed({

                 startActivity(Intent(this, MainActivity::class.java))
                 finish()
             }, 800)*/

            if (ConnectivityReceiver.isConnected()) {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                dialogNetwork = MessageUtils.showNetworkDialog(this)
            }
        } else {
            menu = AnimationUtils.loadAnimation(this, R.anim.anim_login_ui)
            latMenu?.animation = menu
            layTitle?.animation = menu
            layOfRegister?.animation = menu
            imgLogo?.animation = menu
            languageToggle.animation = menu
            //layOfRegisterView.animate().alpha(0f).duration = 0
            mHeaderLay?.animate()?.alpha(0f)?.setDuration(800)?.startDelay = 600
            layLogo?.animate()?.alpha(0f)?.setDuration(800)?.startDelay = 600
        }

        checkAndChangeLanguage()
        Utils.checkMobileNumber(edtPasswordNumber)

        txtLogin?.setOnClickListener {

            val map = HashMap<String, String>()
            map[ParamKey.AADHAR_ID] = edtAadharNumber.text.toString()
            map[PASSWORD] = edtPasswordNumber.text.toString()

            if (validation(map)) {
                Connector.callBasic(this, map, readWriteAPI as ReadWriteAPI, LOGIN)
                /*startActivity(Intent(this, MainActivity::class.java))
                finish()*/
            }
        }

        edtPasswordNumber.transformationMethod = PasswordTransformationMethod.getInstance()
        edtAadharNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    val input = s.toString()
                    if (input.isNotEmpty()) {
                        if (input.length == 12) {
                            //if (AadharCard.validateVerhoeff(input)) {
                            val map = HashMap<String, String>()
                            map[ParamKey.AADHAR_ID] = input
                            Connector.callBasic(
                                this@SplashFragment,
                                map,
                                readWriteAPI as ReadWriteAPI,
                                PASSWORD_HINT
                            )
                            /*} else {
                                showSnackView(getString(R.string.enter_valid_aadhar_number), splashSnackView)
                            }*/
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    showSnackView(ex.message!!, splashSnackView)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        txtRegister?.setOnClickListener {
            if (ConnectivityReceiver.isConnected()) {
                startActivity(
                    Intent(this@SplashFragment, RegisterActivity::class.java).putExtra(
                        "from",
                        "register"
                    )
                )
            } else {
                dialogNetwork = MessageUtils.showNetworkDialog(this)
            }
        }
        txtForgotPassword?.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        languageToggle.setOnClickListener {

            if (language == "en") {
                language = LocaleHelper.getLanguage(this@SplashFragment)
                LocaleHelper.setLocale(this@SplashFragment, "ta")
                LocaleHelper.persistForLanguage(this@SplashFragment, true)
                MessageUtils.dismissDialog(languageDialog)
                recreate()
            } else {
                language = LocaleHelper.getLanguage(this@SplashFragment)
                LocaleHelper.setLocale(this@SplashFragment, "en")
                LocaleHelper.persistForLanguage(this@SplashFragment, true)
                MessageUtils.dismissDialog(languageDialog)
                recreate()
            }
        }


    }

    override fun onSaveInstanceState(
        outState: Bundle?,
        outPersistentState: PersistableBundle?
    ) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState?.putString("aadhar", edtAadharNumber.text.toString())
        outState?.putString("password", edtPasswordNumber.text.toString())
    }


    private fun checkAndChangeLanguage() {

        try {
            language = LocaleHelper.getLanguage(this@SplashFragment)
            if (language == "ta") {
                languageToggle.textOff = getString(R.string.english)
            } else {
                languageToggle.textOff = getString(R.string.tamil)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        val isSet = LocaleHelper.isLanguage(this)
        if (!isSet) {
            languageSet()
        }
    }


    private fun languageSet() {
        try {

            languageDialog = Dialog(this@SplashFragment)
            languageDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            languageDialog!!.window.setBackgroundDrawableResource(R.color.colorDialogTrans)
            languageDialog!!.setContentView(R.layout.dialog_language)
            languageDialog!!.setCancelable(false)
            languageDialog!!.btnEnglish.setOnClickListener {
                LocaleHelper.setLocale(this@SplashFragment, "en")
                LocaleHelper.persistForLanguage(this@SplashFragment, true)
                MessageUtils.dismissDialog(languageDialog)
                recreate()
            }

            languageDialog!!.btnTamil.setOnClickListener {
                LocaleHelper.setLocale(this@SplashFragment, "ta")
                LocaleHelper.persistForLanguage(this@SplashFragment, true)
                MessageUtils.dismissDialog(languageDialog)
                recreate()
            }

            languageDialog!!.show()
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }

    }


    private fun validation(map: HashMap<String, String>): Boolean {
        MessageUtils.dismissSnackBar(snackView)
        when {
            map[ParamKey.AADHAR_ID].toString().isEmpty() -> {
                showSnackView(getString(R.string.enter_your_aadhar_number), splashSnackView)
                return false
            }
            map[ParamKey.AADHAR_ID].toString().length < 12 -> {
                showSnackView(getString(R.string.enter_12_didgit_aadhar), splashSnackView)
                return false
            }
            /*else if (!AadharCard.validateVerhoeff(map[ParamKey.AADHAR_ID].toString())) {
                showSnackView("Enter Valid Aadhar Number")
                return false
            }*/  map[PASSWORD].toString().isEmpty() -> {
            showSnackView(getString(R.string.enter_mobile_number), splashSnackView)
            return false
        }
            map[PASSWORD].toString().length < 10 -> {
                showSnackView(getString(R.string.enter_10_digit_mobile), splashSnackView)
                return false
            }
        }
        return true
    }


    override fun onDestroy() {
        super.onDestroy()
        MessageUtils.dismissSnackBar(snackView)
        MessageUtils.dismissDialog(dialogNetwork)
    }

    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        if (LOGIN == api) {
            createLoginSession(responseBody)
        } else if (PASSWORD_HINT == api) {
            try {
                val modelCheckHint =
                    Gson().fromJson(
                        responseBody.body()?.string(),
                        ModellAadharCheck::class.java
                    )

                if (modelCheckHint.status!!) {
                    if (modelCheckHint.data != null) {
                        txtHint.text =
                            "* Password Hint : ******" + modelCheckHint.data.hint.toString()
                    } else {
                        txtHint.text = getString(R.string.enter_valid_aadhar_number)
                    }
                } else {
                    txtHint.text = ""
                    showSnackView(modelCheckHint.message!!, splashSnackView)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    private fun createLoginSession(responseBody: Response<ResponseBody>) {
        val modelLogin: ModelLogin =
            Gson().fromJson(responseBody.body()?.string(), ModelLogin::class.java)

        if (modelLogin != null) {
            if (modelLogin.status!!) {//public void createLoginSession(String name, String email, String mobile, String id, String token) {
                if (modelLogin.data?.farmerdetails != null) {
                    //if (chRemeberMe.isChecked) {
                    //Create Session
                    sessionManager?.createLoginSession(
                        modelLogin.data.farmerdetails.name,
                        modelLogin.data.farmerdetails.email,
                        modelLogin.data.farmerdetails.mobile_number,
                        modelLogin.data.farmerdetails.id.toString(),
                        modelLogin.data.access_token,
                        modelLogin.data.farmerdetails.profile_pic,
                        modelLogin.data.farmerdetails
                    )
                    // }
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                } else {
                    showSnackView(
                        "" + getString(R.string.something_went_wrong),
                        splashSnackView
                    )
                }
            } else {
                showSnackView("" + modelLogin.message, splashSnackView)
            }

        } else {
            showSnackView("" + getString(R.string.something_went_wrong), splashSnackView)
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        showSnackView(message, splashSnackView)
        if (api == PASSWORD_HINT) {
            txtHint.text = ""
        }
    }
}


