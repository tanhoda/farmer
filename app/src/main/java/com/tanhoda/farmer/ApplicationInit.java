package com.tanhoda.farmer;

import android.app.Application;
import android.content.Context;

import com.tanhoda.farmer.utils.ConnectivityReceiverListener;
import com.tanhoda.farmer.utils.LocaleHelper;

import kotlin.jvm.Synchronized;

public class ApplicationInit extends Application {
    private ConnectivityReceiverListener connectivityReceiverListener;
    static ApplicationInit mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", Fonts.BOLD);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }


    @Synchronized
    public static ApplicationInit getInstance() {
        return mInstance;

    }


    void setConnectivityListener(ConnectivityReceiverListener listener) {
        connectivityReceiverListener = listener;


    }
}
