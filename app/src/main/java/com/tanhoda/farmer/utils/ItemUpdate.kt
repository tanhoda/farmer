package com.tanhoda.farmer.utils

import android.content.Intent
import com.tanhoda.farmer.views.landDetails.EditLandDetails

interface ItemUpdate {

    /**
     * Update Item Images while Image Changes
     */
    fun updateItems(
        requestCode: Int,
        resultCode: Int,
        data: Intent?,
        holder: EditLandDetails.ItemLandInfo.HolderView,
        position: Int
    )
}