package com.tanhoda.farmer.utils

import android.view.View

interface UiUtils {

    /**
     * Show Message with Snack Bar
     */
    fun showSnackView(message: String, view: View)


    /**
     * Used for Developer Purpose only in Development
     */
    fun logMessage(className: String, message: String)
}