package com.tanhoda.farmer.utils;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tanhoda.farmer.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;


/**
 * Created by Mac Mini on 22-06-2019.
 */

public class ImageUtils {
    public static final String IMAGE_DIRECTORY = "/Farmer";
    public static int IMG_MAX_SIZE = 400;

    /**
     * Set imageview using url internal
     *
     * @param image
     * @param url
     * @param activity
     */
    public static void setImage(ImageView image, String url, FragmentActivity activity) {
        Log.e("====IMAGE_URL_LOADER=FROM_LOCAL", "" + url);
        Picasso.get()
                .load(new File(url))
                .placeholder(R.drawable.logo_)
                .into(image);
    }


    /**
     * Set imageview using url live
     *
     * @param image
     * @param url
     * @param activity
     */
    public static void setImageLive(ImageView image, String url, FragmentActivity activity) {
        Log.e("====IMAGE_URL_LOADER=WITHOUT_CATCH", "" + url);
        Picasso.get().load(url).placeholder(R.drawable.logo_).error(R.drawable.logo_).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE).into(image);
    }


    public static void setImageWithCatch(ImageView image, String url, FragmentActivity activity) {
        Log.e("====IMAGE_URL_LOADER=WITh_CATCH", "" + url);
        Picasso.get().load(url).placeholder(R.drawable.logo_).error(R.drawable.logo_).into(image);/*, new Callback() {
            @Override
            public void onSuccess() {
                MessageUtils.dismissDialog(dialog);
            }

            @Override
            public void onError(Exception e) {
                MessageUtils.dismissDialog(dialog);
            }
        });*/
    }

    public static void setImageLiveRealSize(ImageView image, String url, FragmentActivity activity) {
        Log.e("====IMAGE_URL_LOADER=REAL_SIZE", "" + url);
        Picasso.get().load(url).placeholder(R.drawable.logo_).into(image);
    }

    /**
     * Image Compress for Size
     */

    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    public static String compressImage(String imageUri, FragmentActivity activity) {
        try {
            String filePath = getRealPathFromURI(imageUri, activity);
            Bitmap scaledBitmap = null;
            BitmapFactory.Options options = new BitmapFactory.Options();
            //      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
            //      you try the use the bitmap here, you will get null.
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            Log.d("actualWidthsdsd", "actualHeight  " + actualHeight + " actualWidth " + actualWidth);
            //      max Height and width values of the compressed image is taken as 816x612
            /*float maxHeight = 816.0f;
            float maxWidth = 612.0f;*/
            float maxHeight = actualHeight / 3;
            float maxWidth = actualWidth / 3;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;
            //      width and height values are set maintaining the aspect ratio of the image
            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            //      setting inSampleSize value allows to load a scaled down version of the original image

            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

            //      inJustDecodeBounds set to false to load the actual bitmap
            options.inJustDecodeBounds = false;

            //      this options allow android to claim the bitmap memory if it runs low on memory
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                //          load the bitmap from its path
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

            //check the rotation of the image and display it properly
            ExifInterface exif = null;
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
                    exif = new ExifInterface(filePath);
                }

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.d("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                        true);
            } catch (IOException e) {
                e.printStackTrace();
            }

            FileOutputStream out = null;
            String filename = getFilename();
            try {
                out = new FileOutputStream(filename);
                //          write the compressed bitmap at the destination specified by filename.
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            new File(imageUri).delete();

            return filename;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getFilename() {
        try {
            File file = new File("/sdcard/AAZP/"/*Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images"*/);
            if (!file.exists()) {
                file.mkdirs();
            }

            String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
            return uriSting;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getRealPathFromURI(String contentURI, FragmentActivity activity) {
        try {
            Uri contentUri = Uri.parse(contentURI);
            Cursor cursor = activity.getContentResolver().query(contentUri, null, null, null, null);
            if (cursor == null) {
                return contentUri.getPath();
            } else {
                cursor.moveToFirst();
                int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        try {
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }

            return inSampleSize;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static Bitmap rotateImage(AppCompatActivity appCompatActivity, Bitmap bm, int rotation) {
        try {
            if (rotation != 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(rotation);
                Bitmap bmOut = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                return bmOut;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return bm;
    }

    public static String saveImage(FragmentActivity activity, Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(activity,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            //logMessage("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public static float getImageSize(File file) throws IOException {
        float fileSizeInBytes = file.length();

        float fileSizeInKB = fileSizeInBytes / 1024;
        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        float fileSizeInMB = fileSizeInKB / 1024;

        //String calString = Float.toString(fileSizeInMB);
        return fileSizeInMB;


    }

}
