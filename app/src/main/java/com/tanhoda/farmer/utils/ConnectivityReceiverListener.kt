package com.tanhoda.farmer.utils

interface ConnectivityReceiverListener {

    fun onNetworkConnectionChanged(isConnected: Boolean)

}