package com.tanhoda.farmer.utils

import android.app.ActivityOptions
import android.content.Intent
import android.net.Uri
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.APIInterface
import com.tanhoda.farmer.api.APIClient
import android.util.Log
import android.util.Pair
import android.view.View
import android.widget.TextView
import com.tanhoda.farmer.views.ProfileImageView
import java.util.*
import java.util.regex.Pattern
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Build
import android.text.*
import android.view.inputmethod.InputMethodManager
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import com.tanhoda.farmer.BaseActivity
import com.tanhoda.farmer.api.APIClient.Companion.BASE_URL
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat


class Utils {

    companion object {

        var LAND_COUNT_MAX = 0
        var BANK_COUNT_MAX = 0
        var REGISTERED_LAND_COUNT = 0.0
        val PROCEEDING_PDF_VIEW = "http://13.234.154.50/officers/proccedingpdf/"
        val IMAGE_SIZE = 400
        val MARGINAL_FARMER = 1
        val SMALL_FARMER = 2
        val IMAGE_URL_PATH = BASE_URL + "documents/"
        val IMAGE_URL_PATH_SCHEME = BASE_URL + "uploads/schemes/"
        val IMAGE_URL_PATH_PROFILE = BASE_URL + "farmers/profile_pic/"
        val IMAGE_URL_PATH_COMPONETS = BASE_URL + "uploads/components/"
        var PDF_LOADER = BASE_URL + "web/applicationpdf/"
        var JITINVOICEPDF = BASE_URL + "documents/"
        var WORK_ORDER_PDF = BASE_URL + "officers/workorderreleasepdf/"
        var RTGSPDF = BASE_URL + "officers/rtgspdf/"
        val FORMAT_PAN_CARD = "^[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}\$" //ABCDE1234H
        val FORMAT_IFSC_CODE = "^[A-Za-z]{4}0[A-Z0-9a-z]{6}\$"
        val FORMAT_MOBILE_NUMBER = "[6-9]{1}[0-9]{9}"
        val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+.+[a-z]+"
        val PRIVACY_POLICY = "www.google.com"
        val D_NEW_APPLICATION_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"

        val D_FORMAL = "dd-MM-yyyy"

        fun instanceOfRetrofit(): APIInterface {
            return APIClient.getClient().create(APIInterface::class.java)
        }


        fun loadPrivacyPolicy(appCompatActivity: AppCompatActivity, url: String) {
            val builder = CustomTabsIntent.Builder()
            builder.setToolbarColor(ContextCompat.getColor(appCompatActivity, R.color.colorPrimary))
            builder.addDefaultShareMenuItem()

            builder.setStartAnimations(
                appCompatActivity,
                R.anim.left_to_right_start,
                R.anim.right_to_left_start
            )
            builder.setExitAnimations(
                appCompatActivity,
                R.anim.right_to_left_end,
                R.anim.left_to_right_end
            )
            builder.setSecondaryToolbarColor(
                ContextCompat.getColor(
                    appCompatActivity,
                    R.color.colorPrimary
                )
            )

            /*
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(appCompatActivity, Uri.parse(url))
            */

            //val builder = CustomTabsIntent.Builder()
            builder.setToolbarColor(ContextCompat.getColor(appCompatActivity, R.color.colorPrimary))
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(appCompatActivity, Uri.parse(url))


        }

        fun checkMobileNumber(phoneNumber: EditText?) {
            try {
                phoneNumber?.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        val input = s.toString()
                        if (input.isNotEmpty() && input.length == 1) {
                            //if (input.length() == 1) {
                            val inputInt = Integer.parseInt(input)
                            if (inputInt >= 6) {
                                //phoneNumber.setText(input)
                            } else {
                                phoneNumber.setText("6")
                                phoneNumber.setSelection(input.length)

                            }
                            //}
                        }
                    }

                    override fun afterTextChanged(s: Editable) {

                    }
                })
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        /**
         * Validate Expected Format Code
         */

        fun isValidInputFormat(input: String, regExp: String): Boolean {
            return input.matches(regExp.toRegex())
        }

        fun showImage(
            appCompatActivity: AppCompatActivity,
            url: String,
            imgProfileImage: View,
            transitionName: String,
            urlFrom: String,
            title: String
        ) {

            val intent = Intent(appCompatActivity, ProfileImageView::class.java)
            intent.putExtra("imageLogo", url)
            intent.putExtra("imageType", urlFrom)
            intent.putExtra("title", title)
            val options: ActivityOptions =
                ActivityOptions.makeSceneTransitionAnimation(
                    appCompatActivity,
                    Pair.create(imgProfileImage, transitionName)
                )
            appCompatActivity.startActivity(intent, options.toBundle())
        }


        fun findAndRemoveDuplicate(bankList: ArrayList<String>): ArrayList<String> {
            val hashSet = HashSet<String>()
            hashSet.addAll(bankList)
            bankList.clear()
            bankList.addAll(hashSet)
            try {
                bankList.sortWith(Comparator { s1, s2 ->
                    s1!!.toLowerCase().compareTo(s2!!.toLowerCase())
                })
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
            return bankList
        }


        fun findDuplicates(values: ArrayList<HashMap<String, Any>>): ArrayList<HashMap<String, Any>> {
            val hashSet = HashSet<HashMap<String, Any>>()
            hashSet.addAll(values)
            values.clear()
            values.addAll(hashSet)
            return values
        }


        fun findDuplicatesFromPart(values: ArrayList<MultipartBody.Part>): ArrayList<MultipartBody.Part> {
            val hashSet = HashSet<MultipartBody.Part>()
            hashSet.addAll(values)
            values.clear()
            values.addAll(hashSet)
            return values
        }

        fun marquee(farmerName: TextView) {
            farmerName.setSingleLine()
            farmerName.ellipsize = TextUtils.TruncateAt.MARQUEE
            farmerName.isSelected = true
            farmerName.marqueeRepeatLimit = -1
        }

        fun convertDateFormat(dayOfMonth: Int, monthOfYear: Int, year: Int): String {
            var day = "0"
            var monthStr = "0"
            day = if (dayOfMonth < 9) {
                day + "" + dayOfMonth
            } else {
                dayOfMonth.toString()
            }

            monthStr = if (monthOfYear < 9) {
                monthStr + "" + monthOfYear
            } else {
                monthOfYear.toString()
            }
            //Log.d("agesss", "$day/$monthStr/$year")
            return "$day/$monthStr/$year"
        }

        fun getAgeFromDOB(dayOfMonth: Int, monthOfYear: Int, year: Int): Int {
            val dateOfYourBirth = GregorianCalendar(year, (monthOfYear + 1), dayOfMonth)
            val today = Calendar.getInstance()
            var yourAge = today.get(Calendar.YEAR) - dateOfYourBirth.get(Calendar.YEAR)
            dateOfYourBirth.add(Calendar.YEAR, yourAge)
            if (today.before(dateOfYourBirth)) {
                yourAge--
            }

            yourAge += 1
            return yourAge
        }


        fun hideSoftKeyboard(view: View, appCompatActivity: AppCompatActivity) {
            val imm =
                appCompatActivity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun showSoftKeyboard(view: View, appCompatActivity: AppCompatActivity) {
            if (view.requestFocus()) {
                val imm =
                    appCompatActivity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
                imm!!.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            }
        }

        fun multiPartFromFile(filePath: String, key: String): MultipartBody.Part {
            Log.d("filePathsds", "" + filePath)
            val file = File(filePath)
            val requestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), file
            )
            val part = MultipartBody.Part.createFormData(
                key,
                file.name,
                requestBody
            )
            return part
        }

        /**
         * Check File name exists or not
         */
        fun checkFile(fileName: String): Boolean {
            if (fileName.isNotEmpty()) {
                if (File(fileName).exists()) {
                    return true
                }
            }
            return false
        }

        fun stringFromPart(value: String): String {
            return value//RequestBody.create(MediaType.parse("text/plain"), value)
        }

        fun nonEditable(editText: EditText?) {
            editText!!.isFocusableInTouchMode = false
            editText!!.isClickable = false
            editText!!.isLongClickable = false
            editText!!.isClickable = false
            editText!!.isCursorVisible = false
        }


        fun enableEditable(editText: EditText?) {
            editText!!.isFocusableInTouchMode = true
            editText!!.isClickable = true
            editText!!.isLongClickable = true
            editText!!.isClickable = true
            editText!!.isCursorVisible = true
        }

        fun replace(str: String): String {
            return str.replace(",", "");
        }

        fun capitalize(capString: String): String {
            val capBuffer = StringBuffer()
            val capMatcher =
                Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
            while (capMatcher.find()) {
                capMatcher.appendReplacement(
                    capBuffer,
                    capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
                )
            }

            return capMatcher.appendTail(capBuffer).toString();
        }

        fun getFileSize(profilePic: String?): Double {
            val file = File(profilePic)
            // Get length of file in bytes
            val fileSizeInBytes = file.length();
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            val fileSizeInKB = fileSizeInBytes / 1024
            //  Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            val fileSizeInMB = fileSizeInKB / 1024
            Log.d("fileSizeInKBsd", "" + fileSizeInKB)
            return fileSizeInKB.toDouble()
        }

        /**
         * Convert after dot and create two digits
         */

        fun convertDouble(input: String): Double {
            return BigDecimal(input).setScale(2, RoundingMode.HALF_EVEN).toDouble()
        }


        /**
         * Added Bold with Single TextView
         */
        @RequiresApi(Build.VERSION_CODES.N)
        fun htmlCodeConvert(title: String, input: String): Spanned? {
            return Html.fromHtml(
                "<h4><font color='black'>$title</font></h4>$input",
                Html.FROM_HTML_MODE_COMPACT
            )
        }

        /**
         * Added Bold with Single TextView
         */
        @RequiresApi(Build.VERSION_CODES.N)
        fun htmlNextLine(title: String, input: String): Spanned? {
            return Html.fromHtml(
                "<h4><font color='black'>$title</font></h4><br>$input<br/>",
                Html.FROM_HTML_MODE_COMPACT
            )
        }


        @RequiresApi(Build.VERSION_CODES.N)
        fun htmlCodeConvertColor(input: String, shortName: String): Spanned? {
            return Html.fromHtml(
                "$input (<font color='green'>$shortName</font>)",
                Html.FROM_HTML_MODE_COMPACT
            )
        }


        @RequiresApi(Build.VERSION_CODES.N)
        fun htmlCodeConvertSample(input: String): Spanned? {
            return Html.fromHtml(
                "$input",
                Html.FROM_HTML_MODE_COMPACT
            )
        }

        fun convertDate(input: String, inputFormat: String, outputFormat: String): String {
            var spf = SimpleDateFormat(inputFormat)
            val newDate = spf.parse(input)
            spf = SimpleDateFormat(outputFormat)
            return spf.format(newDate)
        }

        @NonNull
        fun createPartFromString(descriptionString: String): RequestBody {
            return RequestBody.create(MediaType.parse("text/plain"), descriptionString);
        }


        @NonNull
        fun prepareFilePart(partName: String, fileUri: Uri): MultipartBody.Part {
            // use the FileUtils to get the actual file by uri
            val file = File(fileUri.toString())

            // create RequestBody instance from file
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.name, requestFile);
        }

        /**
         * Land Edit or Add Land Compare with Old Land Information
         */
        fun validateFarmerTypeLandAllow(
            inputValues: Double,
            farmerType: String,
            snackView: View,
            activity: BaseActivity
        ): Boolean {
            Log.d("farmerTypesdsds", "" + farmerType)
            if (farmerType.toLowerCase().trim().contains("marginal farmer")) {

                if (Utils.MARGINAL_FARMER < inputValues) {
                    MessageUtils.showSnackBar(
                        activity,
                        snackView,
                        activity.getString(R.string.below2ha)
                    )
                    return false
                }
            } else if (farmerType.toLowerCase().trim().contains("small farmer")) {
                if (Utils.SMALL_FARMER < inputValues) {
                    MessageUtils.showSnackBar(
                        activity,
                        snackView,
                        activity.getString(R.string.below_1ha)
                    )
                    return false
                }
            }
            return true
        }


        fun validCellPhone(number: String): Boolean {
            return android.util.Patterns.PHONE.matcher(number).matches()
        }

        fun editTextFocusable(editText: EditText?) {
            val input = editText!!.text.toString()
            if (input.isNotEmpty()) {
                editText.setSelection(input.length)
            }
        }

    }
}