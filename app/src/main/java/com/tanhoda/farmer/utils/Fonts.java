package com.tanhoda.farmer.utils;

public class Fonts {

    String LIGHT = "montserrat/Montserrat-Light.ttf";
    String REGULAR = "montserrat/Montserrat-Regular.ttf";
    String MEDIUM = "montserrat/Montserrat-Medium.ttf";
    String SEM_BOLD = "montserrat/Montserrat-SemiBold.ttf";
    String BOLD = "montserrat/Montserrat-Bold.ttf";
}
