package com.tanhoda.farmer.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.tanhoda.farmer.R;
import com.tanhoda.farmer.SplashFragment;
import com.tanhoda.farmer.model.login.FarmerDetails;

import java.util.HashMap;


public class SessionManager {

    private SharedPreferences pref;
    private Editor editor;


    private FragmentActivity context;
    private int PRIVATE_MODE = 0;

    public static String KEY_ID = "key_id";
    private static String KEY_USERNAME = "key_username";
    private static String KEY_EMAIL = "key_email";
    public static String KEY_MOBILE = "9999999999";
    private static String IS_LOGIN = "IsLogedIn";
    public static String KEY_NO = "PURPLE";
    public static String KET_PROFILE_PHOTO = "";
    public static String KEY_TOKEN = "token";
    public static String LANGUAGES = "English";
    public static String JSON_OBJECT = "json";

    public SessionManager(FragmentActivity context) {
        this.context = context;
        this.pref = context.getSharedPreferences(KEY_NO, PRIVATE_MODE);
        this.editor = pref.edit();
    }


    public void createLoginSession(String name, String email, String mobile, String id, String token, String photo, FarmerDetails farmerDetails) {

        Gson gson = new Gson();
        String json = gson.toJson(farmerDetails);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERNAME, name);
        editor.putString(KET_PROFILE_PHOTO, photo);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_TOKEN, token);
        editor.putString(JSON_OBJECT, json);
        editor.commit();

    }


    public static void updateLoginSession(FragmentActivity context, String name, String email, String mobile, String id, String token, String photo, FarmerDetails farmerDetails) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        Editor editor = pref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(farmerDetails);

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERNAME, name);
        editor.putString(KET_PROFILE_PHOTO, photo);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_TOKEN, token);
        editor.putString(JSON_OBJECT, json);

        editor.commit();

    }

    public void setlanguages(String lang) {
        editor.putString(LANGUAGES, lang);
        editor.commit();
    }

    public boolean checkLogin() {
        return this.isLoggedIn();

    }


    public static FarmerDetails getObject(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        Gson gson = new Gson();
        String json = pref.getString(JSON_OBJECT, "");
        FarmerDetails obj = gson.fromJson(json, FarmerDetails.class);

        return obj;
    }


    public static String getLANGUAGES(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(LANGUAGES, LANGUAGES);
    }


    public static String getToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_TOKEN, KEY_TOKEN);
    }


    public static String getMobile(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_MOBILE, KEY_MOBILE);
    }

    public static String getFarmerId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_ID, KEY_ID);
    }


    public static String getEmailId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_EMAIL, KEY_EMAIL);
    }

    public static String getName(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_USERNAME, KEY_USERNAME);
    }

    public static String getProfileImg(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KET_PROFILE_PHOTO, KET_PROFILE_PHOTO);
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put("profilePhoto", pref.getString(KET_PROFILE_PHOTO, KET_PROFILE_PHOTO));
        user.put("mobileNumber", pref.getString(KEY_MOBILE, KEY_MOBILE));
        user.put("email", pref.getString(KEY_EMAIL, KEY_EMAIL));
        user.put("name", pref.getString(KEY_USERNAME, KEY_USERNAME));
        user.put("id", pref.getString(KEY_ID, KEY_ID));

        return user;
    }

    public static void logoutUser(AppCompatActivity fragmentActivity) {
        SharedPreferences pref = fragmentActivity.getSharedPreferences(KEY_NO, 0);
        Editor editor = pref.edit();
        editor.clear();
        editor.commit();
        MessageUtils.showToastMessage(fragmentActivity, fragmentActivity.getString(R.string.logout_success), false);
        fragmentActivity.startActivity(new Intent(fragmentActivity, SplashFragment.class));
        fragmentActivity.finish();
    }


    public static void upDateProfileImage(String photo, Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        Editor editor = pref.edit();
        editor.putString(KET_PROFILE_PHOTO, photo);
        editor.commit();
    }


    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


}
