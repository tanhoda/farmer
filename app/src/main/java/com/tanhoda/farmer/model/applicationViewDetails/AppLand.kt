package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class AppLand(

    @field:SerializedName("farmerland_id")
    val farmerlandId: Int? = null,

    @field:SerializedName("land_ownership")
    val landOwnership: String? = null,

    @field:SerializedName("servey_no")
    val serveyNo: String? = null,

    @field:SerializedName("active")
    val active: Int? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("land_area")
    val landArea: String? = null,

    @field:SerializedName("source_of_irrigation")
    val sourceOfIrrigation: String? = null,

    @field:SerializedName("lease_agreement")
    val leaseAgreement: String? = null,

    @field:SerializedName("subdivision")
    val subdivision: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("farmer_id")
    val farmerId: Int? = null,

    @field:SerializedName("fmb_copy")
    val fmbCopy: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
) : Serializable