package com.tanhoda.farmer.model.addNewBank.branch

data class ModelBranchNamesDTO(
    val data: BranchDTO? = null,
    val success: Boolean? = null,
    val message: String? = null
)