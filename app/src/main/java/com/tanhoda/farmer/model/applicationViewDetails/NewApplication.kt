package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class NewApplication(

    @field:SerializedName("scheme")
    val scheme: Int? = null,

    @field:SerializedName("gender")
    val gender: Any? = null,

    @field:SerializedName("uzhavanapp_id")
    val uzhavanappId: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("componentname")
    val componentname: ComponentName? = null,

    @field:SerializedName("land_id")
    val landId: Any? = null,

    @field:SerializedName("farmer_type")
    val farmerType: Any? = null,

    @field:SerializedName("scheme_year")
    val schemeYear: Any? = null,

    @field:SerializedName("estimate_cost")
    val estimateCost: String? = null,

    @field:SerializedName("recommended_subsidy")
    val recommendedSubsidy: Any? = null,

    @field:SerializedName("passport_photo")
    val passportPhoto: Any? = null,

    @field:SerializedName("water_test")
    val waterTest: String? = null,

    @field:SerializedName("hortnet_id")
    val hortnetId: String? = null,

    @field:SerializedName("land_records")
    val landRecords: Any? = null,

    @field:SerializedName("bank_loan_applied")
    val bankLoanApplied: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("blockname")
    val blockname: BlockName? = null,

    @field:SerializedName("farmer_id")
    val farmerId: Int? = null,

    @field:SerializedName("subsidy_amount")
    val subsidyAmount: String? = null,

    @field:SerializedName("soil_test")
    val soilTest: String? = null,

    @field:SerializedName("application_status")
    val applicationStatus: String? = null,

    @field:SerializedName("block")
    val block: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("state")
    val state: Int? = null,

    @field:SerializedName("village")
    val village: Int? = null,

    @field:SerializedName("villagename")
    val villagename: VillageName? = null,

    @field:SerializedName("area_proposed")
    val areaProposed: String? = null,

    @field:SerializedName("bank_loan")
    val bankLoan: String? = null,

    @field:SerializedName("active")
    val active: String? = null,

    @field:SerializedName("eligiblity_amount")
    val eligiblityAmount: Any? = null,

    @field:SerializedName("statename")
    val statename: StateName? = null,

    @field:SerializedName("approved_area")
    val approvedArea: String? = null,

    @field:SerializedName("application_id")
    val applicationId: String? = null,

    @field:SerializedName("govt_subsidy")
    val govtSubsidy: String? = null,

    @field:SerializedName("waiting_for")
    val waitingFor: String? = null,

    @field:SerializedName("relevant")
    val relevant: String? = null,

    @field:SerializedName("component")
    val component: Int? = null,

    @field:SerializedName("districtname")
    val districtname: DistrictName? = null,

    @field:SerializedName("district")
    val district: Int? = null,

    @field:SerializedName("schemename")
    val schemename: SchemeName? = null,

    @field:SerializedName("landdetail")
    val landdetail: Any? = null,

    @field:SerializedName("proposed_crop")
    val proposedCrop: String? = null,

    @field:SerializedName("application_date")
    val applicationDate: String? = null,

    @field:SerializedName("categoryname")
    val categoryname: CategoryName? = null,

    @field:SerializedName("category")
    val category: Int? = null,

    @field:SerializedName("proposal_upload")
    val proposalUpload: String? = null,

    @field:SerializedName("status")
    val status: String? = null
) : Serializable