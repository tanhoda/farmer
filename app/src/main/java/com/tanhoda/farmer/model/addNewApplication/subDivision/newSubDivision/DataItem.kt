package com.tanhoda.farmer.model.addNewApplication.subDivision.newSubDivision

data class DataItem(
    val area: Double? = null,
    val subdivision: Subdivision? = null,
    val areaapplied: Double? = null
)
