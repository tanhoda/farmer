package com.tanhoda.farmer.model.addNewApplication.schemeList

data class ModelSchemeListDTO(
    val message: String? = null,
    val success: Boolean? = null,
    val data: DataDTO? = null
)