package com.tanhoda.farmer.model.addNewApplication.subDivision.newSubDivision

data class Farmerlanddetail(
	val chitta_no: String? = null,
	val chitta_copy: String? = null,
	val total_area: String? = null,
	val updated_at: String? = null,
	val farmer_id: Int? = null,
	val district: Int? = null,
	val created_at: String? = null,
	val block: Int? = null,
	val id: Int? = null,
	val state: Int? = null,
	val village: Int? = null
)
