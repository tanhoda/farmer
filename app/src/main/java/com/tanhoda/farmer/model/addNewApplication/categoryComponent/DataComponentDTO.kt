package com.tanhoda.farmer.model.addNewApplication.categoryComponent

import java.io.Serializable

data class DataComponentDTO(
    val id: Int? = null,
    val scheme_id: Int? = null,
    val category: String? = null,
    val create_date: String? = null,
    val active: Int? = null,
    val created_at: String? = null,
    val updated_at: String? = null
)