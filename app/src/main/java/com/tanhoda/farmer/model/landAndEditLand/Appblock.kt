package com.tanhoda.farmer.model.landAndEditLand

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class Appblock(

    @field:SerializedName("block_type")
    val blockType: String? = null,

    @field:SerializedName("do_code")
    val doCode: String? = null,

    @field:SerializedName("phy_all")
    val phyAll: Any? = null,

    @field:SerializedName("division_id")
    val divisionId: Int? = null,

    @field:SerializedName("bill_quantity")
    val billQuantity: Any? = null,

    @field:SerializedName("TBlock_Name")
    val tBlockName: String? = null,

    @field:SerializedName("next_farmer_id")
    val nextFarmerId: Int? = null,

    @field:SerializedName("next_order_id")
    val nextOrderId: String? = null,

    @field:SerializedName("horti_farmer_id")
    val hortiFarmerId: Int? = null,

    @field:SerializedName("next_sample_code")
    val nextSampleCode: Int? = null,

    @field:SerializedName("fin_all")
    val finAll: Any? = null,

    @field:SerializedName("govt_name")
    val govtName: String? = null,

    @field:SerializedName("avail_quantity")
    val availQuantity: Any? = null,

    @field:SerializedName("block_name")
    val blockName: String? = null,

    @field:SerializedName("govt_id")
    val govtId: String? = null,

    @field:SerializedName("Block_Code")
    val blockCode: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("district_id")
    val districtId: Int? = null
) : Serializable