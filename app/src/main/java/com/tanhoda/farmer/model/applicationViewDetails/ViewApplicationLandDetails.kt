package com.tanhoda.farmer.model.applicationViewDetails


import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ViewApplicationLandDetails(

    @field:SerializedName("subdivision")
    val subdivision: String? = null,

    @field:SerializedName("areaproposed")
    val areaproposed: String? = null,

    @field:SerializedName("unit")
    val unit: String? = null,

    @field:SerializedName("district")
    val district: String? = null,

    @field:SerializedName("serveyno")
    val serveyno: String? = null,

    @field:SerializedName("source_of_irregation")
    val sourceOfIrregation: String? = null,

    @field:SerializedName("block")
    val block: String? = null,

    @field:SerializedName("state")
    val state: String? = null,

    @field:SerializedName("chittano")
    val chittano: String? = null,

    @field:SerializedName("village")
    val village: String? = null,

    @field:SerializedName("farmerland")
    val farmerland: String? = null
) : Serializable