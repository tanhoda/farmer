package com.tanhoda.farmer.model.addNewApplication.subDivision



data class FarmerlanddetailDTO(
    val id: Int? = null,
    val farmer_id: Int? = null,
    val state: Int? = null,
    val district: Int? = null,
    val block: Int? = null,
    val village: Int? = null,
    val chitta_no: String? = null,
    val chitta_copy: String? = null,
    val total_area: String? = null,
    val created_at: String? = null,
    val updated_at: String? = null
)