package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class ModelApplicationDetails(

	@field:SerializedName("data")
	val data: AppDetailsDataModel? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
): Serializable