package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class ViewStatusUpdatesItem(

    @field:SerializedName("stages")
    val stages: String? = null,
    @field:SerializedName("status")
    val status: String? = null,
    @field:SerializedName("date")
    val date: String? = null
) : Serializable