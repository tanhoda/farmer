package com.tanhoda.farmer.model.login.aadharCheck

import java.io.Serializable

data class ModellAadharCheck(
    val message: String? = null,
    val status: Boolean? = null,
    val data: Data? = null
)