package com.tanhoda.farmer.model.applicationStatus

import java.io.Serializable

data class ComponentNameDTO(
    val id: Int? = null,
    val scheme_id: Int? = null,
    val category_id: Int? = null,
    val component_name: String? = null,
    val unit: String? = null,
    val maxLimit: String? = null,
    val minLimit: String? = null,
    val type: String? = null,
    val subsidy_amount: String? = null,
    val subsidy_percentage: String? = null,
    val available_in: String? = null,
    val image: String? = null,
    val short_description: String? = null,
    val description: String? = null,
    val worksheet_content: Any? = null,
    val create_date: String? = null,
    val stage: String? = null,
    val active: Int? = null,
    val proposed_crop: String? = null,
    val soilwater_test: String? = null,
    val component_type: String? = null,
    val worksheet_preparation: String? = null,
    val created_at: String? = null,
    val updated_at: String? = null
):Serializable