package com.tanhoda.farmer.model.schemeDetails

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class SchemeComponentItem(

    @SerializedName("max_limit")
    val maxLimit: String? = null,

    @SerializedName("short_description")
    val shortDescription: String? = null,

    @SerializedName("description")
    val description: String? = null,

    @SerializedName("created_at")
    val createdAt: String? = null,

    @SerializedName("worksheet_content")
    val worksheetContent: String? = null,

    @SerializedName("soilwater_test")
    val soilwaterTest: String? = null,

    @SerializedName("type")
    val type: String? = null,

    @SerializedName("subsidy2080to4000")
    val subsidy2080to4000: String? = null,

    @SerializedName("component_type")
    val componentType: String? = null,

    @SerializedName("category_id")
    val categoryId: Int? = null,

    @SerializedName("updated_at")
    val updatedAt: String? = null,

    @SerializedName("scheme_id")
    val schemeId: Int? = null,

    @SerializedName("subsidy_amount")
    val subsidyAmount: String? = null,

    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("create_date")
    val createDate: String? = null,

    @SerializedName("image")
    val image: String? = null,

    @SerializedName("subsidy500to1008")
    val subsidy500to1008: String? = null,

    @SerializedName("worksheet_preparation")
    val worksheetPreparation: String? = null,

    @SerializedName("subsidyUpto500")
    val subsidyupto500: String? = null,

    @SerializedName("min_limit")
    val minLimit: String? = null,

    @SerializedName("component_name")
    val componentName: String? = null,

    @SerializedName("active")
    val active: Int? = null,

    @SerializedName("available_in")
    val availableIn: Any? = null,

    @SerializedName("subsidy1008to2080")
    val subsidy1008to2080: String? = null,

    @SerializedName("unit")
    val unit: String? = null,

    @SerializedName("subsidy_percentage")
    val subsidyPercentage: String? = null,

    @SerializedName("stage")
    val stage: String? = null,

    @SerializedName("proposed_crop")
    val proposedCrop: String? = null
) : Serializable