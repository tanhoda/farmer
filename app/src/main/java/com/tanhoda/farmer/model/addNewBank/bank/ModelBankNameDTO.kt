package com.tanhoda.farmer.model.addNewBank.bank

data class ModelBankNameDTO(
    val data: BankDTO? = null,
    val success: Boolean? = null,
    val message: String? = null
)