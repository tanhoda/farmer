package com.tanhoda.farmer.model.addNewBank.bank

import java.io.Serializable

data class BankDTO(
    val banknames: List<BanknamesDTO>? = null
)