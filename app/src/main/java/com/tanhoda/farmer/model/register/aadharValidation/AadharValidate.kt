package com.tanhoda.farmer.model.register.aadharValidation


data class AadharValidate(
    val message: String? = null,
    val status: Boolean? = null
)