package com.tanhoda.farmer.model.addNewBank.successBank

import com.tanhoda.farmer.model.login.FarmerDetails

data class ModelBankAddedSuccessDTO(
    val message: String? = null,
    val status: Boolean? = null,
    val data: FarmerDetails? = null
)