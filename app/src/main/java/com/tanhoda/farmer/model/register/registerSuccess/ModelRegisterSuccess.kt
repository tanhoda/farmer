package com.tanhoda.farmer.model.register.registerSuccess

import java.io.Serializable

data class ModelRegisterSuccess(
    val message: String? = null,
    val status: Boolean? = null
    //val data: LandAndBankInfo? = null
)