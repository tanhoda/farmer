package com.tanhoda.farmer.model.addNewApplication.subComponent

data class ModelSubComponentDTO(
    val message: String? = null,
    val status: Boolean? = null,
    val data: List<DataSubComponentDTO?>? = null
)