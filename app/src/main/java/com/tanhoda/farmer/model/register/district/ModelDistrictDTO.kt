package com.tanhoda.farmer.model.register.district

data class ModelDistrictDTO(
    val message: String? = null,
    val status: Boolean? = null,
    val data: List<DistrictDTO?>? = null
)