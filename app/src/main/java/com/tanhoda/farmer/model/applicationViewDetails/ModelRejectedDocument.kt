package com.tanhoda.farmer.model.applicationViewDetails

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class ModelRejectedDocument(

    @SerializedName("adh_verified")
    val adhVerified: Any? = null,

    @SerializedName("adh_name")
    val adhName: Any? = null,

    @SerializedName("ho_name")
    val hoName: String? = null,

    @SerializedName("ho_verified")
    val hoVerified: Int? = null,

    @SerializedName("ho_verified_date")
    val hoVerifiedDate: String? = null,

    @SerializedName("ho_status")
    val hoStatus: String? = null,

    @SerializedName("created_at")
    val createdAt: String? = null,

    @SerializedName("land_id")
    val landId: Any? = null,

    @SerializedName("adh_status")
    val adhStatus: String? = null,

    @SerializedName("application_id")
    val applicationId: Int? = null,

    @SerializedName("aho_name")
    val ahoName: Any? = null,

    @SerializedName("verified_date")
    val verifiedDate: Any? = null,

    @SerializedName("document_name")
    val documentName: String? = null,

    @SerializedName("image_name")
    var imageName: String? = null,

    @SerializedName("verified_by")
    val verifiedBy: Any? = null,

    @SerializedName("updated_at")
    val updatedAt: String? = null,

    @SerializedName("appland")
    val appland: Any? = null,

    @SerializedName("adh_verified_date")
    val adhVerifiedDate: Any? = null,

    @SerializedName("farmer_remarks")
    val farmerRemarks: Any? = null,

    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("image_extension")
    val imageExtension: String? = null,

    @SerializedName("remarks")
    var remarks: String? = null,

    @SerializedName("image_type")
    val imageType: String? = null,

    @SerializedName("status")
    val status: String? = null


) : Serializable