package com.tanhoda.farmer.model.addNewApplication.schemeList

import java.io.Serializable

data class ProposedCropDTO(
    val id: Int? = null,
    val crop_name: String? = null,
    val active: Int? = null,
    val created_at: Any? = null,
    val updated_at: String? = null
)