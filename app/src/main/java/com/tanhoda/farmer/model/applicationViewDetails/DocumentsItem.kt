package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class DocumentsItem(

    @field:SerializedName("adh_name")
    val adhName: Any? = null,

    @field:SerializedName("ho_verified")
    val hoVerified: Any? = null,

    @field:SerializedName("ho_status")
    val hoStatus: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("land_id")
    val landId: Any? = null,

    @field:SerializedName("adh_status")
    val adhStatus: String? = null,

    @field:SerializedName("document_name")
    val documentName: String? = null,

    @field:SerializedName("image_name")
    val imageName: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("adh_verified_date")
    val adhVerifiedDate: Any? = null,

    @field:SerializedName("farmer_remarks")
    val farmerRemarks: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("image_extension")
    val imageExtension: String? = null,

    @field:SerializedName("image_type")
    val imageType: String? = null,

    @field:SerializedName("adh_verified")
    val adhVerified: Any? = null,

    @field:SerializedName("ho_name")
    val hoName: Any? = null,

    @field:SerializedName("ho_verified_date")
    val hoVerifiedDate: Any? = null,

    @field:SerializedName("application_id")
    val applicationId: Int? = null,

    @field:SerializedName("aho_name")
    val ahoName: Any? = null,

    @field:SerializedName("verified_date")
    val verifiedDate: Any? = null,

    @field:SerializedName("verified_by")
    val verifiedBy: Any? = null,

    @field:SerializedName("appland")
    val appland: Any? = null,

    @field:SerializedName("verifyuser")
    val verifyuser: Any? = null,

    @field:SerializedName("remarks")
    val remarks: Any? = null,

    @field:SerializedName("status")
    val status: String? = null
) : Serializable