package com.tanhoda.farmer.model.addNewApplication.schemeList

import com.tanhoda.farmer.model.login.FarmerDetails

data class DataDTO(

    val farmer_users: FarmerDetails? = null,
    val schemes: List<SchemeDTO?>? = null,

    val proposed_crop: List<ProposedCropDTO?>? = null,
    val scheme_id: String? = null,
    val categories: String? = null,
    val subcomponentdetail: String? = null,
    val component_id: String? = null,
    val category_d: String? = null,
    val subcomponents: String? = null,
    val districts: List<DistrictsDTO?>? = null,
    val schemecount: Int? = null,
    val schemecheck: String? = null
)