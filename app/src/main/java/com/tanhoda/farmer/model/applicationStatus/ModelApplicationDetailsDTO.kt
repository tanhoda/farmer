package com.tanhoda.farmer.model.applicationStatus

import java.io.Serializable

data class ModelApplicationDetailsDTO(
	val message: String? = null,
	val status: Boolean? = null,
	val data: List<DataDTO?>? = null
):Serializable