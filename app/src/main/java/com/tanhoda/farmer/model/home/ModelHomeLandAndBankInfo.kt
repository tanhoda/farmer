package com.tanhoda.farmer.model.home

data class ModelHomeLandAndBankInfo(
	val data: LandAndBankInfo? = null,
	val message: String? = null,
	val status: Boolean? = null
)
