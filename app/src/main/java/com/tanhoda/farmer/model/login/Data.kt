package com.tanhoda.farmer.model.login

data class Data(
    val farmerdetails: FarmerDetails? = null,
    val access_token: String? = null,
    val token_type: String? = null,
    val expires_at: String? = null
)