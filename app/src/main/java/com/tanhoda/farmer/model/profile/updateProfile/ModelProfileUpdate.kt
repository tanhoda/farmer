package com.tanhoda.farmer.model.profile.updateProfile

import com.google.gson.annotations.SerializedName
import com.tanhoda.farmer.model.login.FarmerDetails
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class ModelProfileUpdate(

    @SerializedName("data")
    val farmerdetails: FarmerDetails? = null,

    @SerializedName("message")
    val message: String? = null,

    @SerializedName("status")
    val status: Boolean? = null
)