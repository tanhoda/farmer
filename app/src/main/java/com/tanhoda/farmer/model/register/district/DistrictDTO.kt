package com.tanhoda.farmer.model.register.district

import java.io.Serializable

data class DistrictDTO(
    val id: Int? = null,
    val district_name: String? = null,
    val collector_name: String? = null,
    val tistrict_name: String? = null,
    val code: String? = null,
    val village_count: Int? = null,
    val next_farmer_id: Int? = null,
    val all_phy: Int? = null,
    val all_fin: Any? = null,
    val mc: Int? = null,
    val gy: Int? = null,
    val goi_district_id: Int? = null
)