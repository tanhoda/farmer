package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class VillageName(

    @field:SerializedName("TVillage_Name")
    val tVillageName: String? = null,

    @field:SerializedName("govt_id")
    val govtId: String? = null,

    @field:SerializedName("district")
    val district: Int? = null,

    @field:SerializedName("block")
    val block: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("village_name")
    val villageName: String? = null,

    @field:SerializedName("aao_id")
    val aaoId: Int? = null,

    @field:SerializedName("Created_Date")
    val createdDate: String? = null,

    @field:SerializedName("govt_name")
    val govtName: String? = null
) : Serializable