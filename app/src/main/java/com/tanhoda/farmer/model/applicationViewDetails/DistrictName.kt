package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class DistrictName(

    @field:SerializedName("district_name")
    val districtName: String? = null,

    @field:SerializedName("gy")
    val gy: Any? = null,

    @field:SerializedName("code")
    val code: String? = null,

    @field:SerializedName("village_count")
    val villageCount: Int? = null,

    @field:SerializedName("all_phy")
    val allPhy: Any? = null,

    @field:SerializedName("mc")
    val mc: Any? = null,

    @field:SerializedName("collector_name")
    val collectorName: String? = null,

    @field:SerializedName("goi_district_id")
    val goiDistrictId: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("TDistrict_Name")
    val tDistrictName: String? = null,

    @field:SerializedName("all_fin")
    val allFin: Any? = null,

    @field:SerializedName("next_farmer_id")
    val nextFarmerId: Int? = null
) : Serializable