package com.tanhoda.farmer.model.register.blockDetails

import java.io.Serializable

data class BlockDTO(
    val id: Int? = null,
    val block_name: String? = null,
    val TBlock_Name: String? = null,
    val district_id: Int? = null,
    val division_id: Int? = null,
    val Block_Code: String? = null,
    val next_farmer_id: Int? = null,
    val next_order_id: String? = null,
    val block_type: String? = null,
    val do_code: String? = null,
    val horti_farmer_id: Int? = null,
    val phy_all: Any? = null,
    val fin_all: Any? = null,
    val next_sample_code: Int? = null,
    val avail_quantity: Any? = null,
    val bill_quantity: Any? = null,
    val govt_name: String? = null,
    val govt_id: String? = null
)