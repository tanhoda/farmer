package com.tanhoda.farmer.model.addNewApplication.subDivision


data class DataSubDivisionDTO(
    val id: Int? = null,
    val farmerId: Int? = null,
    val farmerland_id: Int? = null,
    val land_ownership: String? = null,
    val servey_no: String? = null,
    val subdivision: String? = null,
    val land_area: String? = null,
    val source_of_irrigation: String? = null,
    val fmb_copy: String? = null,
    val lease_agreement: String? = null,
    val active: Int? = null,
    val created_at: String? = null,
    val updated_at: String? = null,
    val farmerlanddetail: FarmerlanddetailDTO? = null
)