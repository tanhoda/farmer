package com.tanhoda.farmer.model.addNewApplication.subComponent

import java.io.Serializable

data class DataSubComponentDTO(
    val id: Int? = null,
    val scheme_id: Int? = null,
    val category_id: Int? = null,
    val component_name: String? = null,
    val unit: String? = null,
    val max_limit: String? = null,
    val min_limit: String? = null,
    val type: String? = null,
    val subsidy_amount: Double? = null,
    val subsidy_percentage: Double? = null,
    val available_in: String? = null,
    val image: String? = null,
    val short_description: String? = null,
    val description: String? = null,
    val worksheet_content: Any? = null,
    val create_date: String? = null,
    val stage: String? = null,
    val active: Int? = null,
    val proposed_crop: String? = null,
    val soilwater_test: String? = null,
    val component_type: String? = null,
    val worksheet_preparation: Any? = null,
    val created_at: String? = null,
    val updated_at: String? = null,

    val subsidyupto500: String? = null,
    val subsidy500to1008: String? = null,
    val subsidy1008to2080: String? = null,
    val subsidy2080to4000: String? = null


)