package com.tanhoda.farmer.model.addNewApplication.schemCountOrAreaApplied


data class DataDTO(
    val schemecount: Double? = null,
    val proposedcrop: String? = null
)