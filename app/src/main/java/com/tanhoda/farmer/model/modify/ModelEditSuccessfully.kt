package com.tanhoda.farmer.model.modify

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class ModelEditSuccessfully(

    @SerializedName("data")
    val data: ModelEditData? = null,

    @SerializedName("message")
    val message: String? = null,

    @SerializedName("status")
    val status: Boolean? = null
)