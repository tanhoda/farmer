package com.tanhoda.farmer.model.login

import java.io.Serializable

data class ModelLogin(
    val status: Boolean? = null,
    val data: Data? = null,
    val message: String? = null
)