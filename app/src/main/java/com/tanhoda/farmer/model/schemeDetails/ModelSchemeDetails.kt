package com.tanhoda.farmer.model.schemeDetails

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class ModelSchemeDetails(

    @SerializedName("data")
    val data: SchemeData? = null,

    @SerializedName("success")
    val success: Boolean? = null,

    @SerializedName("message")
    val message: String? = null
) : Serializable