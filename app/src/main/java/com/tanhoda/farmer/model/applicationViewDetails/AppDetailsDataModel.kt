package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class AppDetailsDataModel(

    @field:SerializedName("statusupdate")
    val statusupdate: StatusUpdate? = null,

    @field:SerializedName("rejecteddocuments")
    val rejecteddocuments: ArrayList<ModelRejectedDocument>? = null,

    @field:SerializedName("documents")
    var documents: ArrayList<DocumentsItem>? = null,

    @field:SerializedName("newapplication")
    val newapplication: NewApplication? = null,

    @field:SerializedName("pendingdocuments")
    val pendingdocuments: ArrayList<ModelPendingDocument>? = null,

    @field:SerializedName("documentsstatus")
    val documentsstatus: ArrayList<DocumentsStatusItem>? = null,

    @field:SerializedName("farmer_users")
    val farmerUsers: FarmerUsers? = null,

    @field:SerializedName("documentsstatus2")
    val documentsstatus2: Int? = null,

    @field:SerializedName("category")
    val component: Component? = null,

    @field:SerializedName("lands")
    val lands: ArrayList<ViewApplicationLandDetails?>? = null,

    @field:SerializedName("stages")
    val stages: ArrayList<String?>? = null,

    @field:SerializedName("viewstatusupdates")
    val viewstatusupdates: ArrayList<ViewStatusUpdatesItem>? = null,

    @field:SerializedName("workorder")
    val workorder: Int? = null

) : Serializable