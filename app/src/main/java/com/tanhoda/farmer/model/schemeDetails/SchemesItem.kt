package com.tanhoda.farmer.model.schemeDetails

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class SchemesItem(

    @SerializedName("short_description")
    val shortDescription: String? = null,

    @SerializedName("scheme_image")
    val schemeImage: String? = null,

    @SerializedName("description")
    val description: String? = null,

    @SerializedName("active")
    val active: Int? = null,

    @SerializedName("created_at")
    val createdAt: String? = null,

    @SerializedName("scheme_type")
    val schemeType: String? = null,

    @SerializedName("available_in")
    val availableIn: String? = null,

    @SerializedName("scheme_name")
    val schemeName: String? = null,

    @SerializedName("hortnet_id")
    val hortnetId: String? = null,

    @SerializedName("updated_at")
    val updatedAt: String? = null,

    @SerializedName("schemecomponent")
    val schemecomponent: ArrayList<SchemeComponentItem?>? = null,

    @SerializedName("schemecategory")
    val schemecategory: ArrayList<SchemecategoryItem?>? = null,

    @SerializedName("short_name")
    val shortName: String? = null,

    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("create_date")
    val createDate: String? = null
) : Serializable