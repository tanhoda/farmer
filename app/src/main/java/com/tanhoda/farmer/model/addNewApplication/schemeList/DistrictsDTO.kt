package com.tanhoda.farmer.model.addNewApplication.schemeList

import java.io.Serializable

data class DistrictsDTO(
    val id: Int? = null,
    val district_name: String? = null,
    val collector_name: String? = null,
    val tDistrict_name: String? = null,
    val code: String? = null,
    val village_count: Int? = null,
    val next_farmer_id: Int? = null,
    val all_phy: Any? = null,
    val all_fin: Any? = null,
    val mc: Any? = null,
    val gy: Any? = null,
    val goi_district_id: Int? = null
)