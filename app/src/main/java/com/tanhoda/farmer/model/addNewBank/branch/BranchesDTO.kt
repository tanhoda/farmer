package com.tanhoda.farmer.model.addNewBank.branch

import java.io.Serializable

data class BranchesDTO(
    val id: Int? = null,
    val bank_branch_name: String? = null,
    val tbank_branch_name: String? = null,
    val bank_id: Int? = null,
    val district: Int? = null,
    val block_id: Int? = null
)