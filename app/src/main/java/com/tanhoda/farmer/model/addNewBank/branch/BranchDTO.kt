package com.tanhoda.farmer.model.addNewBank.branch

import java.io.Serializable

data class BranchDTO(
    val branches: List<BranchesDTO>? = null
)