package com.tanhoda.farmer.model.addNewApplication.subDivision

data class ModelSubDivisionDTO(
    val message: String? = null,
    val status: Boolean? = null,
    val data: ArrayList<DataSubDivisionDTO>? = null
)