package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class SchemeName(

	@field:SerializedName("short_description")
	val shortDescription: String? = null,

	@field:SerializedName("scheme_image")
	val schemeImage: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("scheme_type")
	val schemeType: String? = null,

	@field:SerializedName("available_in")
	val availableIn: String? = null,

	@field:SerializedName("scheme_name")
	val schemeName: String? = null,

	@field:SerializedName("hortnet_id")
	val hortnetId: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("short_name")
	val shortName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("create_date")
	val createDate: String? = null
): Serializable