package com.tanhoda.farmer.model.schemeDetails

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class SchemecategoryItem(

    @SerializedName("updated_at")
    val updatedAt: String? = null,

    @SerializedName("scheme_id")
    val schemeId: Int? = null,

    @SerializedName("active")
    val active: Int? = null,

    @SerializedName("created_at")
    val createdAt: String? = null,

    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("category")
    val category: String? = null,

    @SerializedName("create_date")
    val createDate: String? = null
) : Serializable