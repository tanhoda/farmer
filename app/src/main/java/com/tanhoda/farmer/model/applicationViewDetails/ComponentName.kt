package com.tanhoda.farmer.model.applicationViewDetails

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class ComponentName(

    @field:SerializedName("max_limit")
    val maxLimit: String? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("short_description")
    val shortDescription: String? = null,

    @field:SerializedName("worksheet_preparation")
    val worksheetPreparation: String? = null,

    @field:SerializedName("min_limit")
    val minLimit: String? = null,

    @field:SerializedName("component_name")
    val componentName: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("active")
    val active: Int? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("worksheet_content")
    val worksheetContent: Any? = null,

    @field:SerializedName("soilwater_test")
    val soilwaterTest: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("available_in")
    val availableIn: String? = null,

    @field:SerializedName("component_type")
    val componentType: String? = null,

    @field:SerializedName("unit")
    val unit: String? = null,

    @field:SerializedName("subsidy_percentage")
    val subsidyPercentage: String? = null,

    @field:SerializedName("category_id")
    val categoryId: Int? = null,

    @field:SerializedName("stage")
    val stage: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("scheme_id")
    val schemeId: Int? = null,

    @field:SerializedName("subsidy_amount")
    val subsidyAmount: String? = null,

    @field:SerializedName("proposed_crop")
    val proposedCrop: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("create_date")
    val createDate: String? = null
) : Serializable