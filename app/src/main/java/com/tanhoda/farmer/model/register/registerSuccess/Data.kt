package com.tanhoda.farmer.model.register.registerSuccess

import java.io.Serializable

data class Data(
    val farmer_type: String? = null,
    val mobile_number: String? = null,
    val name: String? = null,
    val email: String? = null,
    val gname: String? = null,
    val gender: String? = null,
    val social_status: String? = null,
    val dob: String? = null,
    val age: String? = null,
    val profile_pic: String? = null,
    val qualification: String? = null,
    val income: String? = null,
    val panNo: String? = null,
    val hotnet_id: String? = null,
    val state: String? = null,
    val district: String? = null,
    val block: String? = null,
    val village: String? = null,
    val habitation: String? = null,
    val houseNo: String? = null,
    val street: String? = null,
    val pincode: String? = null,
    val phone_number: String? = null,
    val aadhaar_id: String? = null,
    val aadhaar_copy: String? = null,
    val ration_card: String? = null,
    val ration_copy: String? = null,
    val active: Int? = null,
    val updated_at: String? = null,
    val created_at: String? = null,
    val id: Int? = null
)