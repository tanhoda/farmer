package com.tanhoda.farmer.model.home

data class LandAndBankInfo(
    val land_count: Int? = null,
    val bank_count: Int? = null
)
