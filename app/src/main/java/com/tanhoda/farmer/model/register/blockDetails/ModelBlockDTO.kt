package com.tanhoda.farmer.model.register.blockDetails

data class ModelBlockDTO(
    val message: String? = null,
    val status: Boolean? = null,
    val data: List<BlockDTO?>? = null
)