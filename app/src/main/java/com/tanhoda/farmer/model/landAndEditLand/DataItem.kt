package com.tanhoda.farmer.model.landAndEditLand

import javax.annotation.Generated

import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("chitta_no")
	val chittaNo: String? = null,

	@field:SerializedName("total_area")
	val totalArea: String? = null,

	@field:SerializedName("appstate")
	val appstate: Appstate? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("landdetailfarmer")
	val landdetailfarmer: ArrayList<LanddetailfarmerItem?>? = null,

	@field:SerializedName("appblock")
	val appblock: Appblock? = null,

	@field:SerializedName("chitta_copy")
	val chittaCopy: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("farmer_id")
	val farmerId: Int? = null,

	@field:SerializedName("district")
	val district: Int? = null,

	@field:SerializedName("block")
	val block: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state")
	val state: Int? = null,

	@field:SerializedName("village")
	val village: Int? = null,

	@field:SerializedName("appdistrict")
	val appdistrict: Appdistrict? = null,

	@field:SerializedName("villagename")
	val villagename: Villagename? = null
):Serializable