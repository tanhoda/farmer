package com.tanhoda.farmer.model.addNewApplication.subDivision.newSubDivision

data class PojoSubDivision(
    val data: ArrayList<DataItem?>? = null,
    val message: String? = null,
    val status: Boolean? = null
)
