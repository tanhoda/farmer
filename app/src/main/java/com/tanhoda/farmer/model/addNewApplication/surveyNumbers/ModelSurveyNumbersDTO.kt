package com.tanhoda.farmer.model.addNewApplication.surveyNumbers

import java.io.Serializable

data class ModelSurveyNumbersDTO(
	val message: String? = null,
	val status: Boolean? = null,
	val data: List<DataDTO?>? = null
)