package com.tanhoda.farmer.model.addNewApplication.schemCountOrAreaApplied

import java.io.Serializable

data class SchemeAreaCountDTO(
	val message: String? = null,
	val status: Boolean? = null,
	val data: DataDTO? = null
)