package com.tanhoda.farmer.model.addNewApplication.subDivision.newSubDivision

data class Subdivision(
    val farmerland_id: Int? = null,
    val land_ownership: String? = null,
    val servey_no: String? = null,
    val active: Int? = null,
    val created_at: String? = null,
    val land_area: String? = null,
    val source_of_irrigation: String? = null,
    val lease_agreement: String? = null,
    val subdivision: String? = null,
    val updated_at: String? = null,
    val farmer_id: Int? = null,
    val fmb_copy: String? = null,
    val id: Int? = null,
    val farmerlanddetail: Farmerlanddetail? = null
)
