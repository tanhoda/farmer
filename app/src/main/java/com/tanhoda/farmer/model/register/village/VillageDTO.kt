package com.tanhoda.farmer.model.register.village

import java.io.Serializable

data class VillageDTO(
    val id: Int? = null,
    val village_name: String? = null,
    val tVillage_name: String? = null,
    val district: Int? = null,
    val block: Int? = null,
    val created_date: String? = null,
    val govt_id: String? = null,
    val govt_name: String? = null,
    val aao_id: Int? = null
)