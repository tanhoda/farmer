package com.tanhoda.farmer.model.addNewBank.bank

import java.io.Serializable

data class BanknamesDTO(
    val id: Int? = null,
    val bank_name: String? = null,
    val tbank_name: String? = null
)