package com.tanhoda.farmer.model.schemeDetails

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class SchemeData(

    @SerializedName("schemes")
    val schemes: ArrayList<SchemesItem?>? = null
): Serializable