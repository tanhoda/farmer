package com.tanhoda.farmer.model.addNewApplication.schemeList

import java.io.Serializable

data class SchemeDTO(
    val id: Int? = null,
    val scheme_name: String? = null,
    val short_name: String? = null,
    val short_description: String? = null,
    val description: String? = null,
    val scheme_type: String? = null,
    val scheme_image: String? = null,
    val available_in: String? = null,
    val hortnet_id: String? = null,
    val create_date: String? = null,
    val active: Int? = null,
    val created_at: String? = null,
    val updated_at: String? = null
)