package com.tanhoda.farmer.model.modify

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class ModelEditData(

    @SerializedName("chitta_no")
    val chittaNo: String? = null,

    @SerializedName("chitta_copy")
    val chittaCopy: String? = null,

    @SerializedName("total_area")
    val totalArea: Double? = 0.0,

    @SerializedName("updated_at")
    val updatedAt: String? = null,

    @SerializedName("farmer_id")
    val farmerId: Int? = null,

    @SerializedName("district")
    val district: Int? = null,

    @SerializedName("created_at")
    val createdAt: String? = null,

    @SerializedName("block")
    val block: Int? = null,

    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("state")
    val state: Int? = null,

    @SerializedName("village")
    val village: Int? = null
)