package com.tanhoda.farmer.model.addNewApplication.categoryComponent

data class ModelComponentCategoryDTO(
    val message: String? = null,
    val status: Boolean? = null,
    val data: List<DataComponentDTO?>? = null

)