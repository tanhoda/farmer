package com.tanhoda.farmer.model.register.village

data class ModelVillageDTO(
    val message: String? = null,
    val status: Boolean? = null,
    val data: List<VillageDTO?>? = null
)