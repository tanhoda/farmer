package com.template.receiver

import android.content.BroadcastReceiver
import android.content.Context

import android.net.ConnectivityManager
import android.content.Intent
import com.tanhoda.farmer.ApplicationInit.getInstance


import com.tanhoda.farmer.utils.ConnectivityReceiverListener


class ConnectivityReceiver : BroadcastReceiver() {


    companion object {


        var connectivityReceiverListener: ConnectivityReceiverListener? = null
        fun isConnected(): Boolean {
            val cm = getInstance()!!.applicationContext
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }


        fun networkInterface(isCheckNetwor: ConnectivityReceiverListener) {
            connectivityReceiverListener = isCheckNetwor
        }
    }

    override fun onReceive(context: Context, arg1: Intent) {
        val cm = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting

        connectivityReceiverListener?.onNetworkConnectionChanged(isConnected)
    }


}