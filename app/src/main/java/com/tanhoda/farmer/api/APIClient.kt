package com.tanhoda.farmer.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class APIClient {

    companion object {
        var retrofit: Retrofit? = null
        val BASE_URL = "http://13.234.154.50/"
        //val BASE_URL = "http://192.168.10.117/tanhoda/tanhoda/public/"


        fun getClient(): Retrofit {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor)
                .readTimeout(1000, TimeUnit.SECONDS).writeTimeout(1000, TimeUnit.SECONDS)
                .connectTimeout(1000, TimeUnit.SECONDS).build()

            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL + "api/farmers/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit as Retrofit
        }
    }
}

/*
Admin password

username-tanhoda@tn.gov.in
Password-1234

Farmer Login

aadharid-252747929641
Mobile – 9578863044

Officers Login

AHO Login
username -vanitha@kambaa.com
password -1234

HO Login
username -deepika@kambaa.com
password -1234

ADH Login
username -vinodha@kambaa.com
password -1234

HO Tech Login

Username-parthiban@kambaa.com
Password-1234

ADH PM Login

Username-test@kambaa.com
Password-1234

DDH Login

Username-loganathan@kambaa.com
Password-1234


Checker Login

Username -franklin@kambaa.com
Password -1234

Approver Login

username:chitra@kambaa.com
Password:1234

Finace Login

Username-vino@kambaa.com
Password-1234

Payment Login

Username-nikshi@kambaa.com
Password-1234

AD Login

Username-subbu@kambaa.com
Password-1234

Director Login

Username-dhanaprabhu@kambaa.com
Password-1234*/