package com.tanhoda.farmer.api

class ParamAPI {

    companion object {

        const val HOME_API = "land_bank_count"
        const val AADHAR_CHECK = "aadhar_check"
        const val PASSWORD_HINT = "password_hint"
        const val REGISTER = "farmer_registration"
        const val FARMER_UPDATE = "farmer_update"
        const val LOGIN = "farmer_login"
        const val DISTRICT_LIST = "districtlist"
        const val BLOCK_LIST = "blocklist"
        const val VILLAGE_LIST = "villagelist"

        const val BLOCK_LIST_PRO = "getlandblocklist"
        const val VILLAGE_LIST_PRO = "getlandvillagelist"
        const val SURVEY_LIST_PRO = "landsurveyno"
        const val SUB_DIVISION_LIST_PRO = "subdivision"

        const val CREATE_NEW_APPLICATION = "addnewapplication"

        const val INIT_VALUES_FOR_CREATE_APPLICATION = "newapplication"
        const val COMPONENT_LIST = "componentlist"
        const val SUB_COMPONENT_AREA = "checksubcomponentlist"
        const val SUB_COMPONENT_LIST = "subcomponentlist"

        const val ADD_LAND = "landupdate"
        const val LAND_EDIT = "land_edit"

        const val ADD_NEW_BANK = "bank_update"
        const val BANK_NAMES = "bankname"
        const val BANK_BRANCH_NAMES = "bankbranchlist"

        const val READ_LAND_DETAILS = "view_land_details"
        const val READ_APPLICATION_STATUS = "application_status_details"
        const val READ_COMPLETED_APPLICATION_DETAILS = "application_completed"

        const val APPLICATION_DETAILS = "application_details"
        const val UPLOAD_AFFIDAVIT = "affidavit_upload"
        const val UPLOAD_ESTIMATION = "estimation_upload"
        const val UPLOAD_INVOICE = "invoice_upload"

        const val SCHEME_DETAILS = "scheme_details"
        const val PENDING_DOCUMENTS_UPLOAD = "upload_pending_documents"
        const val REJECTED_DOCUMENTS_UPLOAD = "upload_rejected_documents"
    }
}