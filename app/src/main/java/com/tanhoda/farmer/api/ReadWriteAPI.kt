package com.tanhoda.farmer.api

import okhttp3.ResponseBody
import retrofit2.Response

interface ReadWriteAPI {

    fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String)

    fun onResponseFailure(message: String, api: String)
}