package com.tanhoda.farmer.api

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface APIInterface {

    @POST("{path}")
    fun getSomething(
        @Path(
            "path", encoded = true
        ) path: String, @Body map: HashMap<String, String>
    ): Call<ResponseBody>

    @Multipart
    @POST("{path}")
    fun callAPIWithPart(
        @Path("path") path: String, @PartMap imgMap: HashMap<String, Any>, @Part part: ArrayList<MultipartBody.Part>
    ): Call<ResponseBody>

    @Multipart
    @POST("{path}")
    fun callAPIWithMultiPart(
        @Path("path") path: String, @PartMap imgMap: HashMap<String, Any>, @Part part: ArrayList<MultipartBody.Part>, @Part partFMBMap: ArrayList<MultipartBody.Part>,
        @Part partLeaseMap: ArrayList<MultipartBody.Part>
    ): Call<ResponseBody>

}