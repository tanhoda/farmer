package com.tanhoda.farmer.ui.home

import android.content.Context
import android.content.Intent
import android.graphics.ColorSpace
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tanhoda.farmer.BaseActivity.Companion.snackView
import com.tanhoda.farmer.MainActivity
import com.tanhoda.farmer.R
import com.tanhoda.farmer.api.Connector
import com.tanhoda.farmer.api.ParamAPI
import com.tanhoda.farmer.api.ParamKey
import com.tanhoda.farmer.api.ReadWriteAPI
import com.tanhoda.farmer.model.home.ModelHomeLandAndBankInfo
import com.tanhoda.farmer.model.landAndEditLand.ModelLandEdit
import com.tanhoda.farmer.utils.LocaleHelper
import com.tanhoda.farmer.utils.MessageUtils
import com.tanhoda.farmer.utils.SessionManager
import com.tanhoda.farmer.utils.Utils.Companion.BANK_COUNT_MAX
import com.tanhoda.farmer.utils.Utils.Companion.LAND_COUNT_MAX
import com.tanhoda.farmer.views.addNewApplication.*
import com.tanhoda.farmer.views.applicationProgressReport.ApplicationProgressReport
import com.tanhoda.farmer.views.bankDetails.AddNewBank
import com.tanhoda.farmer.views.completedApplication.CompletedApplication
import com.tanhoda.farmer.views.landDetails.LandDetails
import com.tanhoda.farmer.views.registerNewFarmer.RegisterActivity
import com.template.receiver.ConnectivityReceiver
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import okhttp3.ResponseBody
import retrofit2.Response
import java.lang.Exception

class HomeFragment : Fragment(), ReadWriteAPI {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as MainActivity).enableNavigation(getString(R.string.menu_home), 0)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.text_home.visibility = View.GONE
        rcyHomeDetails.layoutManager = GridLayoutManager(
            activity,
            2,
            LinearLayoutManager.VERTICAL,
            false
        )
        val listOfGrid = ArrayList<String>()
        listOfGrid.add(getString(R.string.add_new_application))
        listOfGrid.add(getString(R.string.application_progress_details))
        listOfGrid.add(getString(R.string.completed_application))
        listOfGrid.add(getString(R.string.land_details))
        listOfGrid.add(getString(R.string.profile_info))
        listOfGrid.add(getString(R.string.bank_details))

        rcyHomeDetails.adapter = ItemHome(activity!!, listOfGrid)
    }

    override fun onResume() {
        super.onResume()
        Log.d("onResume", "onresume")

        val map = HashMap<String, String>()
        map[ParamKey.FARMER_ID] = SessionManager.getFarmerId(activity)
        Connector.callBasic(activity!!, map, this, ParamAPI.HOME_API)
    }

    override fun onResponseSuccess(responseBody: Response<ResponseBody>, api: String) {
        try {
            val modelHome =
                Gson().fromJson(responseBody.body()?.string(), ModelHomeLandAndBankInfo::class.java)
            if (modelHome != null) {
                if (modelHome.status!!) {
                    LAND_COUNT_MAX = modelHome.data!!.land_count!!
                    BANK_COUNT_MAX = modelHome.data.bank_count!!
                } /*else {
                 snackView=   MessageUtils.showSnackBar(activity!!, homeSnackView, modelHome.message)
                }*/
            } else {
                snackView = MessageUtils.showSnackBar(
                    activity!!,
                    homeSnackView,
                    activity!!.getString(R.string.something_went_wrong)
                )
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResponseFailure(message: String, api: String) {
        snackView = MessageUtils.showSnackBar(activity!!, homeSnackView, message)
    }

    class ItemHome(val activity: FragmentActivity, val listOfGrid: ArrayList<String>) :
        RecyclerView.Adapter<Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

            return Holder(LayoutInflater.from(activity).inflate(R.layout.item_home, parent, false))
        }

        override fun getItemCount(): Int {
            return listOfGrid.size
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.txtItemName.text = listOfGrid[position]
            holder.imagStyle.setOnClickListener {
                onClickEven(position)
            }
            holder.layOfHome.setOnClickListener {
                onClickEven(position)
            }
        }

        private fun onClickEven(position: Int) {
            if (ConnectivityReceiver.isConnected()) {
                when (position) {
                    0 -> {
                        if (checkBankAndLandAdded()) {
                            activity.startActivity(
                                Intent(
                                    activity,
                                    AddNewApplication::class.java
                                ).putExtra(ParamKey.IS_THERE, false)
                            )
                        }
                    }
                    1 -> {
                        activity.startActivity(
                            Intent(
                                activity,
                                ApplicationProgressReport::class.java
                            )
                        )
                    }
                    2 -> {
                        activity.startActivity(Intent(activity, CompletedApplication::class.java))
                    }
                    3 -> {
                        activity.startActivity(Intent(activity, LandDetails::class.java))
                    }
                    4 -> {
                        activity.startActivity(
                            Intent(
                                activity,
                                RegisterActivity::class.java
                            ).putExtra("from", "profile")
                        )
                    }
                    5 -> {
                        activity.startActivity(Intent(activity, AddNewBank::class.java))
                    }
                }
            } else {
                MessageUtils.showNetworkDialog(activity)
            }
        }

        private fun checkBankAndLandAdded(): Boolean {

            Log.d("LAND_COUNT_MAXsds", "" + LAND_COUNT_MAX + " BANK_COUNT_MAX: $BANK_COUNT_MAX")
            /* if (LAND_COUNT_MAX == 0 && BANK_COUNT_MAX == 0) {
                 snackView=  MessageUtils.showSnackBar(
                     activity!!,
                     activity!!.homeSnackView,
                     "Add your Land & Bank Details"
                 )
                 return false
             } else*/ if (LAND_COUNT_MAX == 0) {
                snackView = MessageUtils.showSnackBar(
                    activity,
                    activity.homeSnackView,
                    "Add your Land Info and try again"
                )
                return false
            } else if (BANK_COUNT_MAX == 0) {
                snackView = MessageUtils.showSnackBar(
                    activity,
                    activity.homeSnackView,
                    "Add your Bank Info and Try again"
                )
                return false
            }
            return true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(snackView)
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtItemName = itemView.findViewById<TextView>(R.id.txtItemName)
        val imagStyle = itemView.findViewById<ImageView>(R.id.imagStyle)
        val cnsItemHome = itemView.findViewById<ConstraintLayout>(R.id.cnsItemHome)
        val layOfHome = itemView.findViewById<LinearLayout>(R.id.layOfHome)
    }

    override fun onAttach(context: Context) {
        super.onAttach(LocaleHelper.onAttach(context, "en"))
    }


}