package com.tanhoda.farmer.ui.tools

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tanhoda.farmer.R
import com.tanhoda.farmer.utils.LocaleHelper
import com.tanhoda.farmer.utils.MessageUtils
import kotlinx.android.synthetic.main.fragment_splash.*
import java.lang.Exception

class ToolsFragment : Fragment() {

    private lateinit var toolsViewModel: ToolsViewModel
    var language = ""
    var languageDialog: Dialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        toolsViewModel =
            ViewModelProviders.of(this).get(ToolsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_tools, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            language = LocaleHelper.getLanguage(activity!!)
            if (language == "ta") {
                languageToggle.textOff = getString(R.string.english)
            } else {
                languageToggle.textOff = getString(R.string.tamil)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        languageToggle.setOnClickListener {
            if (language == "en") {
                language = LocaleHelper.getLanguage(activity!!)
                LocaleHelper.setLocale(activity!!, "ta")
                LocaleHelper.persistForLanguage(activity!!, true)
                MessageUtils.dismissDialog(languageDialog)
                activity!!.recreate()
            } else {
                language = LocaleHelper.getLanguage(activity!!)
                LocaleHelper.setLocale(activity!!, "en")
                LocaleHelper.persistForLanguage(activity!!, true)
                MessageUtils.dismissDialog(languageDialog)
                activity!!.recreate()
            }
        }
    }
}